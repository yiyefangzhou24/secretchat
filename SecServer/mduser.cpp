#include "mduser.h"

int MdUser::getUid()
{
    return uid;
}

void MdUser::setUid(int uid)
{
    this->uid = uid;
}

QString MdUser::getUsername()
{
    return username;
}

void MdUser::setUsername(QString username)
{
    this->username = username;
}

QString MdUser::getPassword()
{
    return password;
}

void MdUser::setPassword(QString password)
{
    this->password = password;
}

QString MdUser::getNickname()
{
    return nickname;
}

void MdUser::setNickname(QString nickname)
{
    this->nickname = nickname;
}

QString MdUser::getHeadimg()
{
    return headimg;
}

void MdUser::setHeadimg(QString headimg)
{
    this->headimg = headimg;
}

int MdUser::getCreatetime()
{
    return createtime;
}

void MdUser::setCreatetime(int createtime)
{
    this->createtime = createtime;
}


// 重载 = 运算符
MdUser & MdUser::operator=(MdUser A) {
    uid = A.getUid();
    username = A.getUsername();
    password = A.getPassword();
    nickname = A.getNickname();
    headimg = A.getHeadimg();
    createtime = A.getCreatetime();
    return *this;
}
