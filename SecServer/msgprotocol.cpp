#include "msgprotocol.h"
#include "qaes.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>
#include <QCryptographicHash>

MsgProtocol::MsgProtocol()
{
    cmd = 0;
    self_uid = 0;
    target_uid = 0;
    os_type = 0;
    error = 0;
}

MsgProtocol::MsgProtocol(QByteArray data)
{
    cmd = 0;
    self_uid = 0;
    target_uid = 0;
    os_type = 0;
    error = 0;

    unProtocol(data);
}

MsgProtocol::MsgProtocol(QByteArray en_data, QByteArray key)
{
    cmd = 0;
    self_uid = 0;
    target_uid = 0;
    os_type = 0;
    error = 0;

    unProtocol(en_data, key);
}

void MsgProtocol::unProtocol(QByteArray en_data, QByteArray key)
{
    // 先解密数据再解包
//    QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::CBC);
//    QString iv("secChat@2023");
//    QByteArray hashKey = QCryptographicHash::hash(key, QCryptographicHash::Sha256);
//    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);

//    QByteArray decodeText = encryption.decode(en_data, hashKey, hashIV);
//    unProtocol(encryption.removePadding(decodeText));

    QString iv("secChat@2023!!!");
    QByteArray hashKey = QCryptographicHash::hash(key, QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
    QByteArray decodeText = QAES::decrypt(en_data, hashKey, hashIV);
    unProtocol(decodeText);
}

void MsgProtocol::unProtocol(QByteArray data)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(data);
    if(!jsonDocument.isNull() && jsonDocument.isObject())
    {
        QJsonObject rootObject = jsonDocument.object();
        cmd = rootObject.value("cmd").toInt();
        QJsonObject dataObject = rootObject.value("data").toObject();
        switch (cmd) {
        case MsgProtocol::REQ_HEARTBEAT:
        {
            break;
        }
        case MsgProtocol::REQ_LOGIN:
        {
            uname = dataObject.value("uname").toString();
            passwd = dataObject.value("passwd").toString();
            os_type = dataObject.value("os_type").toInt();
            break;
        }
        case MsgProtocol::REQ_LOGOUT:
        {
            access_token = dataObject.value("access_token").toString();
            break;
        }
        case MsgProtocol::REQ_REGISTER:
        {
            uname = dataObject.value("uname").toString();
            passwd = dataObject.value("passwd").toString();
            nickname = dataObject.value("nickname").toString();
            headimg = dataObject.value("headimg").toString();
            break;
        }
        case MsgProtocol::REQ_DELETECONTACT:
        {
            target_uid = dataObject.value("target_uid").toInt();
            access_token = dataObject.value("access_token").toString();
            break;
        }
        case MsgProtocol::REQ_SEARCHUSER:
        {
            access_token = dataObject.value("access_token").toString();
            keyword = dataObject.value("keyword").toString();
            break;
        }
        case MsgProtocol::REQ_OFFLINEMSG:
        {
            access_token = dataObject.value("access_token").toString();
            break;
        }
        case MsgProtocol::REQ_CONTACTS:
        {
            access_token = dataObject.value("access_token").toString();
            break;
        }
        case MsgProtocol::REQ_UPDATEUSERINFO:
        {
            access_token = dataObject.value("access_token").toString();
            uname = dataObject.value("uname").toString();
            nickname = dataObject.value("nickname").toString();
            headimg = dataObject.value("headimg").toString();
            passwd = dataObject.value("passwd").toString();
            break;
        }
        case MsgProtocol::REQ_SENDMESSAGE:
        {
            mid = dataObject.value("mid").toString();
            access_token = dataObject.value("access_token").toString();
            self_uid = dataObject.value("sender_uid").toInt();
            target_uid = dataObject.value("recver_uid").toInt();
            msg_text = dataObject.value("context").toString();
            msg_timestmp = dataObject.value("timestmp").toInt();
            msg_type = dataObject.value("type").toInt();
            break;
        }
        }
    }
}

QByteArray MsgProtocol::mkProtocol(QByteArray key)
{
    QByteArray data = mkProtocol();
    // 先加密数据在组包
//    QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::CBC);
//    QString iv("secChat@2023");
//    QByteArray hashKey = QCryptographicHash::hash(key, QCryptographicHash::Sha256);
//    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
//    return encryption.encode(data, hashKey, hashIV);


    QString iv("secChat@2023!!!");
    QByteArray hashKey = QCryptographicHash::hash(key, QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
    return QAES::encrypt(data, hashKey, hashIV);
}

QByteArray MsgProtocol::mkProtocol()
{
    QByteArray retData;
    QJsonObject rootObject, dataObject;
    rootObject.insert("cmd", cmd);
    switch (cmd) {
    case MsgProtocol::RET_HEARTBEAT:
    {
        break;
    }
    case MsgProtocol::RET_LOGIN:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonObject userObject;
        if(error == 0)
        {
            userObject.insert("self_uid", self_uid);
            userObject.insert("access_token", access_token);
        }
        dataObject.insert("data", userObject);
        break;
    }
    case MsgProtocol::RET_LOGOUT:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        break;
    }
    case MsgProtocol::RET_REGISTER:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        break;
    }
    case MsgProtocol::RET_DELETECONTACT:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonObject userObject;
        if(error == 0)
        {
            userObject.insert("target_uid", target_uid);
        }
        dataObject.insert("data", userObject);
        break;
    }
    case MsgProtocol::RET_SEARCHUSER:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonObject userObject;
        if(error == 0)
        {
            userObject.insert("target_uid", target_uid);
            userObject.insert("uname", uname);
            userObject.insert("nickname", nickname);
            userObject.insert("headimg", headimg);
        }
        dataObject.insert("data", userObject);
        break;
    }
    case MsgProtocol::RET_OFFLINEMSG:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonArray msgArray;
        for(int i = 0; i < msgs.size();i++)
        {
            QJsonObject msgObject;
            qDebug() << msgs.at(i).getSenderUid() << msgs.at(i).getRecverUid() << msgs.at(i).getContext() << msgs.at(i).getTimestmp();
            msgObject.insert("sender_uid", msgs.at(i).getSenderUid());
            msgObject.insert("recver_uid",  msgs.at(i).getRecverUid());
            msgObject.insert("context", msgs.at(i).getContext());
            msgObject.insert("time", msgs.at(i).getTimestmp());
            msgObject.insert("type", msgs.at(i).getType());
            msgObject.insert("mid", msgs.at(i).getMid());
            msgArray.append(msgObject);
        }
        dataObject.insert("data", msgArray);
        break;
    }
    case MsgProtocol::RET_CONTACTS:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonArray contactArray;
        for(int i = 0; i < contacts.size();i++)
        {
            QJsonObject userObject;
            qDebug() << contacts.at(i).getUid() << contacts.at(i).getNickname() << contacts.at(i).getHeadimg();
            userObject.insert("uid", contacts.at(i).getUid());
            userObject.insert("uname", contacts.at(i).getUsername());
            userObject.insert("nickname", contacts.at(i).getNickname());
            userObject.insert("headimg", contacts.at(i).getHeadimg());
            contactArray.append(userObject);
        }
        dataObject.insert("data", contactArray);
        break;
    }
    case MsgProtocol::RET_UPDATEUSERINFO:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        break;
    }
    case MsgProtocol::RET_SENDMESSAGE:
    {
        dataObject.insert("error", error);
        dataObject.insert("msg", msg);
        QJsonObject msgObject;
        if(error == 0)
        {
            msgObject.insert("mid", mid);
        }
        dataObject.insert("data", msgObject);
        break;
    }
    case MsgProtocol::REQ_RECVMESSAGE:
    {
        dataObject.insert("sender_uid", self_uid);
        dataObject.insert("recver_uid", target_uid);
        dataObject.insert("context", msg_text);
        dataObject.insert("type", msg_type);
        dataObject.insert("time", msg_timestmp);
        dataObject.insert("mid", mid);
        break;
    }
    }
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    return doc.toJson(QJsonDocument::Compact);
}
