#ifndef CONFIG_H
#define CONFIG_H

// **
//
// 配置文件相关类
// 该类均为静态函数，可直接调用
//
// created by yiyefangzhou24
// created time 2023/1/5
//
// **


#include <QObject>

#define CONFIGNAME "SecretServer.cnf"

class Config
{
public:
    Config();

    // 创建默认配置文件
    static void create();

    // 设置监听端口
    static void setListenPort(int port);

    // 获取监听端口
    static int getListenPort();

    // 使用系统默认应用程序打开配置文件
    static bool open();

    // 获取数据库主机
    static QString getSqlHost();

    // 设置数据库主机
    static void setSqlHost(QString host);

    // 设置数据库端口
    static void setSqlPort(int port);

    // 获取数据库端口
    static int getSqlPort();

    // 获取数据库用户名
    static QString getSqlUsername();

    // 设置数据库用户名
    static void setSqlUsername(QString username);

    // 获取数据库密码
    static QString getSqlPassword();

    // 设置数据库密码
    static void setSqlPassword(QString password);

    // 获取数据库名称
    static QString getSqlDbname();

    // 设置数据库名称
    static void setSqlDbname(QString dbname);

    // 设置对称密钥
    static void setCSKey(QString key);

    // 获取对称密钥
    static QString getCSKey();

    // 获取最大心跳间隔
    static int getMaxHeartbeat();

    // 设置最大心跳间隔
    static void setMaxHeartbeat(int maxHeartbeat);
};

#endif // CONFIG_H
