#include "sqlmodel.h"
#include <QSqlError>
#include <QVariant>
#include <QDebug>
#include <QDateTime>

SqlModel::SqlModel()
{
    if(QSqlDatabase::contains("qt_sql_default_connection"))
    {
        sqlDB = QSqlDatabase::database("qt_sql_default_connection");
    }
    else {
        sqlDB = QSqlDatabase::addDatabase("QMYSQL");
    }
    query = QSqlQuery();
}

void SqlModel::config(QString host, int port, QString username, QString password, QString dbname)
{
    dbHost = host;
    dbPort = port;
    dbUsername = username;
    dbPassword = password;
    dbName = dbname;
}

bool SqlModel::connect()
{
    sqlDB.setHostName(dbHost);
    sqlDB.setPort(dbPort);
    sqlDB.setUserName(dbUsername);
    sqlDB.setPassword(dbPassword);
    sqlDB.setDatabaseName(dbName);
    sqlDB.setConnectOptions("MYSQL_OPT_RECONNECT=1");
    return sqlDB.open();
}

void SqlModel::disconnect()
{
    sqlDB.close();
}

QString SqlModel::getDbVersion()
{
   keepAlive();
   if(query.exec("select version()")){
       return query.first() ? query.value(0).toString() : "";
   }else{
       return "";
   }
}

QString SqlModel::lastQuery()
{
    return query.lastQuery();
}

QString SqlModel::queryLastError()
{
    return query.lastError().text();
}

QString SqlModel::dbLastError()
{
    return sqlDB.lastError().text();
}

void SqlModel::keepAlive()
{
    if(!sqlDB.isOpen())
    {
        disconnect();
        connect();
    }
}

bool SqlModel::checkLogin(QString username, QString password)
{
    keepAlive();
    query.prepare("SELECT password FROM member where username= :username");
    query.bindValue(":username", username);
    if(query.exec())
    {
        return query.first() && query.value("password").toString() == password ? true : false;
    }
    return false;
}

MdUser SqlModel::getUserInfo(int uid)
{
    keepAlive();
    MdUser mdUser;
    query.prepare("SELECT * FROM member where uid= :uid");
    query.bindValue(":uid", uid);
    if(query.exec())
    {
        if(query.first())
        {
            mdUser.setUid(query.value("uid").toInt());
            mdUser.setUsername(query.value("username").toString());
            mdUser.setNickname(query.value("nickname").toString());
            mdUser.setHeadimg(query.value("headimg").toString());
            mdUser.setCreatetime(query.value("createtime").toInt());
        }
    }
    return mdUser;
}

MdUser SqlModel::getUserInfo(QString keyword)
{
    keepAlive();
    MdUser mdUser;
    query.prepare("SELECT * FROM member WHERE username= :keyword OR uid=:keyword");
    query.bindValue(":keyword", keyword);
    if(query.exec())
    {
        if(query.first())
        {
            mdUser.setUid(query.value("uid").toInt());
            mdUser.setUsername(query.value("username").toString());
            mdUser.setNickname(query.value("nickname").toString());
            mdUser.setHeadimg(query.value("headimg").toString());
            mdUser.setCreatetime(query.value("createtime").toInt());
        }
    }

    return mdUser;
}

int SqlModel::addUser(MdUser mdUser)
{
    keepAlive();
    // 查询最后一个用户的uid
    int uid = query.exec("SELECT uid FROM member ORDER BY uid DESC") && query.first() ? query.value("uid").toInt() + 1 : 10000;
    // 添加用户
    query.prepare("INSERT INTO member(uid, username, password, nickname, headimg, createtime) VALUES(:uid, :username, :password, :nickname, :headimg, :createtime)");
    query.bindValue(":uid", uid);
    query.bindValue(":username", mdUser.getUsername());
    query.bindValue(":password", mdUser.getPassword());
    query.bindValue(":nickname", mdUser.getNickname());
    query.bindValue(":headimg", mdUser.getHeadimg());
    query.bindValue(":createtime", (int)QDateTime::currentSecsSinceEpoch());
    if(query.exec())
    {
        return uid;
    }
    return 0;
}

bool SqlModel::updateUser(QMap <QString, QVariant> data)
{
    keepAlive();
    if(!data.contains("uid"))
        return false;
    QStringList col,val;
    if(data.contains("username"))
    {
        col.append("username=?");
        val.append(data.value("username").toString());
    }
    if(data.contains("password"))
    {
        col.append("password=?");
        val.append(data.value("password").toString());
    }
    if(data.contains("nickname"))
    {
        col.append("nickname=?");
        val.append(data.value("nickname").toString());
    }
    if(data.contains("headimg"))
    {
        col.append("headimg=?");
        val.append(data.value("headimg").toString());
    }
    query.prepare(QString("UPDATE member SET %1 WHERE uid = ?").arg(col.join(",")));
    int uid=  data.value("uid").toInt();
    data.remove("uid");
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(uid);
    if(query.exec())
    {
        return true;
    }
    return false;
}

MdUserList SqlModel::getContacts(int uid)
{
    keepAlive();
    MdUserList contacts;
    // 正向添加的好友
    query.prepare("SELECT contact.recver_uid,member.username,member.nickname,member.headimg FROM contact JOIN member ON contact.recver_uid=member.uid WHERE (contact.sender_uid=:uid and status = 1)");
    query.bindValue(":uid", uid);
    if(query.exec())
    {
        while(query.next())
        {
            MdUser mdUser;
            mdUser.setUid(query.value(0).toInt());
            mdUser.setUsername(query.value(1).toString());
            mdUser.setNickname(query.value(2).toString());
            mdUser.setHeadimg(query.value(3).toString());
            contacts.insert(mdUser);
        }
    }
    // 反向接受的好友
    query.prepare("SELECT contact.sender_uid,member.username,member.nickname,member.headimg FROM contact JOIN member ON contact.sender_uid=member.uid WHERE (contact.recver_uid=:uid and status = 1)");
    query.bindValue(":uid", uid);
    if(query.exec())
    {
        while(query.next())
        {
            MdUser mdUser;
            mdUser.setUid(query.value(0).toInt());
            mdUser.setUsername(query.value(1).toString());
            mdUser.setNickname(query.value(2).toString());
            mdUser.setHeadimg(query.value(3).toString());
            contacts.insert(mdUser);
        }
    }
    return contacts;
}

bool SqlModel::deleteContact(int self_uid, int target_uid)
{
    keepAlive();
    // 清空该联系人关系的离线聊天记录
    query.prepare("UPDATE message SET status = 0 WHERE (sender_uid = :self_uid AND recver_uid = :target_uid) OR (sender_uid = :target_uid AND recver_uid = :self_uid)");
    query.bindValue(":self_uid", self_uid);
    query.bindValue(":target_uid", target_uid);
    query.exec();
    // 删除对应联系人关系
    query.prepare("DELETE FROM contact WHERE (sender_uid = :self_uid AND recver_uid = :target_uid) OR (sender_uid = :target_uid AND recver_uid = :self_uid)");
    query.bindValue(":self_uid", self_uid);
    query.bindValue(":target_uid", target_uid);
    return query.exec();
}

MdMsgList SqlModel::getOfflineMsgs(int uid)
{
    keepAlive();
    // 查询离线消息
    MdMsgList msgs;
    query.prepare("SELECT * FROM message WHERE recver_uid = :uid AND status=1");
    query.bindValue(":uid", uid);
    if(query.exec())
    {
        while (query.next()) {
            MdMsg msg;
            msg.setSenderUid(query.value("sender_uid").toInt());
            msg.setRecverUid(query.value("recver_uid").toInt());
            msg.setContext(query.value("context").toString());
            msg.setTimestmp(query.value("time").toInt());
            msg.setType(query.value("type").toInt());
            msg.setMid(query.value("mid").toString());
            msgs.insert(msg);
        }
        // 将所有离线消息设置为已接收
        query.prepare("UPDATE message SET status = 0 WHERE recver_uid = :uid AND status=1");
        query.bindValue(":uid", uid);
        query.exec();
    }

    return msgs;
}

bool SqlModel::addOfflineMsg(MdMsg mdMsg)
{
    keepAlive();
    // 添加消息
    query.prepare("INSERT INTO message(mid,sender_uid,recver_uid,context,type,status,time) VALUES(:mid, :sender_uid, :recver_uid, :context, :type, 1, :time)");
    query.bindValue(":mid", mdMsg.getMid());
    query.bindValue(":sender_uid", mdMsg.getSenderUid());
    query.bindValue(":recver_uid", mdMsg.getRecverUid());
    query.bindValue(":context", mdMsg.getContext());
    query.bindValue(":type", mdMsg.getType());
    query.bindValue(":time", mdMsg.getTimestmp());
    if(query.exec())
    {
        return true;
    }
    return false;
}

bool SqlModel::addContact(int self_uid, int target_uid, QString text)
{
    keepAlive();
    // 覆盖删除已经存在好友关系
    query.prepare("DELETE FROM contact WHERE (sender_uid=:sender_uid AND recver_uid=:recver_uid) OR (sender_uid=:recver_uid AND recver_uid=:sender_uid)");
    query.bindValue(":sender_uid", self_uid);
    query.bindValue(":recver_uid", target_uid);
    // 添加好友
    query.prepare("INSERT INTO contact(sender_uid,recver_uid,status,time,remark) VALUES(:sender_uid, :recver_uid, 0, :time, :remark)");
    query.bindValue(":sender_uid", self_uid);
    query.bindValue(":recver_uid", target_uid);
    query.bindValue(":time", (int)QDateTime::currentSecsSinceEpoch());
    query.bindValue(":remark", text);
    if(query.exec())
    {
        return true;
    }
    return false;
}

bool SqlModel::agreeContact(int self_uid, int target_uid)
{
    keepAlive();
    // 判断是否已经存在好友关系
    query.prepare("SELECT * FROM contact WHERE (sender_uid=:sender_uid AND recver_uid=:recver_uid) OR (sender_uid=:recver_uid AND recver_uid=:sender_uid)");
    query.bindValue(":sender_uid", self_uid);
    query.bindValue(":recver_uid", target_uid);
    if(query.exec() && query.size() >= 0)
    {
        // 修改好友关系状态位
        query.prepare("UPDATE contact SET status=1 WHERE (sender_uid=:sender_uid AND recver_uid=:recver_uid) OR (sender_uid=:recver_uid AND recver_uid=:sender_uid)");
        query.bindValue(":sender_uid", self_uid);
        query.bindValue(":recver_uid", target_uid);
        if(query.exec())
        {
            return true;
        }
    }
    return false;
}
