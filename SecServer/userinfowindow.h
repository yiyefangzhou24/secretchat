#ifndef USERINFOWINDOW_H
#define USERINFOWINDOW_H

#include <QMainWindow>

namespace Ui {
class UserInfoWindow;
}

class UserInfoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit UserInfoWindow(QWidget *parent = nullptr);
    ~UserInfoWindow();

    void setData(int uid, QString username, QString nickname, QString access_token, QString ipaddress, int port, QString host);
private:
    Ui::UserInfoWindow *ui;
};

#endif // USERINFOWINDOW_H
