#ifndef MSGPROTOCOL_H
#define MSGPROTOCOL_H

// **
//
// 通讯协议消息结构类
//
// created by yiyefangzhou24
// created time 2023/1/5
//
// **

#include <QString>
#include <QList>
#include <QByteArray>
#include "mduser.h"
#include "mduserlist.h"
#include "mdmsg.h"
#include "mdmsglist.h"

class MsgProtocol
{
public:
    MsgProtocol();

    // 解包构造函数
    MsgProtocol(QByteArray data);

    // 解包构造函数（加密版）
    MsgProtocol(QByteArray en_data, QByteArray key);

    // 解包
    void unProtocol(QByteArray data);

    // 解包（加密版）
    void unProtocol(QByteArray en_data, QByteArray key);

    // 封包
    QByteArray mkProtocol();

    // 封包（加密版）
    QByteArray mkProtocol(QByteArray key);

public:
    int cmd;                            // 请求命令码
    int self_uid;                       // 发送方用户uid
    int target_uid;                     // 接收方用户uid
    QString access_token;               // 授权的access_token
    QString uname;                      // 用户名
    QString passwd;                     // 密码
    QString nickname;                   // 用户昵称
    QString headimg;                    // 用户头像
    int os_type;                        // 客户端系统类型
    int error;                          // 错误代码
    QString msg;                        // 返回状态信息
    QString msg_text;                   // 加密的消息内容
    QString mid;                        // 消息标识
    int msg_timestmp;                   // 消息时间
    int msg_type;                       // 消息类型
    MdUserList contacts;                // 联系人列表
    MdMsgList msgs;                     // 离线消息列表
    QString keyword;                    // 搜索关键词

    static const int OS_PCWIN           = 0x00000001;       // windows电脑端
    static const int OS_PCMAC           = 0x00000002;       // mac电脑端
    static const int OS_PCLINUX         = 0x00000003;       // Linux电脑端
    static const int OS_WAPANDROID      = 0x00000004;       // Android手机端
    static const int OS_WAPIPHONE       = 0x00000005;       // iPhone手机端
    static const int OS_WAPHARMONY      = 0x00000006;       // Harmony手机端

    static const int REQ_HEARTBEAT      = 0x00010001;       // 心跳请求
    static const int RET_HEARTBEAT      = 0x00010002;       // 心跳返回
    static const int REQ_LOGIN          = 0x00010003;       // 登录请求
    static const int RET_LOGIN          = 0x00010004;       // 登录返回
    static const int REQ_LOGOUT         = 0x00010005;       // 注销请求
    static const int RET_LOGOUT         = 0x00010006;       // 注销返回
    static const int REQ_REGISTER       = 0x00010007;       // 注册请求
    static const int RET_REGISTER       = 0x00010008;       // 注册返回
    static const int REQ_SEARCHUSER     = 0x00010009;       // 搜索用户请求
    static const int RET_SEARCHUSER     = 0x0001000a;       // 搜索用户返回
    static const int REQ_OFFLINEMSG     = 0x0001000b;       // 获取离线消息请求
    static const int RET_OFFLINEMSG     = 0x0001000c;       // 获取离线消息返回
    static const int REQ_CONTACTS       = 0x0001000d;       // 获取联系人列表请求
    static const int RET_CONTACTS       = 0x0001000e;       // 获取联系人列表返回
    static const int REQ_UPDATEUSERINFO = 0x0001000f;       // 更新用户信息请求
    static const int RET_UPDATEUSERINFO = 0x00010010;       // 更新用户信息返回
    static const int REQ_DELETECONTACT  = 0x00010011;       // 删除联系人请求
    static const int RET_DELETECONTACT  = 0x00010012;       // 删除联系人返回
    static const int REQ_SENDMESSAGE    = 0x00010013;       // 发送用户消息请求
    static const int RET_SENDMESSAGE    = 0x00010014;       // 发送用户消息返回
    static const int REQ_RECVMESSAGE    = 0x00010015;       // 接收用户消息请求
    static const int RET_RECVMESSAGE    = 0x00010016;       // 接收用户消息返回（暂时未使用该标志）

    static const int ERR_OAUTH          = 0x00020001;       // 未授权访问
    static const int ERR_SQLEXEC        = 0X00020002;       // SQL语句执行错误
    static const int ERR_FORMAT         = 0X00020003;       // 数据格式错误
    static const int ERR_RESNULL        = 0X00020004;       // 没有结果

    static const int MSG_TEXT           = 0x00000001;       // 聊天消息
    static const int MSG_ADDCONTACT     = 0x00000002;       // 添加好友
    static const int MSG_AGREECONTACT   = 0x00000003;       // 接受/拒绝好友
    static const int MSG_DELCONTACT     = 0x00000004;       // 删除好友
};

#endif // MSGPROTOCOL_H
