#ifndef MDMSG_H
#define MDMSG_H

// **
//
// 消息Model类
//    仅用来存消息结构体
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QString>

class MdMsg
{
public:
    // 获取发送方用户uid
    int getSenderUid();

    // 设置发送方用户uid
    void setSenderUid(int uid);

    // 获取接收方用户uid
    int getRecverUid();

    // 设置接收方用户uid
    void setRecverUid(int uid);

    // 获取时间戳
    int getTimestmp();

    // 设置时间戳
    void setTimestmp(int timestmp);

    // 获取消息内容
    QString getContext();

    // 设置消息内容
    void setContext(QString context);

    // 获取消息类型
    int getType();

    // 设置消息类型
    void setType(int type);

    // 设置消息标识
    void setMid(QString mid);

    // 获取消息标识
    QString getMid();

    // 重载运算符=, 类赋值操作（变量赋值）
    MdMsg & operator=(MdMsg A);
private:

    // 发送方用户uid
    int sender_uid = 0;

    // 接收方用户uid
    int recver_uid = 0;

    // 时间戳
    int timestmp = 0;

    // 消息内容
    QString context;

    // 消息类型
    int type = 0;

    // 消息标识
    QString mid;
};

#endif // MDMSG_H
