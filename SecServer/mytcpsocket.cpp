#include "mytcpsocket.h"
#include <QDebug>

MyTcpSocket::MyTcpSocket() : MsgPackage()
{
    oauthStatus = OA_INIT;  //设置初始认证状态
    isCheckSum = false;      //开启校验和
    uid = 0;
}

// 重载 == 运算符
bool MyTcpSocket::operator==(const MyTcpSocket &A) {
    return this->socket->socketDescriptor() == A.socket->socketDescriptor() ? true : false;
}

void MyTcpSocket::pkgReady(QByteArray data)
{
    // 向上层发送接收数据包信号
    emit this->sigPkgReady(data, this);
}
