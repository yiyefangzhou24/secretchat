#include "userinfowindow.h"
#include "ui_userinfowindow.h"

UserInfoWindow::UserInfoWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UserInfoWindow)
{
    ui->setupUi(this);
    setFixedSize(this->width(), this->height());
    ui->labelNickname->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelUsername->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelUid->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelToken->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelIP->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelPort->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelHost->setTextInteractionFlags(Qt::TextSelectableByMouse);
}

UserInfoWindow::~UserInfoWindow()
{
    delete ui;
}

void UserInfoWindow::setData(int uid, QString username, QString nickname, QString access_token, QString ipaddress, int port, QString host)
{
    ui->labelUid->setText(QString::number(uid));
    ui->labelNickname->setText(nickname);
    ui->labelUsername->setText(QString("@%1").arg(username));
    ui->labelToken->setText(access_token);
    ui->labelIP->setText(ipaddress);
    ui->labelPort->setText(QString::number(port));
    ui->labelHost->setText(host);
}
