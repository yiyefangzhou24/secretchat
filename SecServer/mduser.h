#ifndef MDUSER_H
#define MDUSER_H

// **
//
// 用户信息Model类
//    仅用来存放用户数据集
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QString>

class MdUser
{
public:

    int getUid();

    void setUid(int uid);

    QString getUsername();

    void setUsername(QString username);

    QString getPassword();

    void setPassword(QString password);

    QString getNickname();

    void setNickname(QString nickname);

    QString getHeadimg();

    void setHeadimg(QString headimg);

    int getCreatetime();

    void setCreatetime(int createtime);

    // 重载运算符=, 类赋值操作（变量赋值）
    MdUser & operator=(MdUser A);
private:

    int uid = 0;

    QString username;

    QString password;

    QString nickname;

    QString headimg;

    int createtime = 0;
};

#endif // MDUSER_H
