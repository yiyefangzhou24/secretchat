#ifndef MDMSGLIST_H
#define MDMSGLIST_H

// **
//
// 消息列表Model类
//    仅用来存放消息数据结构数组
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QList>
#include "mdmsg.h"

class MdMsgList
{
public:
    void insert(MdMsg msg);

    MdMsg at(int i);

    void clear();

    int size();

    MdMsgList & operator=(MdMsgList A);

private:

    QList<MdMsg> msgList;
};

#endif // MDMSGLIST_H
