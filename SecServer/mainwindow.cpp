#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "config.h"
#include "mduser.h"
#include "qlog.h"
#include "mduserlist.h"
#include "userinfowindow.h"
#include "configwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pushButtonStop->setDisabled(true);
    ui->labelTitle->setScaledContents(true);
    ui->labelTitle->setPixmap(QPixmap("://image/title.png")); 

    // 设置客户端列表组件
    ui->tableWidgetClients->setColumnCount(3);                                                 //设置为3列
    ui->tableWidgetClients->setHorizontalHeaderLabels(QStringList() << "UID" <<"用户名" << "IP地址");  //设置列名称
    ui->tableWidgetClients->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);   //设置第3列自动宽度
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);                 //设置可调宽度
    ui->tableWidgetClients->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidgetClients->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidgetClients->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidgetClients->setContextMenuPolicy(Qt::CustomContextMenu);                     //开启右键菜单

    // 检测配置文件
    QFileInfo configFile(CONFIGNAME);
    if(!configFile.exists())
        Config::create();

    qDebug() << "Database driver has： " << QSqlDatabase::drivers();

    // 以下是测试代码
//    addListItem(20000, "test1", "127.0.0.1");
//    addListItem(20001, "test1", "127.0.0.1");
//    addListItem(20002, "test1", "127.0.0.1");
//    addListItem(20003, "test1", "127.0.0.1");
}

MainWindow::~MainWindow()
{
    delete ui;
}

// ************************** 以下是UI槽函数 ********************************
// 启动按钮消息响应
void MainWindow::on_pushButtonStart_clicked()
{
    // 检查环境变量
    int listenPort = Config::getListenPort();
    QString sqlHost = Config::getSqlHost();
    int sqlPort = Config::getSqlPort();
    QString sqlUsername = Config::getSqlUsername();
    QString sqlPassword = Config::getSqlPassword();
    QString sqlDbname = Config::getSqlDbname();
    int maxHeartbeat = Config::getMaxHeartbeat();
    if(Config::getCSKey().length() > 0)
        secKey = QByteArray::fromBase64(Config::getCSKey().toLatin1(), QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);

    if(listenPort <= 0 || sqlHost.isEmpty() || sqlPort <=0 || sqlUsername.isEmpty())
    {
        addStatus("载入配置文件信息出错", "错误");
        QLog::error("载入配置文件信息出错", "MainWindow");
        return;
    }
    addStatus(QString("[listen:%1, maxHeartbeat:%2]").arg(listenPort).arg(maxHeartbeat), "系统配置");
    addStatus(QString("[host:%1 , port:%2 , username:%3 , password:******]").arg(sqlHost).arg(sqlPort).arg(sqlUsername), "数据库配置");
    QLog::log(QString("载入系统配置 [host:%1 , port:%2 , username:%3 , password:******, listen:%4]").arg(sqlHost).arg(sqlPort).arg(sqlUsername).arg(listenPort), "MainWindow");

    // 初始化数据库连接
    sqlModel.config(sqlHost, sqlPort, sqlUsername, sqlPassword, sqlDbname);
    if(!sqlModel.connect())
    {
        addStatus(QString("数据库连接错误：%1").arg(sqlModel.dbLastError()), "错误");
        QLog::error(QString("数据库连接错误：%1").arg(sqlModel.dbLastError()), "MainWindow");
        return;
    }
    else
    {
        addStatus(QString("数据库连接成功，版本：Mysql %1").arg(sqlModel.getDbVersion()), "信息");
        QLog::log(QString("数据库连接成功，版本：Mysql %1").arg(sqlModel.getDbVersion()), "MainWindow");
    }

    // 初始化网络，并开启网络监听
    tcpServer = new QTcpServer(this);
    if(tcpServer->listen(QHostAddress::Any, listenPort))
    {
        connect(tcpServer, &QTcpServer::newConnection, this, &MainWindow::on_tcp_connect);
        connect(tcpServer, &QTcpServer::acceptError, this, &MainWindow::on_tcp_accept_error);
        ui->pushButtonStart->setDisabled(true);
        ui->pushButtonStop->setDisabled(false);
        addStatus("服务启动成功", "信息");
        QLog::log("服务启动成功", "MainWindow");
    }
    else
    {
        tcpServer->deleteLater();
        addStatus("程序端口被占用", "错误");
        QLog::error("程序端口被占用", "MainWindow");
    }

    // 开启心跳检活线程，定期检测客户端是否意外断开，避免浪费系统资源
    connect(timer,&QTimer::timeout,[=](){
        int timestmp = (int)QDateTime::currentSecsSinceEpoch();
        qDebug() << "启动定时检活程序 | 客户端数量：" << myTcpSocketList.size() << "当前时间戳："<< timestmp;
        foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
        {
            qDebug() << timestmp <<  myTcpSocket->lastHeartbeatTime;
            if(timestmp - myTcpSocket->lastHeartbeatTime > maxHeartbeat)
            {
                addStatus(QString("客户端意外断开:[IP:%1,PORT:%2]").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerPort()));
                QLog::log(QString("客户端意外断开:[IP:%1,PORT:%2]").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerPort()), "MainWindow");

                // 首先判断是否是登录状态，如果是登录状态则需要移除ui列表中项目
                if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED)
                {
                    qDebug() << "删除UI列表中项目";
                    removeListItem(myTcpSocket->uid);
                }
                myTcpSocketList.removeOne(myTcpSocket);// 从QList中删除
                myTcpSocket->socket->close();
                myTcpSocket->socket->deleteLater(); //删除socket
                delete myTcpSocket;// 删除对象
            }
        }
    });
    timer->start(60000);
}

// 停止按钮消息响应
void MainWindow::on_pushButtonStop_clicked()
{
    // 断开数据库连接
    sqlModel.disconnect();
    addStatus("断开数据库连接", "信息");
    QLog::log("断开数据库连接", "MainWindow");

    // 停止监听
    if(tcpServer && tcpServer->isListening())
    {
        tcpServer->close();
        tcpServer->deleteLater();
        tcpServer = nullptr;
        addStatus("服务停止成功", "信息");
        QLog::log("服务停止成功", "MainWindow");
        ui->pushButtonStart->setDisabled(false);
        ui->pushButtonStop->setDisabled(true);
    }
    // 删除网络连接列表
    foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
    {
        delete myTcpSocket;// 删除对象
    }
    myTcpSocketList.clear();
    // 清空ui列表项目
    for(int i = ui->tableWidgetClients->rowCount() - 1;i >= 0; i--)
    {
        ui->tableWidgetClients->removeRow(i);
    }
    // 关闭心跳检活线程
    timer->stop();
}

// 配置按钮消息响应
void MainWindow::on_pushButtonConfig_clicked()
{
    ConfigWindow * configWindow = new ConfigWindow(this);
    configWindow->show();
}

// 日志按钮消息响应
void MainWindow::on_pushButtonLog_clicked()
{
    if(!QLog::open())
        QMessageBox::critical(this, "提示", "找不到默认打开程序，请手动打开程序根目录ServerLogs.log文件");
}

// 列表右键菜单
void MainWindow::on_tableWidgetClients_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu;
    QAction * infoAction = menu.addAction(tr("详细信息"));
    menu.addSeparator();
    QAction * deleteAction = menu.addAction(tr("强制下线"));

    connect(infoAction, &QAction::triggered, [=](){
        int curRow = ui->tableWidgetClients->currentRow();
        // 找到socket列表中对应项目，此处肯定已经登录，直接使用用户uid查找
        foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
        {
            if(myTcpSocket->uid == ui->tableWidgetClients->item(curRow, 0)->text().toInt())
            {
                UserInfoWindow * userInfoWindow = new UserInfoWindow(this);
                userInfoWindow->setData(myTcpSocket->uid,
                                        myTcpSocket->username,
                                        myTcpSocket->nickname,
                                        myTcpSocket->accessToken,
                                        myTcpSocket->socket->peerAddress().toString(),
                                        myTcpSocket->socket->peerPort(),
                                        myTcpSocket->socket->peerName());
                userInfoWindow->setAttribute(Qt::WA_DeleteOnClose);
                userInfoWindow->show();
            }
        }
    });

    connect(deleteAction, &QAction::triggered, [=](){
        int curRow = ui->tableWidgetClients->currentRow();
        // 找到socket列表中对应项目，此处肯定已经登录，直接使用用户uid查找
        foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
        {
            if(myTcpSocket->uid == ui->tableWidgetClients->item(curRow, 0)->text().toInt())
            {
                qDebug() << "强制下线，并删除socket连接" << ui->tableWidgetClients->item(curRow, 0) << ui->tableWidgetClients->item(curRow, 1);
                myTcpSocketList.removeOne(myTcpSocket);// 从QList中删除
                myTcpSocket->socket->close();
                myTcpSocket->socket->deleteLater(); //删除socket
                delete myTcpSocket;// 删除对象
                // 删对应ui列表中项目
                ui->tableWidgetClients->removeRow(curRow);
                break;
            }
        }

    });

    menu.exec(QCursor::pos());
}

// ***********************  以下是网络通讯槽函数 *****************************
void MainWindow::on_tcp_connect()
{
    MyTcpSocket * mytcpsocket = new MyTcpSocket();
    mytcpsocket->socket = tcpServer->nextPendingConnection();
    addStatus(QString("新客户端:[IP:%1,PORT:%2]").arg(mytcpsocket->socket->peerAddress().toString()).arg(mytcpsocket->socket->peerPort()));
    QLog::log(QString("新客户端:[IP:%1,PORT:%2]").arg(mytcpsocket->socket->peerAddress().toString()).arg(mytcpsocket->socket->peerPort()), "MainWindow");
    mytcpsocket->lastHeartbeatTime = (int)QDateTime::currentSecsSinceEpoch();

    // 绑定数据可读信号
    connect(mytcpsocket->socket, &QTcpSocket::readyRead, this, &MainWindow::on_tcp_read);
    // 绑定断开连接信号
    connect(mytcpsocket->socket, &QTcpSocket::disconnected, this, &MainWindow::on_tcp_disconnect);
    // 绑定接收到完整数据包信号
    connect(mytcpsocket, &MyTcpSocket::sigPkgReady, this, &MainWindow::on_pkg_ready);
    // 添加到客户端列表
    myTcpSocketList.append(mytcpsocket);

    qDebug() << "现有客户端个数" << myTcpSocketList.size();
}

// 槽函数 - 客户端网络断开
void MainWindow::on_tcp_disconnect()
{
    QTcpSocket * tmpTcpSocket = (QTcpSocket*)sender();
    addStatus(QString("客户端正常断开:[IP:%1,PORT:%2]").arg(tmpTcpSocket->peerAddress().toString()).arg(tmpTcpSocket->peerPort()));
    QLog::log(QString("客户端正常断开:[IP:%1,PORT:%2]").arg(tmpTcpSocket->peerAddress().toString()).arg(tmpTcpSocket->peerPort()), "MainWindow");

    // 找到socket列表中对应项目，此处可能尚未登录，使用socket描述符查找
    foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
    {
        if(myTcpSocket->socket->socketDescriptor() == tmpTcpSocket->socketDescriptor())
        {
            // 首先判断是否是登录状态，如果是登录状态则需要移除ui列表中项目
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED)
            {
                qDebug() << "删除UI列表中项目";
                removeListItem(myTcpSocket->uid);
            }

            // 从客户端列表删除，并delete对象
            qDebug() << "删除socket连接" << myTcpSocket->socket->peerAddress();
            myTcpSocketList.removeOne(myTcpSocket);// 从QList中删除
            myTcpSocket->socket->deleteLater(); //删除socket
            delete myTcpSocket;// 删除对象

            break;
        }
    }

    qDebug() << "现有客户端个数" << myTcpSocketList.size();
}

// 槽函数 - 收到客户端发来数据
void MainWindow::on_tcp_read()
{
    QTcpSocket * tmpTcpSocket = (QTcpSocket*)sender();

    // 寻找客户端，将接收数据写入客户端的解包函数
    foreach(MyTcpSocket * myTcpSocket, myTcpSocketList)
    {
        if(myTcpSocket->socket->socketDescriptor() == tmpTcpSocket->socketDescriptor())
        {
            myTcpSocket->unPkg(tmpTcpSocket->readAll());
            break;
        }
    }
}

// 槽函数 - 收到客户端发来的一个完整的封包
void MainWindow::on_pkg_ready(QByteArray data, MyTcpSocket *myTcpSocket)
{
    qDebug() << "收到完整数据包" << myTcpSocket->socket->peerAddress().toString() << myTcpSocket->socket->peerName() << "数据包大小" << data.size() << data;
    MsgProtocol recvProtocol, sendProtocol;
    secKey.length()> 0? recvProtocol.unProtocol(data, secKey) : recvProtocol.unProtocol(data);
    switch (recvProtocol.cmd) {
    case MsgProtocol::REQ_HEARTBEAT:
    {
//        qDebug() << "心跳请求";
//        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "心跳请求");
        myTcpSocket->lastHeartbeatTime = (int)QDateTime::currentSecsSinceEpoch();
        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_HEARTBEAT;
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "心跳返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_LOGIN:
    {
        qDebug() << "登录请求" << recvProtocol.uname << recvProtocol.passwd << recvProtocol.os_type;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "登录请求");
        QLog::log(QString("<登录请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(recvProtocol.uname), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_LOGIN;
        // 输入验证
        if(checkDataLegal(recvProtocol))
        {
            // 验证是否存在重复登录问题
            bool isLogin = false;
            foreach(MyTcpSocket * item, myTcpSocketList)
            {
                if(item->username == recvProtocol.uname && item->oauthStatus == MyTcpSocket::OA_LOGINED)
                {
                    isLogin = true;
                    break;
                }
            }
            if(!isLogin)
            {
                if(sqlModel.checkLogin(recvProtocol.uname, recvProtocol.passwd))
                {
                    MdUser mdUser = sqlModel.getUserInfo(recvProtocol.uname);
                    // 更新连接列表
                    myTcpSocket->oauthStatus = MyTcpSocket::OA_LOGINED;
                    myTcpSocket->uid = mdUser.getUid();
                    myTcpSocket->username = mdUser.getUsername();
                    myTcpSocket->nickname = QByteArray::fromBase64(mdUser.getNickname().toUtf8());
                    myTcpSocket->headimg = mdUser.getHeadimg();
                    myTcpSocket->accessToken = getRandomString(32);
                    //构造返回包
                    sendProtocol.error = 0;
                    sendProtocol.self_uid = mdUser.getUid();
                    sendProtocol.access_token = myTcpSocket->accessToken;
                    //列表显示
                    addListItem(myTcpSocket->uid, myTcpSocket->username, myTcpSocket->socket->peerAddress().toString());
                }else
                {
                    sendProtocol.error = MsgProtocol::ERR_OAUTH;
                    sendProtocol.msg = "username or password error";
                }
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "user already login";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }

        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "登录返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_LOGOUT:
    {
        qDebug() << "注销请求" << recvProtocol.access_token;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "注销请求");
        QLog::log(QString("<注销请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_LOGOUT;
        // 输入验证
        if(checkDataLegal(recvProtocol))
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {

                // 更新UI列表
                qDebug() << "测试测试" << myTcpSocket->uid;
                removeListItem(myTcpSocket->uid);
                // 更新连接列表
                myTcpSocket->oauthStatus = MyTcpSocket::OA_LOGOUT;
                myTcpSocket->accessToken = "";
                sendProtocol.error = 0;
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "注销返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_UPDATEUSERINFO:
    {
        qDebug() << "更新用户信息请求" << recvProtocol.access_token << recvProtocol.passwd << recvProtocol.uname << recvProtocol.nickname << recvProtocol.headimg;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "更新用户信息请求");
        QLog::log(QString("<更新用户信息请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_UPDATEUSERINFO;
        // 输入验证
        if(checkDataLegal(recvProtocol))
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                // 更新密码
                QMap<QString, QVariant> dataMap;
                dataMap["uid"] = myTcpSocket->uid;
                if(!recvProtocol.uname.isEmpty())
                {
                    dataMap["username"] = recvProtocol.uname;
                }
                if(!recvProtocol.passwd.isEmpty())
                {
                    dataMap["password"] = recvProtocol.passwd;
                }
                if(!recvProtocol.nickname.isEmpty())
                {
                    dataMap["nickname"] = recvProtocol.nickname;
                }
                if(!recvProtocol.headimg.isEmpty())
                {
                    dataMap["headimg"] = recvProtocol.headimg;
                }
                if(sqlModel.updateUser(dataMap))
                {
                    sendProtocol.error = 0;
                }
                else
                {
                    sendProtocol.error =  MsgProtocol::ERR_SQLEXEC;
                    sendProtocol.msg = sqlModel.queryLastError();
                }
            }
            else
            {
                sendProtocol.error =  MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "更新用户信息返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_REGISTER:
    {
        qDebug() << "注册请求" << recvProtocol.uname << recvProtocol.passwd << recvProtocol.nickname << recvProtocol.headimg;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "注册请求");
        QLog::log(QString("<注册请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(recvProtocol.uname), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_REGISTER;
        // 输入验证
        if(checkDataLegal(recvProtocol))
        {
            MdUser mdUser;
            mdUser.setUsername(recvProtocol.uname);
            mdUser.setNickname(recvProtocol.nickname);
            mdUser.setPassword(recvProtocol.passwd);
            mdUser.setHeadimg(recvProtocol.headimg);
            int uid = sqlModel.addUser(mdUser);
            if(uid > 0)
            {
                sendProtocol.error = 0;
            }else
            {
                sendProtocol.error = MsgProtocol::ERR_SQLEXEC;
                sendProtocol.msg = sqlModel.queryLastError();
            }
        }
        else{
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }

        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "注册返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_DELETECONTACT:
    {
        qDebug() << "删除联系人请求" << recvProtocol.access_token << recvProtocol.target_uid;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "删除联系人请求");
        QLog::log(QString("<删除联系人请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");
        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_DELETECONTACT;
        // 输入验证
        if(checkDataLegal(recvProtocol) && recvProtocol.target_uid > 0)
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                if(sqlModel.deleteContact(myTcpSocket->uid, recvProtocol.target_uid))
                {
                    sendProtocol.error = 0;
                    sendProtocol.target_uid = recvProtocol.target_uid;

                    // 判断删除的联系人是否在线，如果在线则要转发删除联系人消息
                    MyTcpSocket * recverTcpSocket = nullptr;
                    foreach(MyTcpSocket * item, myTcpSocketList)
                    {
                        if(item->uid == recvProtocol.target_uid)
                        {
                            recverTcpSocket = item;
                            break;
                        }
                    }
                    if(recverTcpSocket != nullptr)
                    {
                        MsgProtocol transferProtocol;MdMsg mdMsg;
                        mdMsg.setSenderUid(myTcpSocket->uid);
                        mdMsg.setRecverUid(recvProtocol.target_uid);
                        mdMsg.setTimestmp((int)QDateTime::currentSecsSinceEpoch());
                        mdMsg.setType(MsgProtocol::MSG_DELCONTACT);
                        mdMsg.setMid(getMsgMid(mdMsg));
                        transferProtocol.cmd = MsgProtocol::REQ_RECVMESSAGE;
                        transferProtocol.self_uid = myTcpSocket->uid;
                        transferProtocol.target_uid = recvProtocol.target_uid;
                        transferProtocol.msg_timestmp = mdMsg.getTimestmp();
                        transferProtocol.msg_type = MsgProtocol::MSG_DELCONTACT;
                        transferProtocol.mid = mdMsg.getMid();

                        QByteArray transferData = secKey.length() > 0 ? transferProtocol.mkProtocol(secKey) : transferProtocol.mkProtocol();
                        qDebug() << "通知删除联系人消息" << transferData;
                        recverTcpSocket->socket->write(recverTcpSocket->mkPkg(transferData));
                        recverTcpSocket->socket->flush();
                    }
                }
                else
                {
                    sendProtocol.error = MsgProtocol::ERR_SQLEXEC;
                    sendProtocol.msg = sqlModel.queryLastError();
                }
            }
            else
            {
                sendProtocol.error =  MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else{
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }

        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "删除联系人返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_SEARCHUSER:
    {
        qDebug() << "搜索用户请求" << recvProtocol.access_token << recvProtocol.keyword;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "搜索用户请求");
        QLog::log(QString("<搜索用户请求> %1|%2|%3").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username).arg(recvProtocol.keyword), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_SEARCHUSER;
        // 验证输入
        if(checkDataLegal(recvProtocol))
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                MdUser mdUser = sqlModel.getUserInfo(recvProtocol.keyword);
                //构造返回包
                if(mdUser.getUid() <= 0)
                {
                    sendProtocol.error = MsgProtocol::ERR_RESNULL;
                    sendProtocol.msg = "null result";
                }
                else
                {
                    sendProtocol.error = 0;
                }
                sendProtocol.target_uid = mdUser.getUid();
                sendProtocol.uname = mdUser.getUsername();
                sendProtocol.nickname = mdUser.getNickname();
                sendProtocol.headimg = mdUser.getHeadimg();
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "搜索用户返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_OFFLINEMSG:
    {
        qDebug() << "离线消息请求" << recvProtocol.access_token;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "离线消息请求");
        QLog::log(QString("<离线消息请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_OFFLINEMSG;
        // 验证输入
        if(checkDataLegal(recvProtocol))
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                sendProtocol.error = 0;
                sendProtocol.msgs = sqlModel.getOfflineMsgs(myTcpSocket->uid);
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "离线消息返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_CONTACTS:
    {
        qDebug() << "联系人列表请求" << recvProtocol.access_token;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "获取联系人列表请求");
        QLog::log(QString("<联系人列表请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_CONTACTS;
        // 验证输入
        if(checkDataLegal(recvProtocol))
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                sendProtocol.error = 0;
                sendProtocol.contacts = sqlModel.getContacts(myTcpSocket->uid);
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "联系人列表返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    case MsgProtocol::REQ_SENDMESSAGE:
    {
        qDebug() << "发送用户消息请求" << recvProtocol.access_token << recvProtocol.mid;
        addStatus(QString("%1 %2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->socket->peerName()), "发送用户消息请求");
        //QLog::log(QString("<发送用户消息请求> %1|%2").arg(myTcpSocket->socket->peerAddress().toString()).arg(myTcpSocket->username), "MainWindow");

        // 构造返回包
        sendProtocol.cmd = MsgProtocol::RET_SENDMESSAGE;
        sendProtocol.mid = recvProtocol.mid;
        // 验证输入
        if(checkDataLegal(recvProtocol) && myTcpSocket->uid == recvProtocol.self_uid && recvProtocol.self_uid != recvProtocol.target_uid)
        {
            if(myTcpSocket->oauthStatus == MyTcpSocket::OA_LOGINED && myTcpSocket->accessToken == recvProtocol.access_token)
            {
                bool contactExecFlag = true;
                // 如果是添加/接受好友消息则需要操作数据库的contact表
                if(recvProtocol.msg_type == MsgProtocol::MSG_ADDCONTACT)
                {
                    if(!sqlModel.addContact(recvProtocol.self_uid, recvProtocol.target_uid,recvProtocol.msg_text))
                    {
                        sendProtocol.error = MsgProtocol::ERR_SQLEXEC;
                        sendProtocol.msg = sqlModel.queryLastError();
                        contactExecFlag = false;
                    }
                }
                if(recvProtocol.msg_type == MsgProtocol::MSG_AGREECONTACT)
                {
                    if(!sqlModel.agreeContact(recvProtocol.self_uid, recvProtocol.target_uid))
                    {
                        sendProtocol.error = MsgProtocol::ERR_SQLEXEC;
                        sendProtocol.msg = sqlModel.queryLastError();
                        contactExecFlag = false;
                    }
                }

                // 上面执行成功才执行消息转发功能
                if(contactExecFlag)
                {
                    // 判断用户是否在线
                    MyTcpSocket * recverTcpSocket = nullptr;
                    foreach(MyTcpSocket * item, myTcpSocketList)
                    {
                        if(item->uid == recvProtocol.target_uid)
                        {
                            recverTcpSocket = item;
                            break;
                        }
                    }
                    // 在线则直接转发，否则存入离线消息
                    if(recverTcpSocket != nullptr)
                    {
                        MsgProtocol transferProtocol;
                        transferProtocol.cmd = MsgProtocol::REQ_RECVMESSAGE;
                        transferProtocol.self_uid = recvProtocol.self_uid;
                        transferProtocol.target_uid = recvProtocol.target_uid;
                        transferProtocol.msg_timestmp = recvProtocol.msg_timestmp;
                        transferProtocol.msg_text = recvProtocol.msg_text;
                        transferProtocol.msg_type = recvProtocol.msg_type;
                        transferProtocol.mid = recvProtocol.mid;

                        QByteArray transferData = secKey.length() > 0 ? transferProtocol.mkProtocol(secKey) : transferProtocol.mkProtocol();
                        qDebug() << "接收用户消息请求" << transferData;
                        recverTcpSocket->socket->write(recverTcpSocket->mkPkg(transferData));
                        recverTcpSocket->socket->flush();
                    }else
                    {
                        MdMsg msg;
                        msg.setMid(recvProtocol.mid);
                        msg.setSenderUid(recvProtocol.self_uid);
                        msg.setRecverUid(recvProtocol.target_uid);
                        msg.setContext(recvProtocol.msg_text);
                        msg.setTimestmp(recvProtocol.msg_timestmp);
                        msg.setType(recvProtocol.msg_type);
                        if(sqlModel.addOfflineMsg(msg))
                        {
                            sendProtocol.error = 0;
                            sendProtocol.mid = recvProtocol.mid;
                        }else
                        {
                            sendProtocol.error = MsgProtocol::ERR_SQLEXEC;
                            sendProtocol.msg = sqlModel.queryLastError();
                        }
                    }
                }
            }
            else
            {
                sendProtocol.error = MsgProtocol::ERR_OAUTH;
                sendProtocol.msg = "access_token error";
            }
        }
        else
        {
            sendProtocol.error = MsgProtocol::ERR_FORMAT;
            sendProtocol.msg = "data format error";
        }
        QByteArray sendData = secKey.length() > 0 ? sendProtocol.mkProtocol(secKey) : sendProtocol.mkProtocol();
        qDebug() << "发送用户消息返回" << sendData;
        myTcpSocket->socket->write(myTcpSocket->mkPkg(sendData));
        myTcpSocket->socket->flush();
        break;
    }
    }
}


// 槽函数 - 网络监听Accept错误
void MainWindow::on_tcp_accept_error(QAbstractSocket::SocketError socketError)
{
    addStatus(QString("网络错误：代码 %1").arg(QString::number(socketError)), "错误");
    QLog::error(QString("网络错误：代码 %1").arg(QString::number(socketError)), "MainWindow");
}

// ************************** 以下是私有函数 ********************************
void MainWindow::addStatus(QString text, QString title)
{
    if(title.isEmpty())
        ui->textBrowser->append(QString("%1 %2").arg(QTime::currentTime().toString("hh:mm:ss")).arg(text));
    else
        ui->textBrowser->append(QString("%1 【%2】 %3").arg(QTime::currentTime().toString("hh:mm:ss")).arg(title).arg(text));
}

// 私有功能函数 - 在客户端列表的tablewidget中添加一项
int MainWindow::addListItem(int uid, QString username, QString ipaddress)
{
    int curRow = ui->tableWidgetClients->rowCount();
    ui->tableWidgetClients->insertRow(curRow);
    ui->tableWidgetClients->setItem(curRow,0,new QTableWidgetItem(QString::number(uid)));
    ui->tableWidgetClients->setItem(curRow,1,new QTableWidgetItem(username));
    ui->tableWidgetClients->setItem(curRow,2,new QTableWidgetItem(ipaddress));
    ui->tableWidgetClients->setRowHeight(curRow, 26);
    return curRow;
}

// 私有函数 - 移除客户端列表中一项
void MainWindow::removeListItem(int uid)
{
    int rowCount = ui->tableWidgetClients->rowCount();
    for (int i = 0; i < rowCount; i++)
    {
        qDebug() << ui->tableWidgetClients->item(i, 0)->text() << ui->tableWidgetClients->item(i, 1)->text();
        if(ui->tableWidgetClients->item(i, 0)->text().toInt() == uid)
        {
            ui->tableWidgetClients->removeRow(i);
            break;
        }
    }
}

// 私有功能函数 - 产生随机字符串
QString MainWindow::getRandomString(int nLen)
{
    qsrand(QDateTime::currentMSecsSinceEpoch());
    const char ch[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int size = sizeof(ch);
    char* str = new char[nLen + 1];
    int num = 0;
    for (int nIndex = 0; nIndex < nLen; ++nIndex)
    {
        num = rand() % (size - 1);
        str[nIndex] = ch[num];
    }
    str[nLen] = '\0';
    QString res(str);
    return res;
}

// 私有功能函数 - 计算消息MID
QString MainWindow::getMsgMid(MdMsg mdMsg)
{
    QString strData = QString("%1%2%3%4%5").arg(mdMsg.getType()).arg(mdMsg.getSenderUid()).arg(mdMsg.getRecverUid()).arg(mdMsg.getTimestmp()).arg(mdMsg.getContext());
    return QCryptographicHash::hash(strData.toUtf8(), QCryptographicHash::Md5).toHex();
}

// 私有功能函数 - 验证输入参数是否合法
bool MainWindow::checkDataLegal(MsgProtocol msgProtocol)
{
    if(!msgProtocol.access_token.isEmpty() && msgProtocol.access_token.length()!= 32)
        return false;
    if(!msgProtocol.uname.isEmpty() && (msgProtocol.uname.length() > 24 || !msgProtocol.uname.contains(QRegExp("^[a-zA-Z]([A-Za-z0-9_]+)$"))))
        return false;
    if(!msgProtocol.passwd.isEmpty() && msgProtocol.passwd.length() !=32)
        return false;
    if(!msgProtocol.nickname.isEmpty() && msgProtocol.nickname.length() > 64)
        return false;
    if(!msgProtocol.headimg.isEmpty() && msgProtocol.headimg.length() >32)
        return false;
    if(!msgProtocol.mid.isEmpty() && msgProtocol.mid.length() !=32)
        return false;
    if(!msgProtocol.keyword.isEmpty() && msgProtocol.keyword.length() > 32)
        return false;
    return true;
}


// ************************** 以下是窗口重载函数 ********************************
// 关闭窗口消息重载
void MainWindow::closeEvent(QCloseEvent *e)
{
    // 先执行停止服务操作
    on_pushButtonStop_clicked();
    e->accept();
}

