#include "configwindow.h"
#include "ui_configwindow.h"
#include <QMessageBox>
#include <QRandomGenerator>
#include <QCryptographicHash>
#include <QDebug>

ConfigWindow::ConfigWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConfigWindow)
{
    ui->setupUi(this);
    setFixedSize(this->width(), this->height());

    ui->lineEditPort->setText(QString::number(Config::getListenPort()));
    ui->lineEditDbHost->setText(Config::getSqlHost());
    ui->lineEditDbPort->setText(QString::number(Config::getSqlPort()));
    ui->lineEditDbUsername->setText(Config::getSqlUsername());
    ui->lineEditDbPassword->setText(Config::getSqlPassword());
    ui->lineEditDbName->setText(Config::getSqlDbname());
    ui->textEditKey->setText(Config::getCSKey());
    ui->lineEditHeartbeat->setText(QString::number(Config::getMaxHeartbeat()));
}

ConfigWindow::~ConfigWindow()
{
    delete ui;
}

void ConfigWindow::on_pushButtonUpdate_clicked()
{
    if(!ui->lineEditPort->text().isEmpty())
    {
        Config::setListenPort(ui->lineEditPort->text().toInt());
    }
    if(!ui->lineEditDbHost->text().isEmpty())
    {
        Config::setSqlHost(ui->lineEditDbHost->text());
    }
    if(!ui->lineEditDbPort->text().isEmpty())
    {
        Config::setSqlPort(ui->lineEditDbPort->text().toInt());
    }
    if(!ui->lineEditDbUsername->text().isEmpty())
    {
        Config::setSqlUsername(ui->lineEditDbUsername->text());
    }
    if(!ui->lineEditDbPassword->text().isEmpty())
    {
        Config::setSqlPassword(ui->lineEditDbPassword->text());
    }
    if(!ui->lineEditDbName->text().isEmpty())
    {
        Config::setSqlDbname(ui->lineEditDbName->text());
    }
    if(!ui->lineEditHeartbeat->text().isEmpty())
    {
        Config::setMaxHeartbeat(ui->lineEditHeartbeat->text().toInt());
    }
    Config::setCSKey(ui->textEditKey->toPlainText());
    QMessageBox::information(this, "提示", "修改成功，重启服务器后生效");
}

void ConfigWindow::on_pushButtonCancel_clicked()
{
    close();
}

void ConfigWindow::on_pushButtonNewKey_clicked()
{
    // 生成256位随机密钥
    QByteArray btKey;
    btKey.resize(256);
    for(int i=0; i< 256; i = i + 4)
    {
        int randomNum = QRandomGenerator::global()->generate();
        btKey[i] = (uchar) (0x000000ff & randomNum);
        btKey[i+1] = (uchar) ((0x0000ff00 & randomNum) >> 8);
        btKey[i+2] = (uchar) ((0x00ff0000 & randomNum) >> 16);
        btKey[i+3] = (uchar) ((0xff000000 & randomNum) >> 24);
    }
    qDebug() << btKey.length();
    ui->textEditKey->setText(btKey.toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals));
}
