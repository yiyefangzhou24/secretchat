#include "qlog.h"
#include <QDebug>
#include <QDesktopServices>
#include <QUrl>

void QLog::warning(QString context, QString title)
{
    writeToFile(2, context, title);
}

void QLog::error(QString context, QString title)
{
    writeToFile(3, context, title);
}

void QLog::log(QString context, QString title)
{
    writeToFile(1, context, title);
}

bool QLog::open()
{
    return QDesktopServices::openUrl(QUrl::fromLocalFile(APP_FILE));
}

void QLog::writeToFile(int level, QString context, QString title)
{
    QString strLog;QString strLevel;
    switch (level) {
    case 1:
        strLevel = "[log]";
        break;
    case 2:
        strLevel = "[warning]";
        break;
    case 3:
        strLevel = "[error]";
        break;
    }

    if(title.isEmpty())
        strLog = QString("%1 %2 %3\n").arg(QTime::currentTime().toString("hh:mm:ss")).arg(strLevel).arg(context);
    else
        strLog = QString("%1 %2 <%3> %4\n").arg(QTime::currentTime().toString("hh:mm:ss")).arg(strLevel).arg(title).arg(context);

    qDebug() << strLog;
    QFile file(APP_FILE);
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QByteArray btLog(strLog.toUtf8());
    file.write(btLog);
    file.close();
}
