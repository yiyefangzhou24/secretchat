#ifndef CRCCHECKSUM_H
#define CRCCHECKSUM_H

#include <QByteArray>

typedef unsigned char u8;
typedef unsigned char byte;
typedef unsigned short u16;
typedef unsigned int u32;

class crcCheckSum
{
public:
    static quint16 crc16ForModbus(const QByteArray &data);

    static quint8 Crc8(const QByteArray &data);
    static quint16 Crc16(const QByteArray &data);
    static u32 Crc32(byte *_pBuff, u16 _size);
};

#endif // CRCCHECKSUM_H
