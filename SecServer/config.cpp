#include "config.h"
#include <QSettings>
#include <QDesktopServices>
#include <QUrl>
#include <QProcess>

Config::Config()
{

}

void Config::create()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SYSTEM");
    setting.setValue("listen", 6666);       //监听端口
    setting.setValue("maxheartbeat", 120);       //最大心跳间隔
    setting.endGroup();
    // 数据库配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("host", "");
    setting.setValue("port", 0);
    setting.setValue("username", "");
    setting.setValue("password", "");
    setting.setValue("dbname", "");
    setting.endGroup();
    // 安全性配置
    setting.beginGroup("SECURITY");
    setting.setValue("sckey", "");
    setting.endGroup();
}

void Config::setListenPort(int port)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SYSTEM");
    setting.setValue("listen", port);
    setting.endGroup();
}

int Config::getListenPort()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SYSTEM");
    int port = setting.value("listen", 0).toInt();
    setting.endGroup();
    return port;
}

bool Config::open()
{
    //return QDesktopServices::openUrl(QUrl::fromLocalFile(CONFIGNAME));
}


QString Config::getSqlHost()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    QString host = setting.value("host", "").toString();
    setting.endGroup();
    return host;
}

void Config::setSqlHost(QString host)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("host", host);
    setting.endGroup();
}

QString Config::getCSKey()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 通讯安全配置
    setting.beginGroup("SECURITY");
    QString host = setting.value("cskey", "").toString();
    setting.endGroup();
    return host;
}

void Config::setCSKey(QString key)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SECURITY");
    setting.setValue("cskey", key);
    setting.endGroup();
}

int Config::getSqlPort()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    int port = setting.value("port", 0).toInt();
    setting.endGroup();
    return port;
}

void Config::setSqlPort(int port)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("port", port);
    setting.endGroup();
}

QString Config::getSqlUsername()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    QString username = setting.value("username", "").toString();
    setting.endGroup();
    return username;
}

void Config::setSqlUsername(QString username)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("username", username);
    setting.endGroup();
}

QString Config::getSqlPassword()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    QString password = setting.value("password", "").toString();
    setting.endGroup();
    return password;
}

void Config::setSqlPassword(QString password)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("password", password);
    setting.endGroup();
}

QString Config::getSqlDbname()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    QString password = setting.value("dbname", "").toString();
    setting.endGroup();
    return password;
}

void Config::setSqlDbname(QString dbname)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SQLSERVER");
    setting.setValue("dbname", dbname);
    setting.endGroup();
}

int Config::getMaxHeartbeat()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    // 系统全局配置
    setting.beginGroup("SYSTEM");
    int maxHeartbeat = setting.value("maxheartbeat", 0).toInt();
    setting.endGroup();
    return maxHeartbeat;
}

void Config::setMaxHeartbeat(int maxHeartbeat)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SYSTEM");
    setting.setValue("maxheartbeat", maxHeartbeat);
    setting.endGroup();
}
