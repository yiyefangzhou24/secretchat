#ifndef SQLMODEL_H
#define SQLMODEL_H

// **
//
// Mysql数据库操作Model类
//    为提高效率，该类应设定为全局对象
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include "mduser.h"
#include "mdmsg.h"
#include "mdmsglist.h"
#include "mduserlist.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMap>
#include <QVariant>

class SqlModel
{
public:
    SqlModel();

    // 配置信息
    void config(QString host, int port, QString username, QString password, QString dbname);

    // 连接数据库
    bool connect();

    // 断开数据库连接
    void disconnect();

    // 获取数据库版本
    QString getDbVersion();

    // 获取上一次查询的错误信息
    QString queryLastError();

    // 获取上一次数据库连接的错误信息
    QString dbLastError();

    // 获取最后一次查询
    QString lastQuery();

    // 用户登录
    bool checkLogin(QString username, QString password);

    // 获取用户信息
    MdUser getUserInfo(int uid);

    // 获取用户信息
    MdUser getUserInfo(QString keyword);

    // 添加（注册）用户
    int addUser(MdUser mdUser);

    // 修改用户信息
    bool updateUser(QMap<QString, QVariant> data);

    // 获取好友信息
    MdUserList getContacts(int uid);

    // 删除联系人
    bool deleteContact(int self_uid, int target_uid);

    // 获取离线消息
    MdMsgList getOfflineMsgs(int uid);

    // 添加离线消息
    bool addOfflineMsg(MdMsg msg);

    // 添加好友
    bool addContact(int self_uid ,int target_uid, QString text);

    // 接受好友请求
    bool agreeContact(int self_uid, int target_uid);

private:

    // 检测是否连接，如果超时断开，则重新连接
    // 每一个查询函数之前都要调用该函数
    void keepAlive();

private:

    QSqlDatabase sqlDB;
    QSqlQuery query;

    QString dbHost;
    int dbPort;
    QString dbUsername;
    QString dbPassword;
    QString dbName;
};

#endif // SQLMODEL_H
