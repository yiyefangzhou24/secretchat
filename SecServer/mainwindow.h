#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>
#include <QList>
#include <QFileInfo>
#include <QCloseEvent>
#include <QMap>
#include <QMenu>
#include <QMessageBox>
#include <QTimer>
#include "mytcpsocket.h"
#include "msgprotocol.h"
#include "sqlmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void closeEvent(QCloseEvent *e);

private slots:
    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();

    void on_pushButtonConfig_clicked();

    // 网络通讯槽函数
    void on_tcp_connect();

    void on_tcp_read();

    void on_tcp_disconnect();

    void on_tcp_accept_error(QAbstractSocket::SocketError socketError);

    // 收到一个完整协议封包槽函数
    void on_pkg_ready(QByteArray data, MyTcpSocket * myTcpSocket);

    void on_tableWidgetClients_customContextMenuRequested(const QPoint &pos);

    void on_pushButtonLog_clicked();

private:

    // 私有函数 - 在状态显示窗口添加一行带有固定格式的数据
    void addStatus(QString text, QString title = "");

    // 私有函数 - 在客户端列表中添加一项
    int addListItem(int uid, QString username, QString ipaddress);

    // 私有函数 - 移除客户端列表中一项
    void removeListItem(int uid);

    // 私有函数 - 生成随机字符串（生成access_token）
    QString getRandomString(int nLen);

    // 私有函数 - 计算消息MID
    QString getMsgMid(MdMsg mdMsg);

    // 私有函数 - 输入参数验证
    bool checkDataLegal(MsgProtocol msgProtocol);

private:
    Ui::MainWindow *ui;

    QTcpServer * tcpServer = nullptr;

    QList<MyTcpSocket *> myTcpSocketList;

    SqlModel sqlModel;

    QByteArray secKey;

    QTimer * timer = new QTimer(this);
};
#endif // MAINWINDOW_H
