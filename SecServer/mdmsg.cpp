#include "mdmsg.h"

int MdMsg::getSenderUid()
{
    return this->sender_uid;
}

void MdMsg::setSenderUid(int uid)
{
    this->sender_uid = uid;
}

int MdMsg::getRecverUid()
{
    return this->recver_uid;
}

void MdMsg::setRecverUid(int uid)
{
    this->recver_uid = uid;
}

int MdMsg::getTimestmp()
{
    return this->timestmp;
}

void MdMsg::setTimestmp(int timestmp)
{
    this->timestmp = timestmp;
}

int MdMsg::getType()
{
    return this->type;
}

void MdMsg::setType(int type)
{
    this->type = type;
}

QString MdMsg::getContext()
{
    return this->context;
}

void MdMsg::setContext(QString context)
{
    this->context = context;
}

void MdMsg::setMid(QString mid)
{
    this->mid = mid;
}

QString MdMsg::getMid()
{
    return mid;
}

// 重载 = 运算符
MdMsg & MdMsg::operator=(MdMsg A) {
    this->sender_uid = A.getSenderUid();
    this->recver_uid = A.getRecverUid();
    this->timestmp = A.getTimestmp();
    this->context = A.getContext();
    this->type = A.getType();
    this->mid = A.getMid();
    return *this;
}
