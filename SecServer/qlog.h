#ifndef QLOG_H
#define QLOG_H


// **
//
// 日志消息类
//    该类所有成员函数均为静态函数
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#define APP_FILE    "ServerLogs.log"

#include <QString>
#include <QFile>
#include <QTime>
#include <QByteArray>

class QLog
{
public:
    static bool open();

    static void warning(QString context, QString title = "");

    static void error(QString context, QString title = "");

    static void log(QString context, QString title = "");

private:

    static void writeToFile(int level, QString context, QString title);
};

#endif // QLOG_H
