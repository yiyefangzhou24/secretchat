#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

// **
//
// 自定义的QTcpSocket类
//   添加自定义成员变量，用于记录客户端的用户名、访问令牌、登录时间等信息
//   继承MsgPackage类，用于处理接收的消息
//
// created by yiyefangzhou24
// created time 2023/1/5
//
// **

#include <QObject>
#include <QtNetwork>
#include "msgpackage.h"

class MyTcpSocket : public QObject, public MsgPackage
{
    Q_OBJECT
public:
    MyTcpSocket();

    // 重载比较运算符== ，socket句柄相同则类相同
    bool operator==(const MyTcpSocket &A);

    // 重载父类虚函数，接收到一次完整包的响应函数
    void pkgReady(QByteArray data) override;

signals:

    // 自定义信号，通知调用者收到一次完整的数据包
    void sigPkgReady(QByteArray data, MyTcpSocket * sender);

public:

    int oauthStatus;        // 认证状态

    int uid;                // 用户uid

    QString username;       // 用户名

    QString nickname;       // 用户昵称

    QString headimg;        // 用户头像

    QString accessToken;    // 访问令牌

    QTcpSocket * socket;      // socket句柄类

    int lastHeartbeatTime;  // 最后一次心跳时间戳

    static const int OA_INIT        = 0x00000010;      // 初始状态
    static const int OA_LOGINED     = 0x00000011;      // 登录成功状态
    static const int OA_LOGOUT      = 0x00000012;      // 退出状态
};

#endif // MYTCPSOCKET_H
