#include "netcore.h"
#include "qaes.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QCryptographicHash>

NetCore::NetCore()
{
    beatTimer = new QTimer(this);
    tcpSocket = new QTcpSocket(this);
    tcpSocket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    connect(tcpSocket, &QTcpSocket::connected, this , &NetCore::on_tcp_connect);
    connect(tcpSocket, &QTcpSocket::readyRead, this , &NetCore::on_tcp_read);
    connect(tcpSocket, &QTcpSocket::disconnected, this, &NetCore::on_tcp_disconnect);

    connect(beatTimer, &QTimer::timeout, this, &NetCore::on_tcp_heartbeat);
}

NetCore::~NetCore()
{
    disconnect(tcpSocket, &QTcpSocket::connected, this , &NetCore::on_tcp_connect);
    disconnect(tcpSocket, &QTcpSocket::readyRead, this , &NetCore::on_tcp_read);
    disconnect(tcpSocket, &QTcpSocket::disconnected, this, &NetCore::on_tcp_disconnect);
}

void NetCore::config(QString host, int port, QByteArray secKey, int beat_time)
{
    this->host = host;
    this->port = port;
    this->secKey = secKey;
    this->beat_time = beat_time;
}

void NetCore::connectToServer()
{
    tcpSocket->abort();
    tcpSocket->connectToHost(host, port, QIODevice::ReadWrite, QAbstractSocket::IPv4Protocol);
    // 设置连接超时
    QTimer * timer = new QTimer(this);
    connect(timer,&QTimer::timeout,[=](){
        if(tcpSocket->state() == QAbstractSocket::ConnectingState)
        {
            tcpSocket->close();
            emit this->connectTimeout();
        }
        timer->stop();
    });
    timer->start(3000);
    // 设置心跳包
}

void NetCore::close()
{
    tcpSocket->close();
}

void NetCore::loginRequest(QString username, QString password, int osType)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("uname", username);
    dataObject.insert("passwd", password);
    dataObject.insert("os_type", osType);
    rootObject.insert("cmd", NetCore::REQ_LOGIN);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
    this->username = username;
    this->password = password;
    this->osType = osType;
}

void NetCore::logoutRequest()
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", access_token);
    rootObject.insert("cmd", NetCore::REQ_LOGOUT);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
    this->username.clear();
    this->password.clear();
    this->access_token.clear();
    this->nickname.clear();
    this->headimg.clear();
    this->uid = 0;
    this->osType = 0;
}

void NetCore::getUserInfoRequest(QString keyword)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("keyword", keyword);
    dataObject.insert("access_token", this->access_token);
    rootObject.insert("cmd", NetCore::REQ_SEARCHUSER);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::updateUserInfoRequest(int uid, QMap<QString, QVariant> data)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", this->access_token);
    if(data.contains("username"))
    {
        dataObject.insert("uname", data.value("username").toString());
        username = data.value("username").toString();
    }
    if(data.contains("password"))
    {
        dataObject.insert("passwd", data.value("password").toString());
        password = data.value("password").toString();
    }
    if(data.contains("nickname"))
    {
        dataObject.insert("nickname", QString(data.value("nickname").toString().toUtf8().toBase64()));
        nickname = data.value("nickname").toString();
    }
    if(data.contains("headimg"))
    {
        dataObject.insert("headimg", data.value("headimg").toString());
        headimg = data.value("headimg").toString();
    }
    rootObject.insert("cmd", NetCore::REQ_UPDATEUSERINFO);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::getContactsRequest()
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", this->access_token);
    rootObject.insert("cmd", NetCore::REQ_CONTACTS);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::deleteContectRequest(int uid)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", this->access_token);
    dataObject.insert("target_uid", uid);
    rootObject.insert("cmd", NetCore::REQ_DELETECONTACT);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::registerRequest(QString username, QString nickname, QString password)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("uname", username);
    dataObject.insert("passwd", passwdEncrpyt(password));
    dataObject.insert("nickname", QString(nickname.toUtf8().toBase64()));
    rootObject.insert("cmd", NetCore::REQ_REGISTER);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::getofflinemsgRequest()
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", this->access_token);
    rootObject.insert("cmd", NetCore::REQ_OFFLINEMSG);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

void NetCore::sendMessageRequest(MdMsg mdMsg)
{
    QJsonObject rootObject, dataObject;
    dataObject.insert("access_token", this->access_token);
    dataObject.insert("type", mdMsg.getType());
    dataObject.insert("timestmp", mdMsg.getTimestmp());
    dataObject.insert("sender_uid", mdMsg.getSenderUid());
    dataObject.insert("recver_uid", mdMsg.getRecverUid());
    dataObject.insert("context", mdMsg.getContext());
    dataObject.insert("mid", mdMsg.getMid());
    rootObject.insert("cmd", NetCore::REQ_SENDMESSAGE);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}

// ***********************  以下是定时器槽函数 *****************************
void NetCore::on_tcp_heartbeat()
{
    qDebug() << "发送心跳请求";
    QJsonObject rootObject, dataObject;
    rootObject.insert("cmd", NetCore::REQ_HEARTBEAT);
    rootObject.insert("data", dataObject);
    QJsonDocument doc(rootObject);
    tcpSocket->write(mkPkg(encrypt(doc.toJson(QJsonDocument::Compact))));
}


// ***********************  以下是内部函数 *****************************

int NetCore::getUid()
{
    return this->uid;
}

QString NetCore::getUsername()
{
    return this->username;
}

QString NetCore::getPassword()
{
    return this->password;
}

int NetCore::getOsType()
{
    return this->osType;
}

QString NetCore::getNickname()
{
    return this->nickname;
}

QString NetCore::getHeadimg()
{
    return this->headimg;
}

QString NetCore::getMsgMid(MdMsg mdMsg)
{
    QString strData = QString("%1%2%3%4%5").arg(mdMsg.getType()).arg(mdMsg.getSenderUid()).arg(mdMsg.getRecverUid()).arg(mdMsg.getTimestmp()).arg(mdMsg.getContext());
    return QCryptographicHash::hash(strData.toUtf8(), QCryptographicHash::Md5).toHex();
}
// ***********************  以下是网络通讯槽函数 *****************************

// 服务器链接 - 接收数据槽函数
void NetCore::on_tcp_read()
{
    QTcpSocket *tmpTcpSocket = (QTcpSocket*)sender();
//    qDebug() << "接收消息" << tmpTcpSocket->readAll();
    unPkg(tmpTcpSocket->readAll());
}

void NetCore::on_tcp_connect()
{
//    qDebug() << "连接成功1";

    // 开启主动心跳（间隔1分钟）
    beatTimer->start(beat_time * 1000);
    emit this->connected();
}

void NetCore::on_tcp_disconnect()
{
//    qDebug() << "断开连接1";

    // 结束主动心跳
    beatTimer->stop();
    emit this->disconnected();
}

void NetCore::pkgReady(QByteArray en_data)
{
    // 先解密数据
    QByteArray data = decrypt(en_data);
    qDebug() << "收到完整数据包" << data;

    QJsonDocument jsonDocument = QJsonDocument::fromJson(data);
    if(!jsonDocument.isNull() && jsonDocument.isObject())
    {
        QJsonObject rootObject = jsonDocument.object();
        int cmd = rootObject.value("cmd").toInt();
        QJsonObject dataObject = rootObject.value("data").toObject();
        switch (cmd) {
        case NetCore::RET_HEARTBEAT:
        {
            qDebug() << "服务器心跳返回";
            break;
        }
        case NetCore::RET_LOGIN:
        {
            QJsonObject userObject = dataObject.value("data").toObject();
            access_token = userObject.value("access_token").toString();
            uid = userObject.value("self_uid").toInt();
            emit this->loginResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString());
            break;
        }
        case NetCore::RET_LOGOUT:
        {
            emit this->logoutResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString());
            break;
        }
        case NetCore::RET_UPDATEUSERINFO:
        {
            emit this->updateUserInfoResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString());
            break;
        }
        case NetCore::RET_SEARCHUSER:
        {
            MdUser mdUser;
            QJsonObject userObject = dataObject.value("data").toObject();
            mdUser.setUid(userObject.value("target_uid").toInt());
            mdUser.setUsername(userObject.value("uname").toString());
            mdUser.setNickname(QByteArray::fromBase64(userObject.value("nickname").toString().toUtf8()));
            mdUser.setHeadimg(userObject.value("headimg").toString());
            if(mdUser.getUid() == uid)
            {
                username = mdUser.getUsername();
                nickname = mdUser.getNickname();
                headimg = mdUser.getNickname();
            }
            emit this->getUserInfoResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString(), mdUser);
            break;
        }
        case NetCore::RET_CONTACTS:
        {
            MdUserList mdUserList;
            QJsonArray contactArray = dataObject.value("data").toArray();
            for(int i = 0 ; i < contactArray.size(); i ++)
            {
                QJsonObject userObject = contactArray.at(i).toObject();
                MdUser mdUser;
                mdUser.setUid(userObject.value("uid").toInt());
                mdUser.setUsername(userObject.value("uname").toString());
                mdUser.setNickname(QByteArray::fromBase64(userObject.value("nickname").toString().toUtf8()));
                mdUser.setHeadimg(userObject.value("headimg").toString());
                mdUserList.insert(mdUser);
            }
            emit this->getContactsResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString(), mdUserList);
            break;
        }
        case NetCore::RET_DELETECONTACT:
        {
            QJsonObject userObject = dataObject.value("data").toObject();
            emit this->deleteContactResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString(), userObject.value("target_uid").toInt());
            break;
        }
        case NetCore::RET_OFFLINEMSG:
        {
            MdMsgList mdMsgList;
            QJsonArray contactArray = dataObject.value("data").toArray();
            for(int i = 0 ; i < contactArray.size(); i ++)
            {
                QJsonObject msgObject = contactArray.at(i).toObject();
                MdMsg mdMsg;
                mdMsg.setSenderUid(msgObject.value("sender_uid").toInt());
                mdMsg.setRecverUid(msgObject.value("recver_uid").toInt());
                mdMsg.setContext(msgObject.value("context").toString());
                mdMsg.setTimestmp(msgObject.value("time").toInt());
                mdMsg.setType(msgObject.value("type").toInt());
                mdMsg.setMid(msgObject.value("mid").toString());
                mdMsgList.insert(mdMsg);
            }
            emit this->getofflinemsgResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString(), mdMsgList);
            break;
        }
        case NetCore::RET_REGISTER:
        {
            emit this->registerResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString());
            break;
        }
        case NetCore::RET_SENDMESSAGE:
        {
            QJsonObject msgObject = dataObject.value("data").toObject();
            emit this->sendMessageResponse(dataObject.value("error").toInt(), dataObject.value("msg").toString(), msgObject.value("mid").toString());
            break;
        }
        case NetCore::REQ_RECVMESSAGE:
        {
            MdMsg mdMsg;
            mdMsg.setSenderUid(dataObject.value("sender_uid").toInt());
            mdMsg.setRecverUid(dataObject.value("recver_uid").toInt());
            mdMsg.setType(dataObject.value("type").toInt());
            mdMsg.setContext(dataObject.value("context").toString());
            mdMsg.setTimestmp(dataObject.value("time").toInt());
            mdMsg.setMid(dataObject.value("mid").toString());
            emit this->recvMessageResponse(mdMsg);
            break;
        }
        }
    }
}

// ***********************  以下是私有函数 *****************************

QString NetCore::passwdEncrpyt(QString t)
{
    return QCryptographicHash::hash(t.toLatin1(),QCryptographicHash::Md5).toHex();
}

QByteArray NetCore::encrypt(QByteArray data)
{
    if(secKey.size() > 0)
    {
        QString iv("secChat@2023!!!");
        QByteArray hashKey = QCryptographicHash::hash(secKey, QCryptographicHash::Sha256);
        QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
        QByteArray encodeText = QAES::encrypt(data, hashKey, hashIV);
        return encodeText;
    }else
    {
        return data;
    }
}

QByteArray NetCore::decrypt(QByteArray data)
{
    if(secKey.size() > 0)
    {
        QString iv("secChat@2023!!!");
        QByteArray hashKey = QCryptographicHash::hash(secKey, QCryptographicHash::Sha256);
        QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
        QByteArray decodeText = QAES::decrypt(data, hashKey, hashIV);
        return decodeText;
    }else
    {
        return data;
    }
}
