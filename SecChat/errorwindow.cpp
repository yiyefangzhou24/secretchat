#include "errorwindow.h"
#include "ui_errorwindow.h"
#include "qlog.h"
#include <QTime>

ErrorWindow::ErrorWindow(QWidget *parent, NetCore * netCore) :
    QMainWindow(parent),
    ui(new Ui::ErrorWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;
    setFixedSize(this->width(), this->height());
}

ErrorWindow::~ErrorWindow()
{
    delete ui;
}

void ErrorWindow::on_pushButtonExit_clicked()
{
    exit(0);
}

void ErrorWindow::on_pushButton_clicked()
{
    netCore->connectToServer();
    // 如果缓存有用户密码，重新登录
    if(!netCore->getUsername().isEmpty() && !netCore->getPassword().isEmpty() && netCore->getOsType() != 0)
    {
        netCore->loginRequest(netCore->getUsername(), netCore->getPassword(), netCore->getOsType());
    }
    close();
}

void ErrorWindow::setText(QString text)
{
    ui->textBrowser->append(QString("%1 %2").arg(QTime::currentTime().toString("hh:mm:ss")).arg(text));
}
