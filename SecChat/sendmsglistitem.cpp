#include "sendmsglistitem.h"
#include "ui_sendmsglistitem.h"
#include <QMovie>

SendMsgListItem::SendMsgListItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SendMsgListItem)
{
    ui->setupUi(this);
    ui->labelMsg->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->pushButtonError->setVisible(false);
    QMovie * movie = new QMovie(":/main/msgload.gif");
    ui->labelLoad->setMovie(movie);
    movie->start();
    ui->labelLoad->setVisible(false);
}

SendMsgListItem::~SendMsgListItem()
{
    delete ui;
}

void SendMsgListItem::setData(QString text)
{
    ui->labelMsg->setText(text);
}

int SendMsgListItem::getHeightHint()
{
    return ui->labelMsg->sizeHint().height() + 18;
}

void SendMsgListItem::enterEvent(QEvent *event)
{
    ui->labelMsg->setStyleSheet("background-color: rgb(133, 208, 92);border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;border-top-left-radius: 16px;");
}

void SendMsgListItem::leaveEvent(QEvent *event)
{
   ui->labelMsg->setStyleSheet("background-color:rgb(149, 236, 105);border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;border-top-left-radius: 16px;");
}

void SendMsgListItem::setStatus(int status)
{
    switch (status) {
    case 0:
        ui->pushButtonError->setVisible(false);
        ui->labelLoad->setVisible(false);
        break;
    case 1:
        ui->pushButtonError->setVisible(true);
        ui->labelLoad->setVisible(false);
        break;
    case -1:
        ui->pushButtonError->setVisible(false);
        ui->labelLoad->setVisible(true);
        break;
    }
}
