#include "addcontactlistitem.h"
#include "ui_addcontactlistitem.h"

AddContactListItem::AddContactListItem(QWidget *parent,AddContactWindow * addContactWindow) :
    QWidget(parent),
    ui(new Ui::AddContactListItem)
{
    ui->setupUi(this);
    this->addContactWindow = addContactWindow;
    ui->labelStatus->hide();
}

AddContactListItem::~AddContactListItem()
{
    delete ui;
}

void AddContactListItem::setData(int uid, QString username, QString nickname, QString headimg)
{
    this->uid = uid;
    this->username = username;
    this->nickname = nickname;
    this->headimg = headimg;
    ui->labelNickname->setText(nickname);
    ui->labelUsername->setText(QString("@%1").arg(username));
}

void AddContactListItem::on_pushButtonAdd_clicked()
{
    qDebug() << uid;
    if(addContactWindow)
    {
        ui->pushButtonAdd->hide();
        ui->labelStatus->show();
        addContactWindow->addContactRequest(uid, username, nickname, headimg);
    }
}
