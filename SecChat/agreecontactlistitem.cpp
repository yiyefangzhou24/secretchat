#include "agreecontactlistitem.h"
#include "ui_agreecontactlistitem.h"

AgreeContactListItem::AgreeContactListItem(QWidget *parent, AgreeContactWindow * agreeContactWindow) :
    QWidget(parent),
    ui(new Ui::AgreeContactListItem)
{
    ui->setupUi(this);
    this->agreeContactWindow = agreeContactWindow;
}

AgreeContactListItem::~AgreeContactListItem()
{
    delete ui;
}

void AgreeContactListItem::setData(int uid, QString username, QString nickname, QString context, QString headimg, int status)
{
    this->uid = uid;
    this->username = username;
    this->nickname = nickname;
    this->headimg = headimg;
    ui->labelNickname->setText(nickname);
    ui->labelContext->setText(context);
    if(status > 0)
    {
        ui->pushButtonAgree->hide();
        ui->pushButtonRefuse->hide();
        ui->labelStatus->show();
        ui->labelStatus->setText("已同意");
    }
    else if(status == 0)
    {
        ui->pushButtonAgree->show();
        ui->pushButtonRefuse->show();
        ui->labelStatus->hide();
    }
    else
    {
        ui->pushButtonAgree->hide();
        ui->pushButtonRefuse->hide();
        ui->labelStatus->show();
        ui->labelStatus->setText("已拒绝");
    }
}

void AgreeContactListItem::on_pushButtonAgree_clicked()
{
    if(agreeContactWindow)
    {
        ui->pushButtonAgree->hide();
        ui->pushButtonRefuse->hide();
        ui->labelStatus->show();
        ui->labelStatus->setText("已同意");
        agreeContactWindow->agreeContactRequest(uid, username, nickname, headimg);
    }
}

void AgreeContactListItem::on_pushButtonRefuse_clicked()
{
//    parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget()->close();
    if(agreeContactWindow)
    {
        ui->pushButtonAgree->hide();
        ui->pushButtonRefuse->hide();
        ui->labelStatus->show();
        ui->labelStatus->setText("已拒绝");
        agreeContactWindow->refuseContactRequest(uid);
    }
}
