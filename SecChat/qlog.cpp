#include "qlog.h"
#include <QDebug>

void QLog::log(QString context, QString title)
{
    QString strLog;

    if(title.isEmpty())
        strLog = QString("%1 %2\n").arg(QTime::currentTime().toString("hh:mm:ss")).arg(context);
    else
        strLog = QString("%1 <%2> %3\n").arg(QTime::currentTime().toString("hh:mm:ss")).arg(title).arg(context);

    qDebug() << strLog;
    QFile file(APP_FILE);
    file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QByteArray btLog(strLog.toUtf8());
    file.write(btLog);
    file.close();
}
