#include "userinfowindow.h"
#include "ui_userinfowindow.h"
#include <QDebug>

UserInfoWindow::UserInfoWindow(QWidget *parent, QPoint pos) :
    QMainWindow(parent),
    ui(new Ui::UserInfoWindow)
{
    ui->setupUi(this);
    this->mainWindow = (MainWindow *)parent;
    setFixedSize(this->width(), this->height());
    move(pos);
    ui->labelNickname->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelUsername->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->labelUid->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->pushButtonUpdate->hide();
    setWindowFlags (Qt::FramelessWindowHint | Qt::Popup);
}

UserInfoWindow::~UserInfoWindow()
{
    delete ui;
}

void UserInfoWindow::setData(int uid, QString username, QString nickname, bool isEdit)
{
    ui->labelUid->setText(QString::number(uid));
    ui->labelNickname->setText(nickname);
    ui->labelUsername->setText(QString("@%1").arg(username));
    if(isEdit)
        ui->pushButtonUpdate->show();
}

void UserInfoWindow::on_pushButtonUpdate_clicked()
{
    if(mainWindow)
        mainWindow->editUserInfo();
    close();
}
