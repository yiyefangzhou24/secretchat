#ifndef MDUSERLIST_H
#define MDUSERLIST_H

// **
//
// 联系人列表Model类
//    仅用来存放联系人数据集数组
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QList>
#include "mduser.h"

class MdUserList
{
public:
    void insert(MdUser mdUser);

    MdUser at(int i);

    void clear();

    int size();

    MdUserList & operator=(MdUserList A);

private:

    QList<MdUser> userList;
};

#endif // MDUSERLIST_H
