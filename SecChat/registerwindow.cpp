#include "registerwindow.h"
#include "ui_registerwindow.h"
#include "qlog.h"
#include <QMessageBox>
#include <QCloseEvent>

RegisterWindow::RegisterWindow(QWidget *parent, NetCore * netCore) :
    QMainWindow(parent),
    ui(new Ui::RegisterWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;

    setFixedSize(this->width(), this->height());

    // 登录按钮响应回车事件
    ui->pushButtonRegister->setShortcut(Qt::Key_Return);

    if(netCore)
    {
        // 绑定登录返回信号
        connect(netCore, &NetCore::registerResponse, this, &RegisterWindow::on_register_response);
    }
}

RegisterWindow::~RegisterWindow()
{
    if(netCore)
    {
        // 接触绑定登录返回信号
        disconnect(netCore, &NetCore::registerResponse, this, &RegisterWindow::on_register_response);
    }
    delete ui;
}

void RegisterWindow::on_pushButtonRegister_clicked()
{
    if(!ui->lineEditUsername->text().isEmpty() && !ui->lineEditNickname->text().isEmpty() && !ui->lineEditPassword1->text().isEmpty() && ! ui->lineEditPassword2->text().isEmpty())
    {
        qDebug() << ui->lineEditUsername->text() << ui->lineEditNickname->text() << ui->lineEditPassword1->text() << ui->lineEditPassword2->text();
        if(ui->lineEditPassword1->text() != ui->lineEditPassword2->text())
        {
            QMessageBox::critical(this, "提示", "两次输入的密码不相同");
            ui->lineEditPassword1->setText("");
            ui->lineEditPassword2->setText("");
            return;
        }
        if(!ui->lineEditUsername->text().contains(QRegExp("^[a-zA-Z]([A-Za-z0-9_]+)$")))
        {
            QMessageBox::critical(this, "提示", "用户名首字母必须为英文，可包含数字和下划线，长度不超过24");
            ui->lineEditUsername->setText("");
            return;
        }
        // 发起注册请求
        netCore->registerRequest(ui->lineEditUsername->text(), ui->lineEditNickname->text(), ui->lineEditPassword1->text());
    }
    else
    {
        QMessageBox::critical(this, "错误", "请正确输入用户名、昵称和密码");
    }

}

void RegisterWindow::closeEvent(QCloseEvent* event)
{
    emit closeWind("RegisterWindow");
    event->accept();
}

// ***********************  以下是网络通讯槽函数 *****************************

void RegisterWindow::on_register_response(int error,  QString msg)
{
    if(error == 0)
    {
        qDebug() << "注册成功";
        QMessageBox::information(this, "提示", "注册成功");
        emit openLogin();
        close();
    }
    else
    {
        QLog::log(QString("注册用户错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "RegisterWindow");
        QMessageBox::critical(this, "提示", "注册失败");
    }
}
