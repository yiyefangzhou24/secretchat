#ifndef AGREECONTACTLISTITEM_H
#define AGREECONTACTLISTITEM_H

#include <QWidget>
#include "agreecontactwindow.h"

namespace Ui {
class AgreeContactListItem;
}

class AgreeContactListItem : public QWidget
{
    Q_OBJECT

public:
    explicit AgreeContactListItem(QWidget *parent = nullptr, AgreeContactWindow * agreeContactWindow = nullptr);
    ~AgreeContactListItem();

    void setData(int uid,QString username, QString nickname, QString context, QString headimg, int status);

private slots:
    void on_pushButtonAgree_clicked();

    void on_pushButtonRefuse_clicked();

private:
    Ui::AgreeContactListItem *ui;

    AgreeContactWindow * agreeContactWindow = nullptr;

    int uid = 0;

    QString username;

    QString nickname;

    QString headimg;
};

#endif // AGREECONTACTLISTITEM_H
