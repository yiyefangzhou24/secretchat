#ifndef MSGLOGWINDOW_H
#define MSGLOGWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QScrollBar>
#include <QDateTime>
#include "mdmsg.h"
#include "mdmsglist.h"
#include "sqlmodel.h"
#include "mainwindow.h"

namespace Ui {
class MsgLogWindow;
}

class MsgLogWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MsgLogWindow(QWidget *parent = nullptr, SqlModel * sqlModel = nullptr, int uid = 0);
    ~MsgLogWindow();

    void loadMsg(int start, int limit);

    void clearMsg();

private slots:
    void on_lineEditPage_textChanged(const QString &arg1);

    void on_pushButtonPrevious_clicked();

    void on_pushButtonNext_clicked();

    void on_lineEditPage_returnPressed();

    void on_actionClear_triggered();

    void on_actionExport_triggered();

    void on_pushButtonSearch_clicked();

private:
    Ui::MsgLogWindow *ui;

    SqlModel * sqlModel = nullptr;

    int uid = 0;        // 用户uid

    int maxPage = 0;    // 一共多少页

    int curPage = 0;    // 当前页码数

    int pageLimit = 50; // 每页记录条数（默认50）

    MainWindow * mainWindow = nullptr;
};

#endif // MSGLOGWINDOW_H
