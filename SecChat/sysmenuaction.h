#ifndef SYSMENUACTION_H
#define SYSMENUACTION_H

#include <QWidgetAction>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>

class SysMenuAction : public QWidgetAction
{
public:
    explicit SysMenuAction(QString text, QWidget *parent=0);

    void setUnread(bool isUnread);
protected:
    virtual QWidget *createWidget(QWidget *parent);

private:

    QString text;

    bool isUnread = false;

    QLabel* labelText = nullptr;

    QLabel* labelUnread = nullptr;
};

#endif // SYSMENUACTION_H
