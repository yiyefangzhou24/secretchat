#include "agreecontactwindow.h"
#include "ui_agreecontactwindow.h"
#include "agreecontactlistitem.h"
#include <QDebug>
#include <QMessageBox>
#include "qlog.h"
#include "qecdhmgr.h"

AgreeContactWindow::AgreeContactWindow(QWidget *parent, NetCore * netCore, SqlModel * sqlModel) :
    QMainWindow(parent),
    ui(new Ui::AgreeContactWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;
    this->sqlModel = sqlModel;
    this->mainWindow = (MainWindow *)parent;
    setFixedSize(this->width(), this->height());

    // 设置滚动条样式
    QFile file(":qss/verticalscrollbar.qss");
    file.open(QFile::ReadOnly);
    QString vsBarStyle = file.readAll();
    file.close();
    ui->listWidget->verticalScrollBar()->setStyleSheet(vsBarStyle);

    if(netCore)
    {
        // 绑定发送普通消息返回信号
        connect(netCore, &NetCore::sendMessageResponse, this, &AgreeContactWindow::on_sendmsg_response);
    }

    if(sqlModel)
    {
        // 读取申请列表
        MdUserList mdUserList = sqlModel->getContactApply();
        for(int i = 0; i< mdUserList.size(); i++)
        {
            qDebug() << mdUserList.at(i).getUid()
                     << mdUserList.at(i).getUsername()
                     << mdUserList.at(i).getNickname()
                     << mdUserList.at(i).getHeadimg()
                     << mdUserList.at(i).getStatus();
            addContact(mdUserList.at(i).getUid(),
                       mdUserList.at(i).getUsername(),
                       mdUserList.at(i).getNickname(),
                       "",
                       mdUserList.at(i).getHeadimg(),
                       mdUserList.at(i).getStatus());
        }
    }

    // 以下是测试代码
//    for(int i=0 ; i < 3 ; i++)
//    {
//        addContact(1000, "test", "test", "test", 0);
//        addContact(1000, "test", "test", "test", -1);
//        addContact(1000, "test", "test", "test", 1);
//    }
}

AgreeContactWindow::~AgreeContactWindow()
{
    if(netCore)
    {
        // 绑定发送普通消息返回信号
        disconnect(netCore, &NetCore::sendMessageResponse, this, &AgreeContactWindow::on_sendmsg_response);
    }
    delete ui;
}

void AgreeContactWindow::addContact(int uid, QString username, QString nickname, QString context, QString headimg, int status)
{
    QListWidgetItem * listWidgetItem = new QListWidgetItem(ui->listWidget);
    listWidgetItem->setSizeHint(QSize(-1, 60));
    AgreeContactListItem * agreeContactListItem = new AgreeContactListItem(ui->listWidget, this);
    agreeContactListItem->setData(uid, username, nickname, context, headimg, status);
    ui->listWidget->addItem(listWidgetItem);
    ui->listWidget->setItemWidget(listWidgetItem, agreeContactListItem);
}

void AgreeContactWindow::agreeContactRequest(int uid, QString username, QString nickname, QString headimg)
{
    qDebug() <<"agree" <<  uid << username << nickname << headimg;
    this->uid = uid;
    this->username = username;
    this->nickname = nickname;
    this->headimg = headimg;

    if(netCore && sqlModel)
    {
        // 计算对称密钥
        MdUser mdUser = sqlModel->getContactApplyInfo(uid);
        QECDHMgr qECDHMgr;
        if(!qECDHMgr.SetCurveName("secp384r1"))
        {
            QLog::log("系统不支持secp384r1加密方式，无法协商密钥", "AgreeContactWindow");
            QMessageBox::critical(this, "提示", "系统不支持secp384r1加密方式，无法协商密钥");
            return;
        }
        if(!qECDHMgr.SetKeyPair(mdUser.getRecverPubKey(), mdUser.getRecverPriKey()))
        {
            QLog::log(QString("通过好友申请过程中载入密钥对错误，无法协商密钥，信息：%1").arg(qECDHMgr.GetLastError()), "AgreeContactWindow");
            QMessageBox::critical(this, "提示", "通过好友申请过程中载入密钥对错误");
            return;
        }
        QString secKey = qECDHMgr.DeriveSharedSecret(mdUser.getSenderPubKey());
        qDebug() << "DH算法" << mdUser.getRecverPubKey() << mdUser.getRecverPriKey() << secKey;
        sqlModel->addContact(uid, secKey);
        // 修改数据库联系人状态
        QMap<QString, QVariant> data;
        data["status"] = 1;
        sqlModel->updateContactApply(uid, data);
        // 发送接受请求消息
        MdMsg mdMsg;
        mdMsg.setType(NetCore::MSG_AGREECONTACT);
        mdMsg.setSenderUid(netCore->getUid());
        mdMsg.setRecverUid(uid);
        mdMsg.setContext(mdUser.getRecverPubKey());
        mdMsg.setTimestmp((int)QDateTime::currentSecsSinceEpoch());
        mid = NetCore::getMsgMid(mdMsg);
        mdMsg.setMid(mid);
        netCore->sendMessageRequest(mdMsg);
    }

}

void AgreeContactWindow::refuseContactRequest(int uid)
{
    qDebug() <<"refuse" << uid;
    if(sqlModel)
    {
        // 修改数据库联系人状态
        QMap<QString, QVariant>data;
        data["status"] = -1;
        sqlModel->updateContactApply(uid, data);
    }

}

// ***********************  以下是网络通讯槽函数 *****************************
void AgreeContactWindow::on_sendmsg_response(int error, QString msg, QString mid)
{
    qDebug() << "消息发送成功" << "AgreeContactWindow" << mid;
    if( mid == this->mid)
    {
        if(error == 0)
        {
            qDebug() << "同意添加好友" << mid;
            if(mainWindow)
                mainWindow->addContact(uid, username, nickname, "", headimg, "");
        }
        else
        {
            QLog::log(QString("同意好友请求错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "AgreeContactWindow");
            QMessageBox::critical(this, "错误", msg);
        }
    }
}
