#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "netcore.h"
#include "sqlmodel.h"
#include "mduser.h"
#include "mduserlist.h"
#include "mdmsg.h"
#include "mdmsglist.h"
#include "sysmenuaction.h"
#include <QListWidgetItem>
#include <QMenu>
#include <QScrollBar>
#include <QDateTime>
#include <QMessageBox>
#include <QSound>
#include <QMap>
#include <QCryptographicHash>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr);
    ~MainWindow();

    enum msgType{msgSend, msgRecv};

    // 添加联系人
    void addContact(int uid, QString username, QString nickname, QString lastMsg, QString header, QString time);

    // 修改联系人
    void updateContact(int uid, QMap<QString, QVariant> dataMap);

    // 删除联系人
    void removeContact(int uid);

    // 添加聊天记录
    void addMessage(msgType type, QString mid, QString text, QString headimg, int status);

    // 清空聊天记录
    void removeAllMessage();

    // 对接收消息进行处理的控制器
    void recvMsgController(MdMsg mdMsg);

    // 更新个人信息
    void editUserInfo();

    // 添加文字输入
    void appendPlainText(QString text);

    // 控制工作区的显示
    void setWorkStationVisible(bool isVisable = false);
protected:

    virtual void closeEvent(QCloseEvent* event) override;

    bool eventFilter(QObject *obj, QEvent *event) override;

private slots:

    // 槽函数 - 注销信息响应
    void on_logout_response(int error, QString msg);

    // 槽函数 - 获取用户信息响应
    void on_getuserinfo_response(int error, QString msg, MdUser mdUser);

    // 槽函数 - 获取联系人列表响应
    void on_getcontacts_response(int error, QString msg, MdUserList mdUserList);

    // 槽函数 - 获取离线消息列表
    void on_getofflinemsg_response(int error, QString msg, MdMsgList mdMsgList);

    // 槽函数 - 发送普通消息响应
    void on_sendmsg_response(int error, QString msg, QString mid);

    // 槽函数 - 接收用户消息响应
    void on_recvmsg_response(MdMsg mdMsg);

    // 槽函数 - 删除联系人响应
    void on_deletecontact_response(int error, QString msg, int uid);

    // 槽函数 - 联系人列表菜单选中响应
    void on_contactmenu_triggered(QAction *action);

    // 槽函数 - 更新联系人信息
    void on_updateuserinfo_response(int error, QString msg);

    void on_pushButtonSend_clicked();

    void on_listWidgetContact_itemClicked(QListWidgetItem *item);

    void on_pushButtonHeadimg_clicked();

    void on_listWidgetContact_customContextMenuRequested(const QPoint &pos);

    void on_sysmenu_triggered(QAction *action);

    void on_pushButtonToolMsg_clicked();

    void on_lineEditSearch_textChanged(const QString &arg1);

    void on_pushButtonToolEmoji_clicked();

signals:

    void openLogin();               // 打开登录窗口信号

    void closeWind(QString title);  // 关闭窗口信号
private:
    Ui::MainWindow *ui;

    NetCore * netCore = nullptr;

    SqlModel sqlModel;

    int uid = 0;        // 用户uid

    QString nickname;   // 用户昵称

    QString username;   // 用户username

    QString header;     // 用户头像

    int curUserId = 0;  // 当前选中的用户uid

    QString curUserKey; // 当前选中用户的对称密钥

    SysMenuAction * agreeAction = nullptr;  //临时存放好友申请菜单项指针，用于设置未读
};

#endif // MAINWINDOW_H
