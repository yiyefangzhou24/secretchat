#include "mduserlist.h"

void MdUserList::insert(MdUser mdUser)
{
    userList.append(mdUser);
}

int MdUserList::size()
{
    return userList.size();
}

MdUser MdUserList::at(int i)
{
    return userList.at(i);
}

void MdUserList::clear()
{
    userList.clear();
}

// 重载 = 运算符
MdUserList & MdUserList::operator=(MdUserList A) {
    userList.clear();
    for(int i = 0 ; i < A.size(); i++ )
    {
        userList.append(A.at(i));
    }
    return *this;
}

