#include "loginwindow.h"
#include "ui_loginwindow.h"
#include "qlog.h"
#include "config.h"
#include "configwindow.h"
#include <QMessageBox>
#include <QCloseEvent>

LoginWindow::LoginWindow(QWidget *parent, NetCore * netCore) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;
    setFixedSize(this->width(), this->height());

    // 登录按钮响应回车事件
    ui->pushButtonLogin->setShortcut(Qt::Key_Return);

    if(netCore)
    {
        // 绑定登录返回信号
        connect(netCore, &NetCore::loginResponse, this, &LoginWindow::on_login_response);
    }

    // 以下为测试代码
//    ui->lineEditName->setText("admin");
//    ui->lineEditPasswd->setText("admin");
}

LoginWindow::~LoginWindow()
{
    if(netCore)
    {
        // 解除信号绑定
        disconnect(netCore, &NetCore::loginResponse, this, &LoginWindow::on_login_response);
    }
    delete ui;
}
void LoginWindow::on_pushButtonLogin_clicked()
{
    if(!ui->lineEditName->text().isEmpty() && !ui->lineEditPasswd->text().isEmpty())
    {
        qDebug() << ui->lineEditName->text() << ui->lineEditPasswd->text();
        // 发起登录请求
        netCore->loginRequest(ui->lineEditName->text(), passwdEncrpyt(ui->lineEditPasswd->text()), NetCore::OS_PCWIN);
    }
    else
    {
        QMessageBox::critical(this, "提示", "请正确输入用户名密码");
    }
}

void LoginWindow::on_pushButtonRegister_clicked()
{
    emit openRegister();
    close();
}


void LoginWindow::on_pushButtonConfig_clicked()
{
    ConfigWindow * configWindow = new ConfigWindow(this);
    configWindow->show();
}


QString LoginWindow::passwdEncrpyt(QString t)
{
    return QCryptographicHash::hash(t.toLatin1(),QCryptographicHash::Md5).toHex();
}

void LoginWindow::closeEvent(QCloseEvent* event)
{
    emit closeWind("LoginWindow");
    event->accept();
}


// ***********************  以下是网络通讯槽函数 *****************************

void LoginWindow::on_login_response(int error,  QString msg)
{
    if(error == 0)
    {
        qDebug() << "登录成功";
        emit openMain();
        close();
    }
    else
    {
        QLog::log(QString("登录错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "LoginWindow");
        QMessageBox::critical(this, "登录失败", msg);
    }
}
