#ifndef MDUSER_H
#define MDUSER_H

// **
//
// 用户信息Model类
//    仅用来存放用户数据集
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QString>

class MdUser
{
public:

    int getUid();

    void setUid(int uid);

    QString getUsername();

    void setUsername(QString username);

    QString getPassword();

    void setPassword(QString password);

    QString getNickname();

    void setNickname(QString nickname);

    QString getHeadimg();

    void setHeadimg(QString headimg);

    int getCreatetime();

    void setCreatetime(int createtime);

    QString getAccessToken();

    void setAccessToken(QString access_token);

    void setStatus(int status);

    int getStatus();

    void setSecKey(QString secKey);

    QString getSecKey();

    QString getSenderPubKey();

    void setSenderPubKey(QString senderPubKey);

    QString getSenderPriKey();

    void setSenderPriKey(QString senderPriKey);

    QString getRecverPubKey();

    void setRecverPubKey(QString recverPubKey);

    QString getRecverPriKey();

    void setRecverPriKey(QString recverPriKey);

    // 重载运算符=, 类赋值操作（变量赋值）
    MdUser & operator=(MdUser A);
private:

    int uid = 0;

    QString username;

    QString password;

    QString nickname;

    QString headimg;

    QString access_token;

    int createtime = 0;

    int status = 0;

    QString secKey;

    QString senderPubKey;

    QString senderPriKey;

    QString recverPubKey;

    QString recverPriKey;
};

#endif // MDUSER_H
