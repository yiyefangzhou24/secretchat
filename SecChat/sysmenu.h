#ifndef SYSMENU_H
#define SYSMENU_H

// **
//
// 系统菜单类
//    该类继承QMenu所有功能，仅为了实现向上弹出菜单并调整响应位置
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QMenu>

class SysMenu : public QMenu
{
    Q_OBJECT

public:
    explicit SysMenu(QPoint pos, int direction, QWidget* parent = 0);

    void showEvent(QShowEvent* event) override;

    static const int MENU_POPUP_RIGHT   = 0x0000001;
    static const int MENU_POPUP_LEFT    = 0x0000002;
    static const int MENU_POPDOWN_RIGHT = 0x0000003;
    static const int MENU_POPDOWN_LEFT  = 0X0000004;

private:

    QPoint offsetPos;

    int direction = 0;
};

#endif // SYSMENU_H
