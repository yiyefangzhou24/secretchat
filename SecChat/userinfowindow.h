#ifndef USERINFOWINDOW_H
#define USERINFOWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"

namespace Ui {
class UserInfoWindow;
}

class UserInfoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit UserInfoWindow(QWidget *parent = nullptr, QPoint pos = QPoint(0,0));
    ~UserInfoWindow();

    void setData(int uid, QString username, QString nickname, bool isEdit = false);
private slots:
    void on_pushButtonUpdate_clicked();

private:
    Ui::UserInfoWindow *ui;

    MainWindow * mainWindow = nullptr;
};

#endif // USERINFOWINDOW_H
