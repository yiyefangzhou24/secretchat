#include "basewindow.h"
#include "ui_basewindow.h"
#include "config.h"
#include "loginwindow.h"
#include "errorwindow.h"
#include "mainwindow.h"
#include "registerwindow.h"
#include "qlog.h"

#include "msglogwindow.h"

BaseWindow::BaseWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::BaseWindow)
{
    ui->setupUi(this);

    // 检测配置文件
    QFileInfo configFile(CONFIGNAME);
    if(!configFile.exists())
        Config::create();

    // 初始化网络
    netCore = new NetCore();
    QByteArray secKey;
    if(Config::getCSKey().size() > 0)
    {
        secKey = QByteArray::fromBase64(Config::getCSKey().toLatin1(), QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);
    }
    netCore->config(Config::getServerHost(), Config::getServerPort(), secKey, Config::getHeartbeat());
    connect(netCore, &NetCore::connected, this, &BaseWindow::on_tcp_connect);
    connect(netCore, &NetCore::disconnected, this, &BaseWindow::on_tcp_disconnect);
    connect(netCore, &NetCore::connectTimeout, this, &BaseWindow::on_tcp_connect_timeout);
    netCore->connectToServer();

    // 开启登录窗口
    on_ui_openlogin();

    // 开启窗口检活定时器，没有激活窗口则退出程序
    exitTimer = new QTimer(this);
    connect(exitTimer,&QTimer::timeout,[=](){
//        qDebug() << "Main:" <<windowMap["MainWindow"] <<"Login:"<< windowMap["LoginWindow"] <<"Register:"<< windowMap["RegisterWindow"];
        if(!windowMap["MainWindow"] && !windowMap["LoginWindow"] && !windowMap["RegisterWindow"])
        {
            exit(0);
        }
    });
    exitTimer->start(3000);


    // 以下是测试代码
//    MainWindow * mainWindow = new MainWindow(this);
//    mainWindow->show();
//    MsgLogWindow * msgLogWindow = new MsgLogWindow(this);
//    msgLogWindow->setAttribute(Qt::WA_DeleteOnClose);
//    msgLogWindow->show();
}

BaseWindow::~BaseWindow()
{
    netCore->close();
    delete netCore;
    delete ui;
}

// ***********************  以下是网络通讯槽函数 *****************************

void BaseWindow::on_tcp_connect()
{
    qDebug() << "连接成功";
    QLog::log("服务器连接成功", "BaseWindow");
}

void BaseWindow::on_tcp_disconnect()
{
    qDebug() << "断开连接";
    QLog::log("服务器连接断开", "BaseWindow");
    ErrorWindow * errorWindow = new ErrorWindow(this, netCore);
    errorWindow->setText("服务器断开连接");
    errorWindow->setAttribute(Qt::WA_DeleteOnClose);
    errorWindow->show();
}

void BaseWindow::on_tcp_connect_timeout()
{
    QLog::log("服务器连接超时", "BaseWindow");
    ErrorWindow * errorWindow = new ErrorWindow(this, netCore);
    errorWindow->setText("服务器连接超时");
    errorWindow->setAttribute(Qt::WA_DeleteOnClose);
    errorWindow->show();
}

// ***********************  以下是UI槽函数 *****************************

void BaseWindow::on_ui_openmain()
{
    MainWindow * mainWindow = new MainWindow(nullptr, netCore);
    mainWindow->setAttribute(Qt::WA_DeleteOnClose);
    mainWindow->show();
    windowMap["MainWindow"] = true;
    connect(mainWindow, &MainWindow::openLogin, this, &BaseWindow::on_ui_openlogin);
    connect(mainWindow, &MainWindow::closeWind, this, &BaseWindow::on_ui_close);
}


void BaseWindow::on_ui_openregister()
{
    RegisterWindow * registerWindow = new RegisterWindow(nullptr, netCore);
    registerWindow->setAttribute(Qt::WA_DeleteOnClose);
    registerWindow->show();
    windowMap["RegisterWindow"] = true;
    connect(registerWindow, &RegisterWindow::openLogin, this, &BaseWindow::on_ui_openlogin);
    connect(registerWindow, &RegisterWindow::closeWind, this, &BaseWindow::on_ui_close);
}

void BaseWindow::on_ui_openlogin()
{
    LoginWindow * loginWindow = new LoginWindow(nullptr, netCore);
    loginWindow->setAttribute(Qt::WA_DeleteOnClose);
    loginWindow->show();
    windowMap["LoginWindow"] = true;
    connect(loginWindow, &LoginWindow::openMain, this, &BaseWindow::on_ui_openmain);
    connect(loginWindow, &LoginWindow::openRegister, this, &BaseWindow::on_ui_openregister);
    connect(loginWindow, &LoginWindow::closeWind, this, &BaseWindow::on_ui_close);
}

void BaseWindow::on_ui_close(QString title)
{
    qDebug() << "窗口关闭信号" << title;
    if(title == "MainWindow")
        windowMap["MainWindow"] = false;
    else if(title == "RegisterWindow")
        windowMap["RegisterWindow"] = false;
    else if(title == "LoginWindow")
        windowMap["LoginWindow"] = false;
}
