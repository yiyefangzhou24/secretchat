#include "contactlistitem.h"
#include "ui_contactlistitem.h"
#include "userinfowindow.h"
#include <QDebug>

ContactListItem::ContactListItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ContactListItem)
{
    ui->setupUi(this);
    // 默认未读标志隐藏
    ui->labelUnread->hide();
}

ContactListItem::~ContactListItem()
{
    delete ui;
}

void ContactListItem::setData(int uid, QString username, QString nickname, QString lastMsg, QString headimg, QString time)
{
    this->uid = uid;
    this->username = username;
    this->nickname = nickname;
    this->headimg = headimg;
    ui->labelNickname->setText(nickname);
    ui->labelLastMsg->setText(lastMsg);
    ui->labelTime->setText(time);
}

void ContactListItem::setUnread(bool isShow)
{
    isShow ? ui->labelUnread->show() : ui->labelUnread->hide();
}

void ContactListItem::setLastMsg(QString msg)
{
    ui->labelLastMsg->setText(msg);
}

void ContactListItem::setLastTime(QString time)
{
    ui->labelTime->setText(time);
}


//void ContactListItem::enterEvent(QEvent *event)
//{
//    this->setStyleSheet("background-color:rgb(178, 178, 178)");
//}

//void ContactListItem::leaveEvent(QEvent *event)
//{
//    this->setStyleSheet("background-color:rgb(226, 226, 226)");
//}

void ContactListItem::on_pushButtonHeader_clicked()
{
    UserInfoWindow * userInfoWindow = new UserInfoWindow(this, cursor().pos());
    userInfoWindow->setData(uid, username, nickname);
    userInfoWindow->setAttribute(Qt::WA_DeleteOnClose);
    userInfoWindow->show();
}
