#ifndef REGISTERWINDOW_H
#define REGISTERWINDOW_H

#include <QMainWindow>
#include "netcore.h"

namespace Ui {
class RegisterWindow;
}

class RegisterWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit RegisterWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr);
    ~RegisterWindow();

protected:

    virtual void closeEvent(QCloseEvent* event) override;

private slots:
    void on_pushButtonRegister_clicked();

    // 槽函数 - 注册信号返回
    void on_register_response(int error, QString msg);

signals:

    void openLogin();       // 打开登录窗口信号

    void closeWind(QString  title);       // 关闭窗口信号

private:
    Ui::RegisterWindow *ui;

    NetCore * netCore = nullptr;
};

#endif // REGISTERWINDOW_H
