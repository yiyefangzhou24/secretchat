#ifndef CONFIG_H
#define CONFIG_H

// **
//
// 配置文件相关类
// 该类均为静态函数，可直接调用
//
// created by yiyefangzhou24
// created time 2023/1/5
//
// **


#include <QObject>
#include <QSettings>
#include <QDesktopServices>
#include <QUrl>

#define CONFIGNAME "SecretClient.cnf"

class Config
{
public:

    // 创建默认配置文件
    static void create();

    // 使用系统默认应用程序打开配置文件
    static void open();

    // 获取服务器地址
    static QString getServerHost();

    // 设置服务器地址
    static void setServerHost(QString host);

    // 获取服务器端口
    static int getServerPort();

    // 设置服务器端口
    static void setServerPort(int port);

    // 获取心跳间隔时间
    static int getHeartbeat();

    // 设置心跳间隔时间
    static void setHeartbeat(int heartbeat);

    // 设置通讯对称密钥
    static void setCSKey(QString csKey);

    // 获取通讯对称密钥
    static QString getCSKey();
};

#endif // CONFIG_H
