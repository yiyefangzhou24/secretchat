#include "configwindow.h"
#include "ui_configwindow.h"
#include <QMessageBox>

ConfigWindow::ConfigWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConfigWindow)
{
    ui->setupUi(this);
    ui->lineEditHost->setText(Config::getServerHost());
    ui->lineEditPort->setText(QString::number(Config::getServerPort()));
    ui->lineEditHeartbeat->setText(QString::number(Config::getHeartbeat()));
    ui->textEditKey->setText(Config::getCSKey());
}

ConfigWindow::~ConfigWindow()
{
    delete ui;
}

void ConfigWindow::on_pushButtonUpdate_clicked()
{
    if(!ui->lineEditHost->text().isEmpty())
    {
        Config::setServerHost(ui->lineEditHost->text());
    }
    if(!ui->lineEditPort->text().isEmpty())
    {
        Config::setServerPort(ui->lineEditPort->text().toInt());
    }
    if(!ui->lineEditHeartbeat->text().isEmpty())
    {
        Config::setHeartbeat(ui->lineEditHeartbeat->text().toInt());
    }
    Config::setCSKey(ui->textEditKey->toPlainText());
    QMessageBox::information(this, "提示", "修改成功，重启后生效");
}

void ConfigWindow::on_pushButtonCancel_clicked()
{
    close();
}
