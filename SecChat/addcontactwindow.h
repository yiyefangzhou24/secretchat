#ifndef ADDCONTACTWINDOW_H
#define ADDCONTACTWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QScrollBar>
#include "netcore.h"
#include "sqlmodel.h"

namespace Ui {
class AddContactWindow;
}

class AddContactWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AddContactWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr, SqlModel * sqlModel = nullptr);
    ~AddContactWindow();

    void addContact(int uid, QString username, QString nickname, QString headimg);

    void addContactRequest(int uid, QString username, QString nickname, QString headimg);

private slots:
    void on_pushButton_clicked();

    void on_getuserinfo_response(int error, QString msg, MdUser mdUser);

    void on_sendmsg_response(int error, QString msg, QString mid);

    void on_lineEdit_returnPressed();

private:
    Ui::AddContactWindow *ui;

    NetCore * netCore = nullptr;

    SqlModel * sqlModel = nullptr;

    QString mid;                    // 临时记录发起请求的消息标识
};

#endif // ADDCONTACTWINDOW_H
