#include "recvmsglistitem.h"
#include "ui_recvmsglistitem.h"
#include <QSize>

RecvMsgListItem::RecvMsgListItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RecvMsgListItem)
{
    ui->setupUi(this);
    ui->labelMsg->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->pushButtonError->setVisible(false);
}

RecvMsgListItem::~RecvMsgListItem()
{
    delete ui;
}

void RecvMsgListItem::setData(QString text)
{
    ui->labelMsg->setText(text);
}

int RecvMsgListItem::getHeightHint()
{
    return ui->labelMsg->sizeHint().height() + 18;
}

void RecvMsgListItem::enterEvent(QEvent *event)
{
    ui->labelMsg->setStyleSheet("background-color:rgb(230, 230, 230);border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;border-top-right-radius: 16px;");
}

void RecvMsgListItem::leaveEvent(QEvent *event)
{
    ui->labelMsg->setStyleSheet("background-color:rgb(255, 255, 255);border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;border-top-right-radius: 16px;");
}

void RecvMsgListItem::setStatus(int status)
{
    switch (status) {
    case 0:
        ui->pushButtonError->setVisible(false);
        break;
    case 1:
        ui->pushButtonError->setVisible(true);
        break;
    }
}
