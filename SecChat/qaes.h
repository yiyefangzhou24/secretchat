#ifndef QAES_H
#define QAES_H

#include <QByteArray>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <QString>
#include <QDebug>
#include <QScopedPointer>

class QAES
{
public:

    struct ScopedPointerCipherCtxDeleter
    {
        static inline void cleanup(EVP_CIPHER_CTX *pointer)
        {
            EVP_CIPHER_CTX_free(pointer);
        }
    };

    // 解密函数，失败返回空
    static QByteArray decrypt(QByteArray cyphertext, QByteArray key, QByteArray iv);

    // 加密函数，失败返回空
    static QByteArray encrypt(QByteArray plaintext, QByteArray key, QByteArray iv);

    // 获取最后一次错误信息
    static QString GetLastError();
};

#endif // QAES_H
