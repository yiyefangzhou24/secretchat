#ifndef BASEWINDOW_H
#define BASEWINDOW_H

// **
//
// 窗口父类
//    用该类实现后台进程，该窗口生命周期和程序相同，该类初始状态即为隐藏
//
//    注：可在该窗口处理全局消息
//
// 多窗口开发指南
//
//    BaseWindow（一级窗口，隐藏状态，无状态栏图标，主要控制程序启停和监视二级窗口状态）
//       |
//       | - LoginWindow      \
//       | = RegisterWindow     二级窗口（二级窗口不设定父窗口；同一时间有且只能有一个二级窗口打开，所有二级窗口关闭代表程序结束）
//       | - MainWindow       /
//
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QMainWindow>
#include "netcore.h"
#include <QTimer>
#include <QFileInfo>
#include <QList>

QT_BEGIN_NAMESPACE
namespace Ui { class BaseWindow; }
QT_END_NAMESPACE

class BaseWindow : public QMainWindow
{
    Q_OBJECT

public:
    BaseWindow(QWidget *parent = nullptr);
    ~BaseWindow();

private slots:

    void on_tcp_connect();       // 服务器连接 - 连接槽函数

    void on_tcp_disconnect();    // 服务器连接 - 断开连接槽函数

    void on_tcp_connect_timeout();  // 服务器连接 - 超时

    void on_ui_openmain();       // 打开主窗口信号

    void on_ui_openregister();   // 打开注册窗口信号

    void on_ui_openlogin();      // 打开登录窗口信号

    void on_ui_close(QString title);    // 关闭窗口信号

private:
    Ui::BaseWindow *ui;

    NetCore * netCore = nullptr;

    QTimer * exitTimer = nullptr;

    // 窗口状态表，记录二级窗口状态
    QMap<QString, bool> windowMap;
};
#endif // BASEWINDOW_H
