QT       += core gui network sql multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addcontactlistitem.cpp \
    addcontactwindow.cpp \
    agreecontactlistitem.cpp \
    agreecontactwindow.cpp \
    config.cpp \
    configwindow.cpp \
    contactlistitem.cpp \
    crcchecksum.cpp \
    edituserinfowindow.cpp \
    emojiwindow.cpp \
    errorwindow.cpp \
    loginwindow.cpp \
    main.cpp \
    basewindow.cpp \
    mainwindow.cpp \
    mdmsg.cpp \
    mdmsglist.cpp \
    mduser.cpp \
    mduserlist.cpp \
    msglogwindow.cpp \
    msgpackage.cpp \
    netcore.cpp \
    qaes.cpp \
    qecdhmgr.cpp \
    qlog.cpp \
    recvmsglistitem.cpp \
    registerwindow.cpp \
    sendmsglistitem.cpp \
    sqlmodel.cpp \
    sysmenu.cpp \
    sysmenuaction.cpp \
    userinfowindow.cpp

HEADERS += \
    addcontactlistitem.h \
    addcontactwindow.h \
    agreecontactlistitem.h \
    agreecontactwindow.h \
    basewindow.h \
    config.h \
    configwindow.h \
    contactlistitem.h \
    crcchecksum.h \
    edituserinfowindow.h \
    emojiwindow.h \
    errorwindow.h \
    loginwindow.h \
    mainwindow.h \
    mdmsg.h \
    mdmsglist.h \
    mduser.h \
    mduserlist.h \
    msglogwindow.h \
    msgpackage.h \
    netcore.h \
    qaes.h \
    qecdhmgr.h \
    qlog.h \
    recvmsglistitem.h \
    registerwindow.h \
    sendmsglistitem.h \
    sqlmodel.h \
    sysmenu.h \
    sysmenuaction.h \
    userinfowindow.h

FORMS += \
    addcontactlistitem.ui \
    addcontactwindow.ui \
    agreecontactlistitem.ui \
    agreecontactwindow.ui \
    basewindow.ui \
    configwindow.ui \
    contactlistitem.ui \
    edituserinfowindow.ui \
    emojiwindow.ui \
    errorwindow.ui \
    loginwindow.ui \
    mainwindow.ui \
    msglogwindow.ui \
    recvmsglistitem.ui \
    registerwindow.ui \
    sendmsglistitem.ui \
    userinfowindow.ui

LIBS += \
    $$PWD/openssl/lib/libcrypto.lib \
    $$PWD/openssl/lib/libssl.lib

INCLUDEPATH += \
    $$PWD/openssl/include

RC_ICONS=icon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res/res.qrc
