#include "sysmenu.h"


SysMenu::SysMenu(QPoint offsetPos, int direction, QWidget* parent) : QMenu(parent)
{
    this->offsetPos = offsetPos;
    this->direction = direction;
}

// 拦截弹出消息，移动到指定位置
void SysMenu::showEvent(QShowEvent* event)
{

    QPoint pos;
    QPoint posParent = parentWidget()->mapFromGlobal(this->pos());
    switch (direction) {
    case MENU_POPUP_RIGHT:
    {
        pos.setX(this->pos().x() + offsetPos.x());
        if(posParent.y() + this->sizeHint().height() > parentWidget()->height())
            pos.setY(this->pos().y() + offsetPos.y() - this->sizeHint().height());
        else
            pos.setY(this->pos().y() + offsetPos.y() + 50);
        break;
    }
    case MENU_POPUP_LEFT:
    {
        pos.setX(this->pos().x() + offsetPos.x() - this->sizeHint().width());
        pos.setY(this->pos().y() + offsetPos.y() - this->sizeHint().height());
        break;
    }
    case MENU_POPDOWN_RIGHT:
    {
        pos.setX(this->pos().x() + offsetPos.x());
        pos.setY(this->pos().y() + offsetPos.y());
        break;
    }
    case MENU_POPDOWN_LEFT:
    {
        pos.setX(this->pos().x() + offsetPos.x() - this->sizeHint().width());
        pos.setY(this->pos().y() + offsetPos.y());
        break;
    }
    }
    this->move(pos);
}
