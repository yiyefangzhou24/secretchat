#ifndef ERRORWINDOW_H
#define ERRORWINDOW_H

#include <QMainWindow>
#include "netcore.h"

namespace Ui {
class ErrorWindow;
}

class ErrorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ErrorWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr);
    ~ErrorWindow();

    void setText(QString text);

private slots:
    void on_pushButtonExit_clicked();

    void on_pushButton_clicked();

private:
    Ui::ErrorWindow *ui;

    NetCore * netCore = nullptr;
};

#endif // ERRORWINDOW_H
