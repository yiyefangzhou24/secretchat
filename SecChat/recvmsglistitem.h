#ifndef RECVMSGLISTITEM_H
#define RECVMSGLISTITEM_H

#include <QWidget>

namespace Ui {
class RecvMsgListItem;
}

class RecvMsgListItem : public QWidget
{
    Q_OBJECT

public:
    explicit RecvMsgListItem(QWidget *parent = nullptr);
    ~RecvMsgListItem();

    // 设置各项数据
    void setData(QString text);

    // 获取实际尺寸
    int getHeightHint();

    // 设置消息状态
    void setStatus(int status);

    void enterEvent(QEvent *event) override;

    void leaveEvent(QEvent *event) override;

private:
    Ui::RecvMsgListItem *ui;
};

#endif // RECVMSGLISTITEM_H
