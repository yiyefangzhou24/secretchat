#include "addcontactwindow.h"
#include "ui_addcontactwindow.h"
#include "qlog.h"
#include "addcontactlistitem.h"
#include "qecdhmgr.h"
#include <QMessageBox>

AddContactWindow::AddContactWindow(QWidget *parent, NetCore * netCore, SqlModel * sqlModel) :
    QMainWindow(parent),
    ui(new Ui::AddContactWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;
    this->sqlModel = sqlModel;
    setFixedSize(this->width(), this->height());

    // 设置滚动条样式
    QFile file(":qss/verticalscrollbar.qss");
    file.open(QFile::ReadOnly);
    QString vsBarStyle = file.readAll();
    file.close();
    ui->listWidget->verticalScrollBar()->setStyleSheet(vsBarStyle);

    if(netCore)
    {
        // 绑定获取用户信息返回信号
        connect(netCore, &NetCore::getUserInfoResponse, this, &AddContactWindow::on_getuserinfo_response);
        // 绑定发送普通消息返回信号
        connect(netCore, &NetCore::sendMessageResponse, this, &AddContactWindow::on_sendmsg_response);
    }

    // 以下是测试代码
//    for(int i = 0; i< 10; i++)
//    {
//        addContact(10000, "", "", "");
//    }
}

AddContactWindow::~AddContactWindow()
{
    if(netCore)
    {
        // 解除获取用户信息返回信号
        disconnect(netCore, &NetCore::getUserInfoResponse, this, &AddContactWindow::on_getuserinfo_response);
    }
    delete ui;
}


// ***********************  以下是网络通讯槽函数 *****************************
void AddContactWindow::on_getuserinfo_response(int error, QString msg, MdUser mdUser)
{
    if(error == 0)
    {
        qDebug() << "获取用户信息" << mdUser.getUid() << mdUser.getUsername() << mdUser.getNickname() << mdUser.getHeadimg();
        addContact(mdUser.getUid(), mdUser.getUsername(), mdUser.getNickname(), mdUser.getHeadimg());
    }
    else
    {
        QLog::log(QString("获取用户信息错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "AddContactWindow");
    }
}

void AddContactWindow::on_sendmsg_response(int error, QString msg, QString mid)
{
    if( mid == this->mid)
    {
        if(error == 0)
        {
            qDebug() << "发起好友申请" << mid;
    //        parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget()->parentWidget()->close();
        }
        else
        {
            QLog::log(QString("添加好友错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "AddContactListItem");
            QMessageBox::critical(this, "错误", msg);
        }
    }
}

// ***********************  以下是UI函数 *****************************

void AddContactWindow::addContactRequest(int uid, QString username, QString nickname, QString headimg)
{
    qDebug() << uid;
    // 生成非对称密钥对
    QECDHMgr qECDHMgr;
    if(!qECDHMgr.SetCurveName("secp384r1"))
    {
        QLog::log("系统不支持secp384r1加密方式，无法协商密钥", "AddContactWindow");
        QMessageBox::critical(this, "提示", "系统不支持secp384r1加密方式，无法协商密钥");
        return;
    }
    if(!qECDHMgr.GenerateKeys())
    {
        QLog::log(QString("生成密钥对错误，信息:%1").arg(qECDHMgr.GetLastError()), "AddContactWindow");
        QMessageBox::critical(this, "提示", "生成密钥对错误，无法协商密钥");
        return;
    }
    QString publicKey = qECDHMgr.GetPublicKey();
    QString privateKey = qECDHMgr.GetPrivateKey();
    qDebug() << "DH算法" << publicKey << privateKey;
    // 发送添加好友请求
    if(netCore)
    {
        MdMsg mdMsg;
        mdMsg.setSenderUid(netCore->getUid());
        mdMsg.setRecverUid(uid);
        mdMsg.setType(NetCore::MSG_ADDCONTACT);
        mdMsg.setTimestmp((int)QDateTime::currentSecsSinceEpoch());
        mdMsg.setContext(publicKey);
        mid = NetCore::getMsgMid(mdMsg);
        mdMsg.setMid(mid);
        netCore->sendMessageRequest(mdMsg);
    }
    // 写入数据库
    if(sqlModel)
    {
        sqlModel->addContactApply(uid, username, nickname, headimg, publicKey, privateKey,"","");
    }
}

void AddContactWindow::addContact(int uid, QString username, QString nickname, QString headimg)
{
    QListWidgetItem * listWidgetItem = new QListWidgetItem(ui->listWidget);
    listWidgetItem->setSizeHint(QSize(-1, 60));
    AddContactListItem * addContactListItem = new AddContactListItem(ui->listWidget, this);
    addContactListItem->setData(uid, username, nickname, headimg);
    ui->listWidget->addItem(listWidgetItem);
    ui->listWidget->setItemWidget(listWidgetItem, addContactListItem);
}

void AddContactWindow::on_lineEdit_returnPressed()
{
    on_pushButton_clicked();
}

void AddContactWindow::on_pushButton_clicked()
{
    ui->listWidget->clear();
    if(ui->lineEdit->text().isEmpty())
    {
        QMessageBox::critical(this, "提示", "请输入对方用戶UID/用户名");
        return;
    }
    if(netCore)
    {
        if(ui->lineEdit->text() == netCore->getUsername() || ui->lineEdit->text().toInt() == netCore->getUid())
        {
            QMessageBox::critical(this, "提示", "不能添加自己为好友");
            return;
        }
        netCore->getUserInfoRequest(ui->lineEdit->text());
    }

}

