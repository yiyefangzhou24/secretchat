#ifndef AGREECONTACTWINDOW_H
#define AGREECONTACTWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QScrollBar>
#include "netcore.h"
#include "sqlmodel.h"
#include "mduser.h"
#include "mduserlist.h"
#include "mainwindow.h"

namespace Ui {
class AgreeContactWindow;
}

class AgreeContactWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AgreeContactWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr, SqlModel * sqlModel = nullptr);
    ~AgreeContactWindow();

    void addContact(int uid, QString username, QString nickname, QString context, QString headimg, int status);

    void agreeContactRequest(int uid, QString username, QString nickname, QString headimg);

    void refuseContactRequest(int uid);

private slots:

    void on_sendmsg_response(int error, QString msg, QString mid);

private:
    Ui::AgreeContactWindow *ui;

    NetCore * netCore = nullptr;

    SqlModel * sqlModel = nullptr;

    MainWindow * mainWindow = nullptr;

    QString mid;                    // 临时记录发送请求的消息标识

    int uid;                        // 临时记录用户uid

    QString username;               // 临时记录用户名

    QString nickname;               // 临时记录昵称

    QString headimg;                // 临时记录用户头像
};

#endif // AGREECONTACTWINDOW_H
