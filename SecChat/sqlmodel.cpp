#include "sqlmodel.h"
#include <QDebug>
#include <QFile>
#include <QDateTime>

SqlModel::SqlModel()
{
    if(QSqlDatabase::contains("qt_sql_default_connection"))
    {
        sqlDB = QSqlDatabase::database("qt_sql_default_connection");
    }
    else {
        sqlDB = QSqlDatabase::addDatabase("QSQLITE");
    }
    query = QSqlQuery();
}

bool SqlModel::connect(QString dbname)
{
    if(dbname.isEmpty())
        sqlDB.setDatabaseName(this->dbname);
    else
        sqlDB.setDatabaseName(dbname);
    // 检测是否有该数据库，没有则需要创建并初始化表
    QFile file(dbname);
    if(file.exists())
    {
        return sqlDB.open() ? true : false;
    }
    else
    {
        return sqlDB.open() ? initTables() : false;
    }
}

void SqlModel::close()
{
    sqlDB.close();
}

QString SqlModel::queryLastError()
{
    return query.lastError().text();
}

QString SqlModel::dbLastError()
{
    return sqlDB.lastError().text();
}

bool SqlModel::initTables()
{
    keepAlive();
    QString sql1 = "CREATE TABLE \"message\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"mid\" TEXT NOT NULL, \"sender\" integer NOT NULL, \"recver\" integer NOT NULL, \"time\" integer NOT NULL,\"type\" integer NOT NULL, \"status\" integer NOT NULL,\"context\" TEXT)";
    QString sql2 = "CREATE INDEX \"message_index\" ON \"message\" (\"sender\", \"recver\", \"mid\")";
    QString sql3 = "CREATE TABLE \"contact_apply\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"sender_uid\" integer NOT NULL, \"sender_uname\" TEXT, \"sender_nickname\" TEXT, \"sender_header\" TEXT, \"sender_pubkey\" TEXT, \"sender_prikey\" TEXT, \"recver_pubkey\" TEXT, \"recver_prikey\" TEXT, \"status\" integer NOT NULL, \"time\" integer NOT NULL)";
    QString sql4 = "CREATE INDEX \"contact_apply_index\" ON \"contact_apply\" (\"sender\")";
    QString sql5 = "CREATE TABLE \"contact\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"uid\" integer NOT NULL, \"seckey\" TEXT)";
    QString sql6 = "CREATE INDEX \"contact_index\" ON \"contact\" (\"uid\")";
    return query.exec(sql1) && query.exec(sql2) && query.exec(sql3) && query.exec(sql4) && query.exec(sql5) && query.exec(sql6);
}

MdMsg SqlModel::getUserLastMsg(int uid)
{
    keepAlive();
    query.prepare("SELECT * FROM message WHERE sender=:uid OR recver=:uid ORDER BY time DESC limit 1");
    query.bindValue(":uid", uid);
    MdMsg mdMsg;
    if(query.exec() && query.next())
    {
        mdMsg.setMid(query.value("mid").toString());
        mdMsg.setType(query.value("type").toInt());
        mdMsg.setContext(query.value("context").toString());
        mdMsg.setTimestmp(query.value("time").toInt());
        mdMsg.setSenderUid(query.value("sender").toInt());
        mdMsg.setRecverUid(query.value("recver").toInt());
        mdMsg.setStatus(query.value("status").toInt());
    }
    return mdMsg;
}

MdMsgList SqlModel::getMsgs(int uid, int start, int limit)
{
    keepAlive();
    query.prepare("SELECT * FROM message WHERE sender=:uid OR recver=:uid ORDER BY time limit :limit OFFSET :offset");
    query.bindValue(":uid", uid);
    query.bindValue(":limit", limit);
    query.bindValue(":offset", start);
    MdMsgList mdMsgList;
    if(query.exec())
    {
        while(query.next())
        {
            MdMsg mdMsg;
            mdMsg.setMid(query.value("mid").toString());
            mdMsg.setType(query.value("type").toInt());
            mdMsg.setContext(query.value("context").toString());
            mdMsg.setTimestmp(query.value("time").toInt());
            mdMsg.setSenderUid(query.value("sender").toInt());
            mdMsg.setRecverUid(query.value("recver").toInt());
            mdMsg.setStatus(query.value("status").toInt());
            mdMsgList.insert(mdMsg);
        }
    }
    return mdMsgList;
}

MdMsgList SqlModel::searchMsgs(int uid, QString keyword)
{
    keepAlive();
    keyword = QString("%%1%").arg(keyword);
    qDebug() << keyword;
    query.prepare("SELECT * FROM message WHERE (sender=:uid OR recver=:uid) AND (context LIKE :keyword OR mid LIKE :keyword) ORDER BY time");
    query.bindValue(":uid", uid);
    query.bindValue(":keyword", keyword);
    MdMsgList mdMsgList;
    if(query.exec())
    {
        while(query.next())
        {
            MdMsg mdMsg;
            mdMsg.setMid(query.value("mid").toString());
            mdMsg.setType(query.value("type").toInt());
            mdMsg.setContext(query.value("context").toString());
            mdMsg.setTimestmp(query.value("time").toInt());
            mdMsg.setSenderUid(query.value("sender").toInt());
            mdMsg.setRecverUid(query.value("recver").toInt());
            mdMsg.setStatus(query.value("status").toInt());
            mdMsgList.insert(mdMsg);
        }
    }
    qDebug() << query.lastQuery() << query.lastError();
    return mdMsgList;
}

MdMsg SqlModel::getMsg(QString mid)
{
    keepAlive();
    query.prepare("SELECT * FROM message WHERE mid=:mid");
    query.bindValue(":mid", mid);
    MdMsg mdMsg;
    if(query.exec() && query.next())
    {
        mdMsg.setMid(query.value("mid").toString());
        mdMsg.setType(query.value("type").toInt());
        mdMsg.setContext(query.value("context").toString());
        mdMsg.setTimestmp(query.value("time").toInt());
        mdMsg.setSenderUid(query.value("sender").toInt());
        mdMsg.setRecverUid(query.value("recver").toInt());
        mdMsg.setStatus(query.value("status").toInt());
    }
    return mdMsg;
}

int SqlModel::countMsgs(int uid)
{
    keepAlive();
    query.prepare("SELECT COUNT(*) FROM message WHERE sender=:uid OR recver=:uid");
    query.bindValue(":uid", uid);
    if(query.exec() && query.next())
    {
        return query.value("count(*)").toString().toInt();
    }
    return 0;
}

bool SqlModel::clearMsgs(int uid)
{
    keepAlive();
    query.prepare("DELETE FROM message WHERE sender=:uid OR recver=:uid");
    query.bindValue(":uid", uid);
    return query.exec();
}

MdUserList SqlModel::getContactApply()
{
    keepAlive();
    query.prepare("SELECT * FROM contact_apply ORDER BY time DESC limit 10");
    MdUserList mdUserList;
    if(query.exec())
    {
        while(query.next())
        {
            MdUser mdUser;
            mdUser.setUid(query.value("sender_uid").toInt());
            mdUser.setUsername(query.value("sender_uname").toString());
            mdUser.setNickname(query.value("sender_nickname").toString());
            mdUser.setStatus(query.value("status").toInt());
            mdUser.setHeadimg(query.value("sender_header").toString());
            mdUserList.insert(mdUser);
        }
    }
    return mdUserList;
}

MdUser SqlModel::getContactApplyInfo(int uid)
{
    keepAlive();
    query.prepare("SELECT * FROM contact_apply WHERE sender_uid=:uid");
    query.bindValue(":uid", uid);
    MdUser mdUser;
    if(query.exec() && query.next())
    {
        mdUser.setUid(query.value("sender_uid").toInt());
        mdUser.setUsername(query.value("sender_uname").toString());
        mdUser.setNickname(query.value("sender_nickname").toString());
        mdUser.setHeadimg(query.value("sender_header").toString());
        mdUser.setSenderPubKey(query.value("sender_pubkey").toString());
        mdUser.setSenderPriKey(query.value("sender_prikey").toString());
        mdUser.setRecverPubKey(query.value("recver_pubkey").toString());
        mdUser.setRecverPriKey(query.value("recver_prikey").toString());
    }
    return mdUser;
}

bool SqlModel::addContactApply(int uid, QString username, QString nickname, QString header,
                               QString senderPubKey, QString senderPriKey ,QString recverPubKey, QString recverPriKey)
{
    keepAlive();
    query.prepare("DELETE FROM contact_apply WHERE sender_uid=:uid");
    query.bindValue(":uid", uid);
    query.exec();
    query.prepare("INSERT INTO contact_apply(sender_uid, sender_uname, sender_nickname, sender_header, sender_pubkey, sender_prikey,recver_pubkey, recver_prikey, status, time) VALUES (:uid, :username, :nickname, :header, :senderPubKey, :senderPriKey, :recverPubKey, :recverPriKey,0, :time)");
    query.bindValue(":uid", uid);
    query.bindValue(":username", username);
    query.bindValue(":nickname", nickname);
    query.bindValue(":header", header);
    query.bindValue(":senderPubKey", senderPubKey);
    query.bindValue(":senderPriKey", senderPriKey);
    query.bindValue(":recverPubKey", recverPubKey);
    query.bindValue(":recverPriKey", recverPriKey);
    query.bindValue(":time", (int)QDateTime::currentSecsSinceEpoch());
    return query.exec();
}

bool SqlModel::addContact(int uid, QString secKey)
{
    keepAlive();
    query.prepare("DELETE FROM contact WHERE uid=:uid");
    query.bindValue(":uid", uid);
    query.exec();
    query.prepare("INSERT INTO contact(uid, seckey) VALUES (:uid, :seckey)");
    query.bindValue(":uid", uid);
    query.bindValue(":seckey", secKey);
    return query.exec();
}

QString SqlModel::getContactKey(int uid)
{
    keepAlive();
    query.prepare("SELECT * FROM contact WHERE uid=:uid");
    query.bindValue(":uid", uid);
    if(query.exec() && query.next())
    {
        return query.value("seckey").toString();
    }
    return "";
}

bool SqlModel::updateContactApply(int uid, QMap<QString, QVariant> data)
{
    keepAlive();
    QStringList col,val;
    if(data.contains("username"))
    {
        col.append("sender_uname=?");
        val.append(data.value("username").toString());
    }
    if(data.contains("nickname"))
    {
        col.append("sender_nickname=?");
        val.append(data.value("nickname").toString());
    }
    if(data.contains("header"))
    {
        col.append("sender_header=?");
        val.append(data.value("header").toString());
    }
    if(data.contains("sender_pubkey"))
    {
        col.append("sender_pubkey=?");
        val.append(data.value("sender_pubkey").toString());
    }
    if(data.contains("sender_prikey"))
    {
        col.append("sender_prikey=?");
        val.append(data.value("sender_prikey").toString());
    }
    if(data.contains("recver_pubkey"))
    {
        col.append("recver_pubkey=?");
        val.append(data.value("recver_pubkey").toString());
    }
    if(data.contains("recver_prikey"))
    {
        col.append("recver_prikey=?");
        val.append(data.value("recver_prikey").toString());
    }
    if(data.contains("status"))
    {
        col.append("status=?");
        val.append(QString::number(data.value("status").toInt()));
    }
    // qDebug() << QString("UPDATE contact_apply SET %1 WHERE sender_uid = ?").arg(col.join(","));
    query.prepare(QString("UPDATE contact_apply SET %1 WHERE sender_uid = ?").arg(col.join(",")));
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(uid);
    return query.exec();
}

bool SqlModel::updateMsg(QString mid, QMap<QString, QVariant> data)
{
    keepAlive();
    QStringList col,val;
    if(data.contains("sender"))
    {
        col.append("sender=?");
        val.append(QString::number(data.value("sender").toInt()));
    }
    if(data.contains("recver"))
    {
        col.append("recver=?");
        val.append(QString::number(data.value("recver").toInt()));
    }
    if(data.contains("time"))
    {
        col.append("time=?");
        val.append(QString::number(data.value("time").toInt()));
    }
    if(data.contains("type"))
    {
        col.append("type=?");
        val.append(QString::number(data.value("type").toInt()));
    }
    if(data.contains("status"))
    {
        col.append("status=?");
        val.append(QString::number(data.value("status").toInt()));
    }
    if(data.contains("context"))
    {
        col.append("context=?");
        val.append(data.value("context").toString());
    }
    // qDebug() << QString("UPDATE contact_apply SET %1 WHERE sender_uid = ?").arg(col.join(","));
    query.prepare(QString("UPDATE message SET %1 WHERE mid = ?").arg(col.join(",")));
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(mid);
    return query.exec();
}

bool SqlModel::addMsg(MdMsg mdMsg)
{
    keepAlive();
    query.prepare("INSERT INTO message(mid, sender, recver, time, type,status, context) VALUES (:mid, :sender, :recver, :time, :type,:status, :context)");
    query.bindValue(":mid", mdMsg.getMid());
    query.bindValue(":sender", mdMsg.getSenderUid());
    query.bindValue(":recver", mdMsg.getRecverUid());
    query.bindValue(":time", mdMsg.getTimestmp());
    query.bindValue(":type", mdMsg.getType());
    query.bindValue(":status", mdMsg.getStatus());
    query.bindValue(":context", mdMsg.getContext());
    return query.exec();
}


void SqlModel::keepAlive()
{
    if(!sqlDB.isOpen())
    {
        close();
        connect();
    }
}
