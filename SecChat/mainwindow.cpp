#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qlog.h"
#include "contactlistitem.h"
#include "recvmsglistitem.h"
#include "sendmsglistitem.h"
#include "userinfowindow.h"
#include "addcontactwindow.h"
#include "agreecontactwindow.h"
#include "sysmenu.h"
#include "msglogwindow.h"
#include "edituserinfowindow.h"
#include "emojiwindow.h"
#include "qecdhmgr.h"
#include "qaes.h"
#include "configwindow.h"

MainWindow::MainWindow(QWidget *parent, NetCore * netCore) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;
    ui->listWidgetMsg->setResizeMode(QListWidget::Adjust);

    // 开启联系人列表右键菜单
    ui->listWidgetContact->setContextMenuPolicy(Qt::CustomContextMenu);

    // 设置滚动条样式
    QFile file(":qss/verticalscrollbar.qss");
    file.open(QFile::ReadOnly);
    QString vsBarStyle = file.readAll();
    file.close();
    ui->listWidgetContact->verticalScrollBar()->setStyleSheet(vsBarStyle);
    ui->listWidgetMsg->verticalScrollBar()->setStyleSheet(vsBarStyle);

    // 设置系统菜单样式
    ui->labelMenuUnread->hide();
    SysMenu * sysMenu = new SysMenu(QPoint(50, 0), SysMenu::MENU_POPUP_RIGHT, this);
    SysMenuAction * logoutAction= new SysMenuAction("退出登录", this);
    SysMenuAction * sysAction= new SysMenuAction("系统设置", this);
    SysMenuAction * addAction= new SysMenuAction("添加好友", this);
    agreeAction= new SysMenuAction("好友请求", this);
    logoutAction->setData("logoutAction");
    addAction->setData("addAction");
    agreeAction->setData("agreeAction");
    sysAction->setData("sysAction");
    sysMenu->addAction(addAction);
    sysMenu->addAction(agreeAction);
    sysMenu->addAction(sysAction);
    sysMenu->addAction(logoutAction);
    ui->pushButtonMenu->setMenu(sysMenu);
    connect(sysMenu, &QMenu::triggered, this, &MainWindow::on_sysmenu_triggered);

    // 隐藏工作区域
    setWorkStationVisible(false);

    // 定义回车响应
    ui->plainTextEdit->installEventFilter(this);

    // 绑定获取用户信息信号
    connect(netCore, &NetCore::getUserInfoResponse, this, &MainWindow::on_getuserinfo_response);
    // 绑定获取联系人列表信号
    connect(netCore, &NetCore::getContactsResponse, this, &MainWindow::on_getcontacts_response);
    // 绑定获取离线消息信号
    connect(netCore, &NetCore::getofflinemsgResponse, this, &MainWindow::on_getofflinemsg_response);
    // 绑定发送普通消息返回信号
    connect(netCore, &NetCore::sendMessageResponse, this, &MainWindow::on_sendmsg_response);
    // 绑定接收用户消息信号
    connect(netCore, &NetCore::recvMessageResponse, this, &MainWindow::on_recvmsg_response);
    // 绑定注销消息信号
    connect(netCore, &NetCore::logoutResponse, this, &MainWindow::on_logout_response);
    // 绑定删除联系人消息信号
    connect(netCore, &NetCore::deleteContactResponse, this, &MainWindow::on_deletecontact_response);
    // 绑定更新用户信息返回信号
    connect(netCore, &NetCore::updateUserInfoResponse, this, &MainWindow::on_updateuserinfo_response);

    if(netCore && netCore->getUid()> 0)
    {
        // 初始化数据库
        if(!sqlModel.connect(QString("%1.db").arg(netCore->getUid())))
        {
            QLog::log(QString("SQLITE错误 | addMsg :%1 %2").arg(sqlModel.dbLastError()).arg(sqlModel.queryLastError()), "MainWindow");
        }
        // 获取个人信息
        netCore->getUserInfoRequest(netCore->getUsername());

        // 获取联系人列表
        netCore->getContactsRequest();
    }

    // 以下是测试代码
//    QString emoji(0x231A);
//    for (int i = 0; i <5; i++) {
//        addMessage(msgSend, emoji, "");
//    }
}

MainWindow::~MainWindow()
{
    if(netCore)
    {
        disconnect(netCore, &NetCore::getUserInfoResponse, this, &MainWindow::on_getuserinfo_response);
        disconnect(netCore, &NetCore::getContactsResponse, this, &MainWindow::on_getcontacts_response);
        disconnect(netCore, &NetCore::getofflinemsgResponse, this, &MainWindow::on_getofflinemsg_response);
        disconnect(netCore, &NetCore::sendMessageResponse, this, &MainWindow::on_sendmsg_response);
        disconnect(netCore, &NetCore::recvMessageResponse, this, &MainWindow::on_recvmsg_response);
        disconnect(netCore, &NetCore::logoutResponse, this, &MainWindow::on_logout_response);
        disconnect(netCore, &NetCore::updateUserInfoResponse, this, &MainWindow::on_updateuserinfo_response);
    }
    sqlModel.close();
    delete ui;
}

// ***********************  以下是网络通讯槽函数 *****************************
void MainWindow::on_getuserinfo_response(int error, QString msg, MdUser mdUser)
{
    if(error == 0)
    {
        if(mdUser.getUid() == netCore->getUid())
        {
            qDebug() << "获取自己的用户信息" << mdUser.getUid() << mdUser.getUsername() << mdUser.getNickname() << mdUser.getHeadimg();
            uid = mdUser.getUid();
            username = mdUser.getUsername();
            nickname = mdUser.getNickname();
            header = mdUser.getHeadimg();
        }
        else
        {
            qDebug() << "更新联系人列表" << mdUser.getUid() << mdUser.getUsername() << mdUser.getNickname() << mdUser.getHeadimg();
            QMap<QString,QVariant> map;
            map["username"] = mdUser.getUsername();
            map["nickname"] = mdUser.getNickname();
            map["header"] = mdUser.getHeadimg();
            sqlModel.updateContactApply(mdUser.getUid(), map);
        }
    }
    else
    {
        QLog::log(QString("获取用户信息错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_updateuserinfo_response(int error, QString msg)
{
    if(error == 0)
    {
        username = netCore->getUsername();
        nickname = netCore->getNickname();
        header = netCore->getHeadimg();
    }
}

void MainWindow::on_getcontacts_response(int error, QString msg, MdUserList mdUserList)
{
    if(error == 0)
    {
        for(int i = 0; i< mdUserList.size(); i++)
        {
//            qDebug() << "获取联系人列表" << mdUserList.at(i).getUid() << mdUserList.at(i).getNickname() << mdUserList.at(i).getHeadimg();
            MdMsg mdMsg = sqlModel.getUserLastMsg(mdUserList.at(i).getUid());
            QDateTime time;QString stime;
            if(mdMsg.getTimestmp() > 0)
            {
                time.setTime_t(mdMsg.getTimestmp());
                stime = time.toString("yyyy/MM/dd");
            }
            addContact(mdUserList.at(i).getUid(),
                       mdUserList.at(i).getUsername(),
                       mdUserList.at(i).getNickname(),
                       mdMsg.getContext(),
                       "",
                       stime);
        }
        // 联系人列表加载完毕后，发送获取离线消息
        netCore->getofflinemsgRequest();
    }
    else
    {
        QLog::log(QString("获取联系人列表错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_getofflinemsg_response(int error, QString msg, MdMsgList mdMsgList)
{
    if(error == 0)
    {
        for(int i = 0; i< mdMsgList.size(); i++)
        {
            recvMsgController(mdMsgList.at(i));
        }
    }
    else
    {
        QLog::log(QString("获取离线消息错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_sendmsg_response(int error, QString msg, QString mid)
{
    // 修改数据库标志位
    QMap<QString, QVariant> data;
    data["status"] = error == 0 ? 0 : 1;
    if(!sqlModel.updateMsg(mid, data))
    {
        QString lastError = sqlModel.queryLastError();
        qDebug() << "发送消息写入数据库错误 | 信息:" << lastError;
        QLog::log(QString("发送消息写入数据库错误 | 信息:%1").arg(lastError), "MainWindow");
    }

    // 更新UI显示
    for (int i = ui->listWidgetMsg->count() -1 ; i >= 0; i--)
    {
        if(ui->listWidgetMsg->item(i)->data(Qt::UserRole).toString() == mid)
        {
            SendMsgListItem * sendMsgListItem = (SendMsgListItem *)ui->listWidgetMsg->itemWidget(ui->listWidgetMsg->item(i));
            error == 0 ? sendMsgListItem->setStatus(0) : sendMsgListItem->setStatus(1);
            break;
        }
    }

    // 打印日志
    if(error ==0)
    {
        qDebug() << "消息发送成功" << mid;
    }else
    {
        QLog::log(QString("发送消息错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_deletecontact_response(int error, QString msg, int uid)
{
    if(error ==0)
    {
        qDebug() << "删除联系人成功" << uid;
        removeContact(uid);
    }
    else
    {
        QLog::log(QString("删除联系人错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_logout_response(int error, QString msg)
{
    if(error == 0)
    {
        emit this->openLogin();
        close();
    }
    else
    {
        QLog::log(QString("注销错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "MainWindow");
    }
}

void MainWindow::on_recvmsg_response(MdMsg mdMsg)
{
    recvMsgController(mdMsg);
}

// ***********************  以下是UI槽函数 *****************************
void MainWindow::on_sysmenu_triggered(QAction *action)
{
    qDebug() << action->data();
    if(action->data().toString() == "logoutAction")
    {
        netCore->logoutRequest();
    }
    else if(action->data().toString() == "addAction")
    {
        AddContactWindow * addContactWindow = new AddContactWindow(this, netCore, &sqlModel);
        addContactWindow->setAttribute(Qt::WA_DeleteOnClose);
        addContactWindow->show();
    }
    else if(action->data().toString() == "agreeAction")
    {
        ui->labelMenuUnread->hide();
        agreeAction->setUnread(false);
        AgreeContactWindow * agreeContactWindow = new AgreeContactWindow(this, netCore, &sqlModel);
        agreeContactWindow->setAttribute(Qt::WA_DeleteOnClose);
        agreeContactWindow->show();
    }
    else if(action->data().toString() == "sysAction")
    {
        ConfigWindow * configWindow = new ConfigWindow(this);
        configWindow->setAttribute(Qt::WA_DeleteOnClose);
        configWindow->show();
    }
}

void MainWindow::on_contactmenu_triggered(QAction *action)
{
    if(action->text() == "删除联系人")
    {
        int curRow = ui->listWidgetContact->currentRow();
        qDebug() << "删除联系人" << ui->listWidgetContact->item(curRow)->data(Qt::UserRole).toInt();
        netCore->deleteContectRequest(ui->listWidgetContact->item(curRow)->data(Qt::UserRole).toInt());
    }
}

void MainWindow::on_pushButtonSend_clicked()
{
    if(!ui->plainTextEdit->toPlainText().isEmpty() && curUserId > 0)
    {
        // 创建消息结构体
        qDebug() << ui->plainTextEdit->toPlainText();
        MdMsg mdMsg, mdMsgEn;
        mdMsg.setSenderUid(netCore->getUid());
        mdMsg.setType(NetCore::MSG_TEXT);
        mdMsg.setRecverUid(curUserId);
        mdMsg.setContext(ui->plainTextEdit->toPlainText());
        mdMsg.setTimestmp((int)QDateTime::currentSecsSinceEpoch());
        mdMsg.setMid(NetCore::getMsgMid(mdMsg));
        mdMsg.setStatus(-1);
        // 创建加密消息结构体
        mdMsgEn = mdMsg;
        QByteArray hashKey = QCryptographicHash::hash(curUserKey.toLatin1(), QCryptographicHash::Sha256);
        QByteArray hashIV = QCryptographicHash::hash(mdMsg.getMid().toLatin1(), QCryptographicHash::Md5);
        QByteArray encodeText = QAES::encrypt(mdMsg.getContext().toUtf8(), hashKey, hashIV);
        //qDebug() << "密钥" <<hashKey<< "iv"<< hashIV << "密文" << encodeText.toBase64(QByteArray::Base64UrlEncoding);
        mdMsgEn.setContext(QString::fromLatin1(encodeText.toBase64(QByteArray::Base64UrlEncoding)));
        netCore->sendMessageRequest(mdMsgEn);

        // 更新ui显示
        ui->plainTextEdit->clear();
        addMessage(msgSend, mdMsg.getMid(), mdMsg.getContext(), "", -1);
        // 添加数据库
        if(!sqlModel.addMsg(mdMsg))
        {
            QLog::log(QString("SQLITE错误 | addMsg : %1").arg(sqlModel.queryLastError()), "MainWindow");
        }
        // 设定一个定时器检测消息发送状态
        QTimer * statusTimer = new QTimer(this);
        QString mid = mdMsg.getMid();
        connect(statusTimer,&QTimer::timeout,[=](){
            statusTimer->stop();
            MdMsg mdMsgCheck = sqlModel.getMsg(mid);
            if(mdMsgCheck.getStatus() < 0)  // 如果状态还是发送中则直接改为错误
            {
                // 修改数据库标志位
                QMap<QString, QVariant> data;
                data["status"] = 1;
                if(!sqlModel.updateMsg(mid, data))
                {
                    QString lastError = sqlModel.queryLastError();
                    qDebug() << "更新数据库消息状态错误 | 信息:" << lastError;
                    QLog::log(QString("更新数据库消息状态错误 | 信息:%1").arg(lastError), "MainWindow");
                }
                // 更新UI显示
                for (int i = ui->listWidgetMsg->count() -1 ; i >= 0; i--)
                {
                    if(ui->listWidgetMsg->item(i)->data(Qt::UserRole).toString() == mid)
                    {
                        SendMsgListItem * sendMsgListItem = (SendMsgListItem *)ui->listWidgetMsg->itemWidget(ui->listWidgetMsg->item(i));
                        sendMsgListItem->setStatus(1);
                        break;
                    }
                }
            }
        });
        statusTimer->start(5000);
    }

}

void MainWindow::on_pushButtonToolMsg_clicked()
{
    MsgLogWindow * msgLogWindow = new MsgLogWindow(this, &sqlModel, curUserId);
    msgLogWindow->setAttribute(Qt::WA_DeleteOnClose);
    msgLogWindow->show();
}

void MainWindow::on_listWidgetContact_itemClicked(QListWidgetItem *item)
{
    curUserId = item->data(Qt::UserRole).toInt();
    curUserKey = sqlModel.getContactKey(curUserId);
    qDebug() << item->data(Qt::UserRole) << item->data(Qt::UserRole + 1) << curUserKey;

    // 初始化工作区域UI显示
    setWorkStationVisible(true);
    removeAllMessage();
    ui->labelName->setText(item->data(Qt::UserRole + 2).toString());
    if(curUserKey.size() > 0)
    {
        ui->labeSec->setText("本次通讯已加密");
        ui->labeSec->setStyleSheet("color:#5FB878;border:none;");
    }else
    {
        ui->labeSec->setText("本次通讯未加密");
        ui->labeSec->setStyleSheet("color:#FF5722;border:none;");
    }
    // 删除未读显示
    QMap<QString, QVariant> map;
    map["unread"] = false;
    updateContact(curUserId, map);
    // 载入前20条聊天记录
    MdMsgList mdMsgList = sqlModel.getMsgs(curUserId, 0, 20);
    for (int i = 0; i < mdMsgList.size(); i++)
    {
        if(mdMsgList.at(i).getSenderUid() == netCore->getUid())
            addMessage(msgSend, mdMsgList.at(i).getMid(), mdMsgList.at(i).getContext(), "" , mdMsgList.at(i).getStatus());
        else
            addMessage(msgRecv, mdMsgList.at(i).getMid(), mdMsgList.at(i).getContext(), "", mdMsgList.at(i).getStatus());
    }
}


void MainWindow::on_pushButtonHeadimg_clicked()
{
    UserInfoWindow * userInfoWindow = new UserInfoWindow(this, cursor().pos());
    userInfoWindow->setData(uid, username, nickname, true);
    userInfoWindow->setAttribute(Qt::WA_DeleteOnClose);
    userInfoWindow->show();
}

void MainWindow::on_listWidgetContact_customContextMenuRequested(const QPoint &pos)
{
    SysMenu * sysMenu = new SysMenu(QPoint(0, 0), SysMenu::MENU_POPDOWN_RIGHT, ui->listWidgetContact);
    sysMenu->setStyleSheet("QMenu{background:#ffffff;border:none;}QMenu::item{padding:10px 20px;font-size:11px;}QMenu::item:selected{background-color:#c2c2c2;}");
    sysMenu->addAction(QString("删除联系人"));
    connect(sysMenu, &QMenu::triggered, this, &MainWindow::on_contactmenu_triggered);
    sysMenu->exec(QCursor::pos());
}

void MainWindow::on_lineEditSearch_textChanged(const QString &arg1)
{
    //qDebug() << arg1;
    if(arg1.isEmpty())
    {
        for (int i = 0; i < ui->listWidgetContact->count(); i++)
        {
            ui->listWidgetContact->item(i)->setHidden(false);
        }
    }else
    {
        for (int i = 0; i < ui->listWidgetContact->count(); i++)
        {
            if(ui->listWidgetContact->item(i)->data(Qt::UserRole).toInt() == arg1.toInt() ||
                    ui->listWidgetContact->item(i)->data(Qt::UserRole + 1).toString().contains(arg1, Qt::CaseInsensitive) ||
                    ui->listWidgetContact->item(i)->data(Qt::UserRole + 2).toString().contains(arg1, Qt::CaseInsensitive) )
            {
                ui->listWidgetContact->item(i)->setHidden(false);
            }
            else
            {
                ui->listWidgetContact->item(i)->setHidden(true);
            }
        }
    }
}

void MainWindow::on_pushButtonToolEmoji_clicked()
{
    EmojiWindow * emojiWindow = new EmojiWindow(this, ui->pushButtonToolEmoji->mapToGlobal(QPoint(0, 0)));
    emojiWindow->setAttribute(Qt::WA_DeleteOnClose);
    emojiWindow->show();
}

// ***********************  以下是成员函数 *****************************

void MainWindow::addContact(int uid, QString username, QString nickname, QString lastMsg, QString header, QString time)
{
    bool isExist = false;
    for (int i = 0; i < ui->listWidgetContact->count(); i++)
    {
        if(ui->listWidgetContact->item(i)->data(Qt::UserRole).toInt() == uid)
        {
            isExist = true;
            break;
        }
    }
    if(!isExist)
    {
        QListWidgetItem * listWidgetItem = new QListWidgetItem(ui->listWidgetContact);
        listWidgetItem->setSizeHint(QSize(252, 60));
        ContactListItem * contactListItem = new ContactListItem(ui->listWidgetContact);
        contactListItem->setData(uid, username, nickname, lastMsg, header, time);
        listWidgetItem->setData(Qt::UserRole, uid);
        listWidgetItem->setData(Qt::UserRole + 1, username);
        listWidgetItem->setData(Qt::UserRole + 2, nickname);
        ui->listWidgetContact->addItem(listWidgetItem);
        ui->listWidgetContact->setItemWidget(listWidgetItem, contactListItem);
    }
}

void MainWindow::addMessage(msgType type,QString mid, QString text, QString headimg, int status)
{
    if(type == msgRecv)
    {
        RecvMsgListItem * recvMsgListItem = new RecvMsgListItem(ui->listWidgetMsg);
        recvMsgListItem->setData(QString("<p style='line-height:24px;'>%1</p>").arg(text));
        recvMsgListItem->setStatus(status);
        QListWidgetItem * listWidgetItem = new QListWidgetItem(ui->listWidgetMsg);
        listWidgetItem->setSizeHint(QSize(-1, recvMsgListItem->getHeightHint()));
        listWidgetItem->setData(Qt::UserRole, mid);
        ui->listWidgetMsg->addItem(listWidgetItem);
        ui->listWidgetMsg->setItemWidget(listWidgetItem, recvMsgListItem);
    }
    else if(type == msgSend)
    {
        SendMsgListItem * sendMsgListItem = new SendMsgListItem(ui->listWidgetMsg);
        sendMsgListItem->setData(QString("<p style='line-height:24px;'>%1</p>").arg(text));
        sendMsgListItem->setStatus(status);
        QListWidgetItem * listWidgetItem = new QListWidgetItem(ui->listWidgetMsg);
        listWidgetItem->setSizeHint(QSize(-1, sendMsgListItem->getHeightHint()));
        listWidgetItem->setData(Qt::UserRole, mid);
        ui->listWidgetMsg->addItem(listWidgetItem);
        ui->listWidgetMsg->setItemWidget(listWidgetItem, sendMsgListItem);
    }
    ui->listWidgetMsg->scrollToBottom();
}

void MainWindow::updateContact(int uid, QMap<QString, QVariant> dataMap)
{
    for (int i = 0; i < ui->listWidgetContact->count(); i++)
    {
        if(ui->listWidgetContact->item(i)->data(Qt::UserRole).toInt() == uid)
        {
            ContactListItem * contactListItem = (ContactListItem *)ui->listWidgetContact->itemWidget(ui->listWidgetContact->item(i));
            if(dataMap.contains("lastMsg"))
            {
                contactListItem->setLastMsg(dataMap["lastMsg"].toString());
            }
            if(dataMap.contains("unread"))
            {
                contactListItem->setUnread(dataMap["unread"].toBool());
            }
            if(dataMap.contains("lastTime"))
            {
                contactListItem->setLastTime(dataMap["lastTime"].toString());
            }
            break;
        }
    }
}

void MainWindow::removeContact(int uid)
{
    for (int i = 0; i < ui->listWidgetContact->count(); i++)
    {
        if(ui->listWidgetContact->item(i)->data(Qt::UserRole).toInt() == uid)
        {
            ui->listWidgetContact->removeItemWidget(ui->listWidgetContact->takeItem(i));
            // 如果工作区为当前用户，清空所有内容，并隐藏
            if(curUserId == uid)
            {
                ui->labelName->setText("");
                ui->listWidgetMsg->clear();
                setWorkStationVisible(false);
                curUserId = 0;
            }
            break;
        }
    }
}

void MainWindow::recvMsgController(MdMsg mdMsg)
{
    switch (mdMsg.getType()) {
    case NetCore::MSG_TEXT:
    {
        // 先解密消息
        MdMsg mdMsgDe;
        mdMsgDe = mdMsg;
        //qDebug() << "密钥" <<hashKey<< "iv"<< hashIV << "密文" << mdMsg.getContext();
        QByteArray hashKey = QCryptographicHash::hash(sqlModel.getContactKey(mdMsgDe.getSenderUid()).toLatin1(), QCryptographicHash::Sha256);
        QByteArray hashIV = QCryptographicHash::hash(mdMsg.getMid().toLatin1(), QCryptographicHash::Md5);
        QByteArray decodeText = QAES::decrypt(QByteArray::fromBase64(mdMsg.getContext().toLatin1(), QByteArray::Base64UrlEncoding), hashKey, hashIV);
        mdMsgDe.setContext(QString::fromUtf8(decodeText));

        // 先存入数据库
        sqlModel.addMsg(mdMsgDe);

        // 判断是否为当前聊天对象
        bool isUnread = false;
        if(mdMsgDe.getSenderUid() == curUserId)
        {
            addMessage(msgRecv, mdMsgDe.getMid(), mdMsgDe.getContext(), "", 0);
            isUnread = false;
        }
        else
        {
            QSound::play(":/base/msg_notice.wav");
            isUnread = true;

        }
        // 更新联系人信息
        QMap<QString, QVariant> map;
        QDateTime time;QString stime;
        if(mdMsgDe.getTimestmp() > 0)
        {
            time.setTime_t(mdMsgDe.getTimestmp());
            stime = time.toString("yyyy/MM/dd");
        }
        map["lastTime"] = stime;
        map["lastMsg"] = mdMsgDe.getContext();
        map["unread"] = isUnread;
        updateContact(mdMsgDe.getSenderUid(), map);
        break;
    }
    case NetCore::MSG_ADDCONTACT:
    {
        // 生成非堆成密钥对
        QECDHMgr qECDHMgr;
        if(!qECDHMgr.SetCurveName("secp384r1"))
        {
            QLog::log("系统不支持secp384r1加密方式，无法协商密钥", "MainWindow");
            QMessageBox::critical(this, "提示", "系统不支持secp384r1加密方式，无法协商密钥");
            break;
        }
        if(!qECDHMgr.GenerateKeys())
        {
            QLog::log(QString("接收好友申请过程中生成密钥对错误，无法协商密钥，信息:%1").arg(qECDHMgr.GetLastError()), "MainWindow");
            QMessageBox::critical(this, "提示", "接收好友申请过程中生成密钥对错误，无法协商密钥");
            break;
        }
        QString publicKey = qECDHMgr.GetPublicKey();
        QString privateKey = qECDHMgr.GetPrivateKey();
        // 先存入数据库
        sqlModel.addContactApply(mdMsg.getSenderUid(), "", "", "", mdMsg.getContext(),"", publicKey, privateKey);
        // 发送消息继续获取用户详细信息
        netCore->getUserInfoRequest(QString::number(mdMsg.getSenderUid()));
        // 发送通知给前台
        QSound::play(":/base/msg_notice.wav");
        ui->labelMenuUnread->show();
        agreeAction->setUnread(true);
        break;
    }
    case NetCore::MSG_AGREECONTACT:
    {
        // 获取联系人信息
        MdUser mdUser = sqlModel.getContactApplyInfo(mdMsg.getSenderUid());
        // 计算对称密钥
        QECDHMgr qECDHMgr;
        if(!qECDHMgr.SetCurveName("secp384r1"))
        {
            QLog::log("系统不支持secp384r1加密方式，无法协商密钥", "MainWindow");
            QMessageBox::critical(this, "提示", "系统不支持secp384r1加密方式，无法协商密钥");
            break;
        }
        if(!qECDHMgr.SetKeyPair(mdUser.getSenderPubKey(), mdUser.getSenderPriKey()))
        {
            QLog::log(QString("处理对方同意好友申请消息过程中载入密钥对错误，无法协商密钥，信息：%1").arg(qECDHMgr.GetLastError()), "MainWindow");
            QMessageBox::critical(this, "提示", "处理对方同意好友申请消息过程中载入密钥对错误，无法协商密钥");
            break;
        }
        QString secKey = qECDHMgr.DeriveSharedSecret(mdMsg.getContext());
        sqlModel.addContact(mdUser.getUid(), secKey);
        // 修改数据库联系人信息
        QMap<QString, QVariant> data;
        data["status"] = 1;
        data["recver_pubkey"] = mdMsg.getContext();
        sqlModel.updateContactApply(mdMsg.getSenderUid(), data);
        // 添加用户到UI列表
        addContact(mdUser.getUid(), mdUser.getUsername(), mdUser.getNickname(), "", mdUser.getHeadimg(), "");
        break;
    }
    case NetCore::MSG_DELCONTACT:
    {
        // 从UI列表删除用户
        removeContact(mdMsg.getSenderUid());
        break;
    }
    }
}

void MainWindow::editUserInfo()
{
    qDebug() << "修改用户信息" ;
    MdUser mdUser;
    mdUser.setUid(uid);
    mdUser.setUsername(username);
    mdUser.setNickname(nickname);
    mdUser.setHeadimg(header);
    mdUser.setPassword(netCore->getPassword());
    EditUserInfoWindow * editUserInfoWindow = new EditUserInfoWindow(this, netCore);
    editUserInfoWindow->setAttribute(Qt::WA_DeleteOnClose);
    editUserInfoWindow->setData(mdUser);
    editUserInfoWindow->show();
}

void MainWindow::appendPlainText(QString text)
{
    ui->plainTextEdit->insertPlainText(text);
}

void MainWindow::removeAllMessage()
{
    ui->listWidgetMsg->clear();
    ui->plainTextEdit->clear();
}

void MainWindow::setWorkStationVisible(bool isVisable)
{
    if(isVisable)
    {
        ui->labelWorkLogo->setVisible(false);
        ui->widgetMainTitle->setVisible(true);
        ui->listWidgetMsg->setVisible(true);
        ui->widgetTools->setVisible(true);
        ui->plainTextEdit->setVisible(true);
        ui->widgetSend->setVisible(true);
    }else
    {
        ui->labelWorkLogo->setVisible(true);
        ui->widgetMainTitle->setVisible(false);
        ui->listWidgetMsg->setVisible(false);
        ui->widgetTools->setVisible(false);
        ui->plainTextEdit->setVisible(false);
        ui->widgetSend->setVisible(false);
    }
}

// ***********************  以下窗口重载 *****************************
void MainWindow::closeEvent(QCloseEvent* event)
{
    emit closeWind("MainWindow");
    event->accept();
}

bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    static bool shiftPressed = false;
    // 捕获plainTextEdit的回车和shift+回车消息，回车则发送消息，shift+回车为回车消息
    if(target == ui->plainTextEdit)
    {
        if(event->type() == QEvent::KeyPress)
        {
             QKeyEvent *k = static_cast < QKeyEvent*>(event);
             if(k->key() == Qt::Key_Shift)
             {
                 shiftPressed = true;
             }
             else if(k->key() == Qt::Key_Return   || k->key() == Qt::Key_Enter )
             {
                 if(!shiftPressed)
                 {
                     on_pushButtonSend_clicked();
                     return true;
                 }
             }

        }
        else if(event->type() == QEvent::KeyRelease)
        {
            QKeyEvent *k = static_cast < QKeyEvent*>(event);
            if(k->key() == Qt::Key_Shift)
            {
                shiftPressed = false;
            }
        }
    }
    return QWidget::eventFilter(target,event);
}
