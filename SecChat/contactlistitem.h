#ifndef CONTACTLISTITEM_H
#define CONTACTLISTITEM_H

#include <QWidget>

namespace Ui {
class ContactListItem;
}

class ContactListItem : public QWidget
{
    Q_OBJECT

public:
    explicit ContactListItem(QWidget *parent = nullptr);
    ~ContactListItem();

    void setData(int uid, QString username, QString nickname, QString lastMsg, QString headimg, QString time);

    void setLastMsg(QString msg);

    void setLastTime(QString time);

    void setUnread(bool isShow = false);

private slots:
    void on_pushButtonHeader_clicked();

private:
    Ui::ContactListItem *ui;

    int uid;            // 临时存放用户uid

    QString username;   // 临时存放用户名

    QString nickname;   // 临时存放用户昵称

    QString headimg;    // 临时存放用户头像
};

#endif // CONTACTLISTITEM_H
