#include "msglogwindow.h"
#include "ui_msglogwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QTimer>
#include <QFileDialog>

MsgLogWindow::MsgLogWindow(QWidget *parent, SqlModel * sqlModel, int uid) :
    QMainWindow(parent),
    ui(new Ui::MsgLogWindow)
{
    ui->setupUi(this);
    mainWindow = (MainWindow *)parent;
    this->sqlModel = sqlModel;
    this->uid = uid;

    ui->lineEditPage->setValidator(new QRegExpValidator(QRegExp("[0-9]+$")));
    ui->lineEditPage->setText("0");
    ui->progressBar->hide();

    ui->tableWidget->setColumnCount(5);                                                 //设置为5列
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "消息MID" <<"发送者UID"<< "接收者UID" << "消息内容" << "时消息间");  //设置列名称
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);                 //设置可调宽度
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidget->verticalHeader()->setHidden(true);                               //隐藏列表头


    // 设置滚动条样式
    QFile verticalFile(":qss/verticalscrollbar.qss");
    QFile horizontalFile(":qss/horizontalscrollbar.qss");
    verticalFile.open(QFile::ReadOnly);
    horizontalFile.open(QFile::ReadOnly);
    ui->tableWidget->verticalScrollBar()->setStyleSheet(verticalFile.readAll());
    ui->tableWidget->horizontalScrollBar()->setStyleSheet(horizontalFile.readAll());
    verticalFile.close();
    horizontalFile.close();

    if(uid > 0 && sqlModel)
    {
        // 获取聊天记录总条数
        int totalMsg = sqlModel->countMsgs(uid);
        maxPage = totalMsg % pageLimit == 0 ? totalMsg / pageLimit : totalMsg / pageLimit + 1;
        qDebug() << totalMsg << maxPage;
        ui->label->setText(QString("共%1页").arg(maxPage));

        // 显示50条聊天记录
        loadMsg(0, pageLimit);
        ui->lineEditPage->setText("1");
        curPage = 1;
    }
}

MsgLogWindow::~MsgLogWindow()
{
    delete ui;
}

void MsgLogWindow::loadMsg(int start, int limit)
{
    if(uid > 0 && sqlModel)
    {
        MdMsgList mdMsgList = sqlModel->getMsgs(uid, start, limit);
        for (int i=0 ; i < mdMsgList.size() ; i++) {
            int curRow = ui->tableWidget->rowCount();
            ui->tableWidget->insertRow(curRow);
            ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(mdMsgList.at(i).getMid()));
            ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(QString::number(mdMsgList.at(i).getSenderUid())));
            ui->tableWidget->setItem(curRow,2,new QTableWidgetItem(QString::number(mdMsgList.at(i).getRecverUid())));
            ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(mdMsgList.at(i).getContext()));
            ui->tableWidget->setItem(curRow,4,new QTableWidgetItem(QDateTime::fromSecsSinceEpoch(mdMsgList.at(i).getTimestmp()).toString("yyyy/MM/dd hh:mm:ss")));
            ui->tableWidget->setRowHeight(curRow, 22);
        }
    }
}

void MsgLogWindow::on_lineEditPage_textChanged(const QString &arg1)
{
    ui->pushButtonPrevious->setDisabled(false);
    ui->pushButtonNext->setDisabled(false);
    if(arg1.toInt() <= 1)
    {
        ui->pushButtonPrevious->setDisabled(true);
    }
    if(arg1.toInt() >= maxPage)
    {
        ui->pushButtonNext->setDisabled(true);
    }
}

void MsgLogWindow::on_pushButtonPrevious_clicked()
{
    if(curPage > 0)
    {
        curPage --;
        clearMsg();
        loadMsg((curPage - 1) * pageLimit, pageLimit);
        ui->lineEditPage->setText(QString::number(curPage));
    }
}

void MsgLogWindow::on_pushButtonNext_clicked()
{
    if(curPage < maxPage)
    {
        curPage ++;
        clearMsg();
        loadMsg((curPage - 1) * pageLimit, pageLimit);
        ui->lineEditPage->setText(QString::number(curPage));
    }
}

void MsgLogWindow::clearMsg()
{
    // 清空ui列表项目
    for(int i = ui->tableWidget->rowCount() - 1;i >= 0; i--)
    {
        ui->tableWidget->removeRow(i);
    }
}

void MsgLogWindow::on_lineEditPage_returnPressed()
{
    int page = ui->lineEditPage->text().toInt();
    if(page > 0 && page < maxPage)
    {
        curPage = page;
        clearMsg();
        loadMsg((curPage - 1) * pageLimit, pageLimit);
    }
}

void MsgLogWindow::on_actionClear_triggered()
{
    if(uid > 0&& sqlModel)
    {
        QMessageBox::StandardButton result=QMessageBox::question(this, "提示", "正在清空该联系人所有消息，是否继续？");
        if(result == QMessageBox::Yes)
        {
            qDebug() << "清空记录";
            if(sqlModel->clearMsgs(uid))
            {
                // 清空记录查看器所有内容
                clearMsg();
                // 同步清空主界面工作区聊天记录
                if(mainWindow)
                    mainWindow->removeAllMessage();
            }
            else
            {
                QMessageBox::critical(this, "执行错误", sqlModel->queryLastError());
            }
        }
    }
}

void MsgLogWindow::on_actionExport_triggered()
{
    if(uid > 0 && sqlModel && maxPage > 0)
    {
        QString fileName = QFileDialog::getSaveFileName(this, "另存为", QString("SecChat_%1.csv").arg((int)QDateTime::currentSecsSinceEpoch()), "*.csv");
        if(!fileName.isEmpty())
        {
            QFile file(fileName);
            file.open(QIODevice::WriteOnly| QIODevice::Text);
            file.write(QString("MID,sender_uid,recver_uid,context,time\n").toUtf8());
            ui->progressBar->setValue(0);
            ui->progressBar->show();
            for(int i= 0; i< maxPage; i++)
            {
                MdMsgList mdMsgList = sqlModel->getMsgs(uid, i * pageLimit, pageLimit);
                for(int j = 0 ; j < mdMsgList.size() ; j++)
                {
                    QString strLine = QString("%1,%2,%3,%4,%5\n")
                            .arg(mdMsgList.at(j).getMid())
                            .arg(mdMsgList.at(j).getSenderUid())
                            .arg(mdMsgList.at(j).getRecverUid())
                            .arg(mdMsgList.at(j).getContext())
                            .arg(QDateTime::fromSecsSinceEpoch(mdMsgList.at(j).getTimestmp()).toString("yyyy/MM/dd hh:mm:ss"));
                    file.write(strLine.toUtf8());
                }
                qDebug()  << (i + 1) * (100 / maxPage) << 100 / maxPage << i;
                ui->progressBar->setValue((i + 1) * (100 / maxPage));
            }
            file.close();
            ui->progressBar->setValue(100);
            // 延时3秒隐藏进度条
            QTimer * timer = new QTimer(this);
            connect(timer,&QTimer::timeout,[=](){
                ui->progressBar->hide();
            });
            timer->start(3000);
        }
    }
}

void MsgLogWindow::on_pushButtonSearch_clicked()
{
    if(uid > 0&& sqlModel && !ui->lineEditSearch->text().isEmpty())
    {
        clearMsg();
        MdMsgList mdMsgList = sqlModel->searchMsgs(uid, ui->lineEditSearch->text());
        for (int i=0 ; i < mdMsgList.size() ; i++) {
            int curRow = ui->tableWidget->rowCount();
            ui->tableWidget->insertRow(curRow);
            ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(mdMsgList.at(i).getMid()));
            ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(QString::number(mdMsgList.at(i).getSenderUid())));
            ui->tableWidget->setItem(curRow,2,new QTableWidgetItem(QString::number(mdMsgList.at(i).getRecverUid())));
            ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(mdMsgList.at(i).getContext()));
            ui->tableWidget->setItem(curRow,4,new QTableWidgetItem(QDateTime::fromSecsSinceEpoch(mdMsgList.at(i).getTimestmp()).toString("yyyy/MM/dd hh:mm:ss")));
            ui->tableWidget->setRowHeight(curRow, 22);
        }
    }
}
