#ifndef ADDCONTACTLISTITEM_H
#define ADDCONTACTLISTITEM_H

#include <QWidget>
#include <QDebug>
#include "addcontactwindow.h"

namespace Ui {
class AddContactListItem;
}

class AddContactListItem : public QWidget
{
    Q_OBJECT

public:
    explicit AddContactListItem(QWidget *parent = nullptr, AddContactWindow * addContactWindow = nullptr);
    ~AddContactListItem();

    void setData(int uid, QString username, QString nickname, QString headimg);

private slots:
    void on_pushButtonAdd_clicked();

private:
    Ui::AddContactListItem *ui;

    AddContactWindow * addContactWindow = nullptr;

    NetCore * netCore = nullptr;

    int uid = 0;

    QString username;

    QString nickname;

    QString headimg;
};

#endif // ADDCONTACTLISTITEM_H
