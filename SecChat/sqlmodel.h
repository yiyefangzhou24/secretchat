#ifndef SQLMODEL_H
#define SQLMODEL_H
// **
//
// sqlite数据库操作Model类
//    该类的生命周期应该和主窗口MainWindow的生命周期一样
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QApplication>
#include "mdmsg.h"
#include "mdmsglist.h"
#include "mduser.h"
#include "mduserlist.h"

class SqlModel
{
public:
    SqlModel();

    // 连接数据库
    bool connect(QString dbname = "");

    // 断开数据库连接
    void close();

    // 获取数据库连接错误信息
    QString dbLastError();

    // 获取数据库查询错误信息
    QString queryLastError();

    // 保持数据库连接
    void keepAlive();

    // 获取用户最后一条聊天记录
    MdMsg getUserLastMsg(int uid);

    // 获取指定条数的聊天记录
    MdMsgList getMsgs(int uid, int start, int limit);

    // 搜索聊天记录
    MdMsgList searchMsgs(int uid, QString keyword);

    // 获取指定的消息
    MdMsg getMsg(QString mid);

    // 获取指定用户聊天记录条数
    int countMsgs(int uid);

    // 删除所有聊天记录
    bool clearMsgs(int uid);

    // 添加好友申请
    bool addContactApply(int uid, QString username, QString nickname, QString header,
                         QString senderPubKey, QString senderPriKey ,QString recverPubKey, QString recverPriKey);

    // 获取好友申请列表
    MdUserList getContactApply();

    // 更新好友申请列表
    bool updateContactApply(int uid, QMap<QString, QVariant> data);

    // 添加消息
    bool addMsg(MdMsg mdMsg);

    // 修改消息
    bool updateMsg(QString mid, QMap<QString, QVariant> data);

    // 获取联系人申请的详细信息
    MdUser getContactApplyInfo(int uid);

    // 添加联系人（记录对称密钥）
    bool addContact(int uid, QString secKey);

    // 获取联系人信息（获取对称密钥）
    QString getContactKey(int uid);

private:

    // 初始化表
    bool initTables();

private:

    QSqlDatabase sqlDB;

    QSqlQuery query;

    QString dbname;
};

#endif // SQLMODEL_H
