#ifndef SENDMSGLISTITEM_H
#define SENDMSGLISTITEM_H

#include <QWidget>

namespace Ui {
class SendMsgListItem;
}

class SendMsgListItem : public QWidget
{
    Q_OBJECT

public:
    explicit SendMsgListItem(QWidget *parent = nullptr);
    ~SendMsgListItem();

    // 设置数据
    void setData(QString text);

    // 获取实际尺寸
    int getHeightHint();

    // 设置消息状态
    void setStatus(int status);

    void enterEvent(QEvent *event) override;

    void leaveEvent(QEvent *event) override;

private:
    Ui::SendMsgListItem *ui;
};

#endif // SENDMSGLISTITEM_H
