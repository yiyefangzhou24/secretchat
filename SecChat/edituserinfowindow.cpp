#include "edituserinfowindow.h"
#include "ui_edituserinfowindow.h"
#include "qlog.h"
#include <QMessageBox>
#include <QMap>

EditUserInfoWindow::EditUserInfoWindow(QWidget *parent, NetCore * netCore) :
    QMainWindow(parent),
    ui(new Ui::EditUserInfoWindow)
{
    ui->setupUi(this);
    this->netCore = netCore;

    // 修改按钮响应回车事件
    ui->pushButtonUpdate->setShortcut(Qt::Key_Return);

    if(netCore)
    {
        // 绑定更新用户信息返回信号
        connect(netCore, &NetCore::updateUserInfoResponse, this, &EditUserInfoWindow::on_updateuserinfo_response);
    }
}

EditUserInfoWindow::~EditUserInfoWindow()
{
    if(netCore)
    {
        // 解除绑定更新用户信息返回信号
        disconnect(netCore, &NetCore::updateUserInfoResponse, this, &EditUserInfoWindow::on_updateuserinfo_response);
    }
    delete ui;
}

void EditUserInfoWindow::setData(MdUser mdUser)
{
    this->mdUser = mdUser;
    ui->lineEditNickname->setText(mdUser.getNickname());
    ui->lineEditUsername->setText(mdUser.getUsername());
    ui->lineEditPassword->setText(mdUser.getPassword());
}

void EditUserInfoWindow::on_pushButtonUpdate_clicked()
{
    if(netCore)
    {
        if(!ui->lineEditUsername->text().isEmpty() && !ui->lineEditNickname->text().isEmpty() && !ui->lineEditPassword->text().isEmpty())
        {
            QMap<QString, QVariant> data;
            if(ui->lineEditUsername->text() != mdUser.getUsername())
                data["username"] = ui->lineEditUsername->text();
            if(ui->lineEditNickname->text() != mdUser.getNickname())
                data["nickname"] = ui->lineEditNickname->text();
            if(ui->lineEditPassword->text() != mdUser.getPassword())
                data["password"] = passwdEncrpyt(ui->lineEditPassword->text());
            if(data.size() > 0)
                netCore->updateUserInfoRequest(mdUser.getUid(), data);
        }
    }
}

void EditUserInfoWindow::on_updateuserinfo_response(int error, QString msg)
{
    if(error == 0)
    {
        close();
    }else
    {
        QMessageBox::critical(this, "错误", msg);
        QLog::log(QString("更新用户信息错误 | 错误代码：%1，错误信息:%2").arg(error).arg(msg), "EditUserInfoWindow");
    }
}

QString EditUserInfoWindow::passwdEncrpyt(QString t)
{
    return QCryptographicHash::hash(t.toLatin1(),QCryptographicHash::Md5).toHex();
}
