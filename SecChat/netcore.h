#ifndef NETCORE_H
#define NETCORE_H

#include "msgpackage.h"
#include "mduser.h"
#include "mduserlist.h"
#include "mdmsg.h"
#include "mdmsglist.h"
#include <QObject>
#include <QtNetwork>
#include <QByteArray>

class NetCore: public QObject, public MsgPackage
{
    Q_OBJECT

public:
    NetCore();
    ~NetCore();

    // 配置信息
    void config(QString host, int port, QByteArray secKey, int beat_time = 60);

    // 连接服务器
    void connectToServer();

    // 关闭服务器连接
    void close();

    // 重载父类虚函数，接收到一次完整包的响应函数
    void pkgReady(QByteArray en_data) override;

    // 获取用户uid
    int getUid();

    // 获取用户名
    QString getUsername();

    // 获取密码
    QString getPassword();

    // 获取昵称
    QString getNickname();

    // 获取头像
    QString getHeadimg();

    // 获取系统版本
    int getOsType();

    // 登录请求
    void loginRequest(QString username, QString password, int osType);

    // 注销请求
    void logoutRequest();

    // 获取用户信息请求
    void getUserInfoRequest(QString keyword);

    // 更新用户信息
    void updateUserInfoRequest(int uid, QMap<QString, QVariant> data);

    // 获取联系人列表请求
    void getContactsRequest();

    // 删除联系人
    void deleteContectRequest(int uid);

    // 注册请求
    void registerRequest(QString username, QString nickname, QString password);

    // 获取离线消息
    void getofflinemsgRequest();

    // 发送通讯消息请求
    void sendMessageRequest(MdMsg mdMsg);

    // 计算消息的唯一标识符
    static QString getMsgMid(MdMsg mdMsg);
signals:

    // 服务器连接信号
    void connected();

    // 服务器连接超时
    void connectTimeout();

    // 服务器断开连接信号
    void disconnected();

    // 登录请求返回信号
    void loginResponse(int error, QString msg);

    // 注销请求返回信号
    void logoutResponse(int error, QString msg);

    // 注册用户请求返回信号
    void registerResponse(int error, QString msg);

    // 搜索用户信息请求返回信号
    void getUserInfoResponse(int error, QString msg, MdUser mdUser);

    // 更新用户信息返回信号
    void updateUserInfoResponse(int error, QString msg);

    // 获取联系人列表请求返回信号
    void getContactsResponse(int error, QString msg, MdUserList mdUserList);

    // 删除联系人请求返回信号
    void deleteContactResponse(int error, QString msg, int target_uid);

    // 获取离线消息请求返回信号
    void getofflinemsgResponse(int error, QString msg, MdMsgList mdMsgList);

    // 发送普通消息返回信号
    void sendMessageResponse(int error, QString msg, QString mid);

    // 接收用户消息返回信号
    void recvMessageResponse(MdMsg mdMsg);

private slots:

    // 服务器连接 - 连接槽函数
    void on_tcp_connect();

    // 服务器连接 - 接收消息槽函数
    void on_tcp_read();

    // 服务器连接 - 断开连接槽函数
    void on_tcp_disconnect();

    // 心跳请求
    void on_tcp_heartbeat();


private:
    // 私有函数 - 密码加密
    QString passwdEncrpyt(QString t);

    // 私有函数 - 对称加密（密钥为空则返回原数据）
    QByteArray encrypt(QByteArray data);

    // 私有函数 - 对称解密（密钥为空则返回原数据）
    QByteArray decrypt(QByteArray data);

private:

    QTcpSocket * tcpSocket;

    QString host;   // 服务器地址

    QByteArray secKey;  // 通讯对称密钥

    int port;       // 服务器端口

    int beat_time;   // 心跳间隔

    QString username;   // 用户名（用于断线重连）

    QString password;   // 密码（用于断线重连）

    QString nickname;   // 用户昵称

    QString headimg;    // 用户头像

    int osType;         // 客户端类型（用于断线重连）

    int uid;            // 用户uid

    QString access_token;   // 认证密钥

    QTimer * beatTimer; // 心跳请求定时器

public:
    static const int OS_PCWIN           = 0x00000001;       // windows电脑端
    static const int OS_PCMAC           = 0x00000002;       // mac电脑端
    static const int OS_PCLINUX         = 0x00000003;       // Linux电脑端
    static const int OS_WAPANDROID      = 0x00000004;       // Android手机端
    static const int OS_WAPIPHONE       = 0x00000005;       // iPhone手机端
    static const int OS_WAPHARMONY      = 0x00000006;       // Harmony手机端

    static const int REQ_HEARTBEAT      = 0x00010001;       // 心跳请求
    static const int RET_HEARTBEAT      = 0x00010002;       // 心跳返回
    static const int REQ_LOGIN          = 0x00010003;       // 登录请求
    static const int RET_LOGIN          = 0x00010004;       // 登录返回
    static const int REQ_LOGOUT         = 0x00010005;       // 注销请求
    static const int RET_LOGOUT         = 0x00010006;       // 注销返回
    static const int REQ_REGISTER       = 0x00010007;       // 注册请求
    static const int RET_REGISTER       = 0x00010008;       // 注册返回
    static const int REQ_SEARCHUSER     = 0x00010009;       // 搜索用户请求
    static const int RET_SEARCHUSER     = 0x0001000a;       // 搜索用户返回
    static const int REQ_OFFLINEMSG     = 0x0001000b;       // 获取离线消息请求
    static const int RET_OFFLINEMSG     = 0x0001000c;       // 获取离线消息返回
    static const int REQ_CONTACTS       = 0x0001000d;       // 获取联系人列表请求
    static const int RET_CONTACTS       = 0x0001000e;       // 获取联系人列表返回
    static const int REQ_UPDATEUSERINFO = 0x0001000f;       // 更新用户信息请求
    static const int RET_UPDATEUSERINFO = 0x00010010;       // 更新用户信息返回
    static const int REQ_DELETECONTACT  = 0x00010011;       // 删除联系人请求
    static const int RET_DELETECONTACT  = 0x00010012;       // 删除联系人返回
    static const int REQ_SENDMESSAGE    = 0x00010013;       // 发送用户消息请求
    static const int RET_SENDMESSAGE    = 0x00010014;       // 发送用户消息返回
    static const int REQ_RECVMESSAGE    = 0x00010015;       // 接收用户消息请求
    static const int RET_RECVMESSAGE    = 0x00010016;       // 接收用户消息返回（暂时未使用该标志）


    static const int ERR_OAUTH          = 0x00020001;       // 未授权访问
    static const int ERR_SQLEXEC        = 0X00020002;       // SQL语句执行错误
    static const int ERR_FORMAT         = 0X00020003;       // 数据格式错误
    static const int ERR_RESNULL        = 0X00020004;       // 没有结果

    static const int MSG_TEXT           = 0x00000001;       // 聊天消息
    static const int MSG_ADDCONTACT     = 0x00000002;       // 添加好友
    static const int MSG_AGREECONTACT   = 0x00000003;       // 接受/拒绝好友
    static const int MSG_DELCONTACT     = 0x00000004;       // 删除好友
};

#endif // NETCORE_H
