#include "mdmsglist.h"

void MdMsgList::insert(MdMsg msg)
{
    msgList.append(msg);
}

MdMsg MdMsgList::at(int i)
{
    return msgList.at(i);
}

void MdMsgList::clear()
{
    msgList.clear();
}

int MdMsgList::size()
{
    return msgList.size();
}

// 重载 = 运算符
MdMsgList & MdMsgList::operator=(MdMsgList A) {
    msgList.clear();
    for(int i = 0 ; i < A.size(); i++ )
    {
        msgList.append(A.at(i));
    }
    return *this;
}
