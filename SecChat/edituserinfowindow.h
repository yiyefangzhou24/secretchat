#ifndef EDITUSERINFOWINDOW_H
#define EDITUSERINFOWINDOW_H

#include <QMainWindow>
#include "mduser.h"
#include "netcore.h"

namespace Ui {
class EditUserInfoWindow;
}

class EditUserInfoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditUserInfoWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr);
    ~EditUserInfoWindow();

    void setData(MdUser mdUser);

    QString passwdEncrpyt(QString t);

private slots:
    void on_pushButtonUpdate_clicked();

    void on_updateuserinfo_response(int error, QString msg);

private:
    Ui::EditUserInfoWindow *ui;

    MdUser mdUser;

    NetCore * netCore = nullptr;
};

#endif // EDITUSERINFOWINDOW_H
