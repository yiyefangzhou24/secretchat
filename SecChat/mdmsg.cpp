#include "mdmsg.h"

int MdMsg::getSenderUid()
{
    return this->sender_uid;
}

void MdMsg::setSenderUid(int uid)
{
    this->sender_uid = uid;
}

int MdMsg::getRecverUid()
{
    return this->recver_uid;
}

void MdMsg::setRecverUid(int uid)
{
    this->recver_uid = uid;
}

int MdMsg::getTimestmp()
{
    return this->timestmp;
}

void MdMsg::setTimestmp(int timestmp)
{
    this->timestmp = timestmp;
}

int MdMsg::getType()
{
    return this->type;
}

void MdMsg::setType(int type)
{
    this->type = type;
}

QString MdMsg::getContext()
{
    return this->context;
}

void MdMsg::setContext(QString context)
{
    this->context = context;
}

void MdMsg::setMid(QString mid)
{
    this->mid = mid;
}

QString MdMsg::getMid()
{
    return this->mid;
}

int MdMsg::getStatus()
{
    return this->status;
}

void MdMsg::setStatus(int status)
{
    this->status = status;
}

// 重载 = 运算符
MdMsg & MdMsg::operator=(MdMsg A) {
    this->sender_uid = A.sender_uid;
    this->recver_uid = A.recver_uid;
    this->timestmp = A.timestmp;
    this->context = A.context;
    this->type = A.type;
    this->mid = A.mid;
    this->status = A.status;
    return *this;
}
