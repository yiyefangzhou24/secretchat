#include "sysmenuaction.h"
#include <QDebug>

SysMenuAction::SysMenuAction(QString text,QWidget *parent):QWidgetAction(parent)
{
    this->text = text;
}

QWidget *SysMenuAction::createWidget(QWidget *parent)
{
    QWidget* widget = new QWidget(parent);
    widget->setStyleSheet("padding-left:20px;padding-right:20px;color:#C2C2C2;font-size:12px;background-color:#393D49;border:none;");
    widget->setFixedSize(105, 45);

    labelText = new QLabel(text, widget);
    labelText->setFixedSize(95, 45);
    labelText->move(0, 0);

    labelUnread = new QLabel("", widget);
    labelUnread->setStyleSheet("border-image:url(:/main/unread.png);background:transparent;");
    labelUnread->setFixedSize(14, 14);
    labelUnread->move(80, 12);
    labelUnread->hide();

    return widget;
}

void SysMenuAction::setUnread(bool isUnread)
{
    isUnread ? labelUnread->show() : labelUnread->hide();
}
