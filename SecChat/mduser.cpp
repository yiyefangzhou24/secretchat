#include "mduser.h"

int MdUser::getUid()
{
    return uid;
}

void MdUser::setUid(int uid)
{
    this->uid = uid;
}

QString MdUser::getUsername()
{
    return username;
}

void MdUser::setUsername(QString username)
{
    this->username = username;
}

QString MdUser::getPassword()
{
    return password;
}

void MdUser::setPassword(QString password)
{
    this->password = password;
}

QString MdUser::getNickname()
{
    return nickname;
}

void MdUser::setNickname(QString nickname)
{
    this->nickname = nickname;
}

QString MdUser::getHeadimg()
{
    return headimg;
}

void MdUser::setHeadimg(QString headimg)
{
    this->headimg = headimg;
}

int MdUser::getCreatetime()
{
    return createtime;
}

void MdUser::setCreatetime(int createtime)
{
    this->createtime = createtime;
}

QString MdUser::getAccessToken()
{
    return access_token;
}

void MdUser::setAccessToken(QString access_token)
{
    this->access_token = access_token;
}

void MdUser::setStatus(int status)
{
    this->status = status;
}

int MdUser::getStatus()
{
    return status;
}

QString MdUser::getSecKey()
{
    return this->secKey;
}

void MdUser::setSecKey(QString secKey)
{
    this->secKey = secKey;
}

QString MdUser::getSenderPubKey()
{
    return this->senderPubKey;
}

void MdUser::setSenderPubKey(QString senderPubKey)
{
    this->senderPubKey = senderPubKey;
}

QString MdUser::getSenderPriKey()
{
    return this->senderPriKey;
}

void MdUser::setSenderPriKey(QString senderPriKey)
{
    this->senderPriKey = senderPriKey;
}

QString MdUser::getRecverPubKey()
{
    return this->recverPubKey;
}

void MdUser::setRecverPubKey(QString recverPubKey)
{
    this->recverPubKey = recverPubKey;
}

QString MdUser::getRecverPriKey()
{
    return this->recverPriKey;
}

void MdUser::setRecverPriKey(QString recverPriKey)
{
    this->recverPriKey = recverPriKey;
}

// 重载 = 运算符
MdUser & MdUser::operator=(MdUser A) {
    uid = A.getUid();
    username = A.getUsername();
    password = A.getPassword();
    nickname = A.getNickname();
    headimg = A.getHeadimg();
    createtime = A.getCreatetime();
    access_token = A.getAccessToken();
    createtime = A.getCreatetime();
    status = A.getStatus();
    secKey = A.getSecKey();
    senderPubKey = A.getSenderPubKey();
    senderPriKey = A.getSenderPriKey();
    recverPubKey = A.getRecverPubKey();
    recverPriKey = A.getRecverPriKey();
    return *this;
}
