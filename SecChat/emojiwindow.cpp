#include "emojiwindow.h"
#include "ui_emojiwindow.h"
#include <QDebug>

EmojiWindow::EmojiWindow(QWidget *parent, QPoint pos) :
    QMainWindow(parent),
    ui(new Ui::EmojiWindow)
{
    ui->setupUi(this);
    mainWindow = (MainWindow *)parent;
    setFixedSize(this->width(), this->height());
    pos.setY(pos.y() - this->height() - 8);
    move(pos);
    setWindowFlags (Qt::FramelessWindowHint | Qt::Popup);

    // 添加emoji按钮
    for (int i= 0; i< emoji.size(); i ++) {
        insertEmoji(emoji.at(i), QPoint((i % 10) *40, (i / 10) * 40 + 30));
    }
}

EmojiWindow::~EmojiWindow()
{
    delete ui;
}

void EmojiWindow::insertEmoji(QString text, QPoint pos)
{
    QPushButton * button = new QPushButton(this);
    button->resize(40, 40);
    button->setStyleSheet("QPushButton{font: 12pt}");
    button->setText(text);
    button->setFlat(true);
    button->move(pos);
    button->show();
    connect(button,&QPushButton::clicked,this,[=](){emojiSelected(text);});
}

void EmojiWindow::emojiSelected(QString text)
{
    qDebug() << text;
    mainWindow->appendPlainText(text);
    close();
}
