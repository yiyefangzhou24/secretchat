#include "config.h"

void Config::create()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);

    // 系统全局配置
    setting.beginGroup("SERVER");
    setting.setValue("port", 6666);       //默认服务器端口
    setting.setValue("host", "127.0.0.1");  //默认服务器地址
    setting.setValue("heartbeat", 60);       //默认心跳间隔（秒）
    setting.endGroup();
    // 安全配置
    setting.beginGroup("SECURITY");
    setting.setValue("cskey", "");
    setting.endGroup();
}

void Config::open()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(CONFIGNAME));
}

QString Config::getServerHost()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    QString host = setting.value("host", "").toString();
    setting.endGroup();
    return host;
}

void Config::setServerHost(QString host)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    setting.setValue("host", host);
    setting.endGroup();
}

int Config::getServerPort()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    int port = setting.value("port", 0).toInt();
    setting.endGroup();
    return port;
}

void Config::setServerPort(int port)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    setting.setValue("port", port);
    setting.endGroup();
}

int Config::getHeartbeat()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    int port = setting.value("heartbeat", 0).toInt();
    setting.endGroup();
    return port;
}

void Config::setHeartbeat(int heartbeat)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SERVER");
    setting.setValue("heartbeat", heartbeat);
    setting.endGroup();
}

void Config::setCSKey(QString csKey)
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SECURITY");
    setting.setValue("cskey", csKey);
    setting.endGroup();
}

QString Config::getCSKey()
{
    QSettings setting(CONFIGNAME, QSettings::IniFormat);
    setting.beginGroup("SECURITY");
    QString host = setting.value("cskey", "").toString();
    setting.endGroup();
    return host;
}
