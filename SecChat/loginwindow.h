#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include "netcore.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = nullptr, NetCore * netCore = nullptr);
    ~LoginWindow();

protected:

    virtual void closeEvent(QCloseEvent* event) override;

private:

    // 私有函数 - 密码加密
    QString passwdEncrpyt(QString t);

signals:

    void openMain();                    // 打开主界面窗口信号

    void openRegister();                // 打开注册窗口信号

    void closeWind(QString title);      // 关闭窗口信号

private slots:
    void on_pushButtonLogin_clicked();

    // 登录响应 槽函数
    void on_login_response(int error, QString msg);

    void on_pushButtonRegister_clicked();

    void on_pushButtonConfig_clicked();

private:
    Ui::LoginWindow *ui;

    NetCore * netCore = nullptr;
};

#endif // LOGINWINDOW_H
