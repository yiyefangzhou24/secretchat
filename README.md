# 设计架构
本程序采用了客户端 <--> 服务器 <-->的通讯方式。

#### （一）消息服务器主要作用
1、处理客户端与服务器之间的C/S消息。此类消息不需要其他客户端参与，由客户端发起请求，服务器执行完毕后将结果返回给该客户端。主要有：登录、注册、修改用户信息、获取离线消息等。

2、协助客户端与客户端之间的C2C消息。如果两个客户端均在线（登录状态）则直接转发消息，如果有一方离线，则缓存消息到数据库，等待该客户端上线后拉取离线消息时发还。

3、处理用户身份认证。对用户的登录、注销请求进行认证，并发放一次性访问令牌。

#### （二）通讯加密的主要方式
1、客户端与客户端之间的C2C消息中，聊天内容字段必须加密，且加解密权限仅限于两个客户端之间，服务器在转发和缓存消息的过程中，无法解密消息内容。本程序在客户端通讯过程中，采用openssl库的ECDH密钥协商算法，协商后的AES加密密钥保存在两个客户端自己本地，服务器不参与该内容的加解密。（网络包格式图片中灰色部分）

2、客户端与服务器之间的C/S消息，因为没有聊天内容字段的信息，所以可选加密，对该内容加密能够保护用户登录认证、用户信息等内容安全。本程序使用的方法是：客户端使用预服务器设好的AES对称密钥加密后发送给服务器，服务器对内容进行解密后再进一步处理。（网络包格式图片中橙色部分）

![加密通讯过程](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/%E5%8A%A0%E5%AF%86%E9%80%9A%E8%AE%AF%E8%BF%87%E7%A8%8B.PNG)

#### （三）网络包的结构

![网络包结构](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/%E6%B6%88%E6%81%AF%E7%BB%93%E6%9E%84.PNG)

#### （四）消息结构体内容
网络包格式图片中绿色部分。

请求数据包（客户端->服务器）

![请求数据包](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/%E8%AF%B7%E6%B1%82%E6%95%B0%E6%8D%AE%E5%8C%85_%EF%BC%88%E5%AE%A2%E6%88%B7%E7%AB%AF-%E6%9C%8D%E5%8A%A1%E5%99%A8%EF%BC%89.png)

返回数据包（服务器->客户端）

![返回数据包](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/%E8%BF%94%E5%9B%9E%E6%95%B0%E6%8D%AE%E5%8C%85_%EF%BC%88%E6%9C%8D%E5%8A%A1%E5%99%A8-%E5%AE%A2%E6%88%B7%E7%AB%AF%EF%BC%89.png)

# 编译运行环境

Qt Creator 4.11.1

Based on Qt 5.14.1 (32 bit)

MYSQL 5.6.43 (32 bit)

Openssl 3.0.8 (32 bit)


服务端和客户端运行时均需要openssl库支持，需要把代码目录下的openssl/bin文件夹中的库复制到程序运行目录

服务端运行时需要mysql库支持，需要把mysql安装目录下的mysql.dll复制到程序运行目录

服务端运行前需要手动建立secretchat数据库，并执行SecServer/secretchat.sql初始化数据库


# 运行效果

![图片1](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin1.PNG)

![图片2](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin2.PNG)

![图片3](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin3.PNG)

![图片4](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin4.PNG)

![图片5](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin5.PNG)

![图片6](https://gitee.com/yiyefangzhou24/secretchat/raw/master/doc/bin6.PNG)


# 联系方式

有任何疑问或bug请直接在issues反馈或发邮件到yiyefangzhou24@qq.com。